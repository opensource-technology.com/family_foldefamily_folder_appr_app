require "application_system_test_case"

class CommunitySurveysTest < ApplicationSystemTestCase
  # setup do
  #   login
  # end

  # test "visiting the community surveys" do
  #   # login
  #   visit community_surveys_url
  #   assert_selector ".active.section", text: "#{I18n.t('community_survey.title_label')}"
  # end

  # test "creating a Community survey" do
  #   visit community_surveys_url
  #   first('a[href="/community_surveys/new"]').click
  #   assert_selector "div", text: "#{I18n.t('community_survey.add_label')}"

  #   fill_in('community_survey[vname]', with: 'community')
  #   choose(id: 'community_survey_ctype_1', allow_label_click: true)
  #   choose(id: 'community_survey_subtype_12', allow_label_click: true)
  #   select(addr_changwats(:one).label, from: 'community_survey[addr_changwat_id]')
  #   select(addr_amphurs(:one).label, from: 'community_survey[addr_amphur_id]')
  #   select(addr_tambons(:one).label, from: 'community_survey[addr_tambon_id]')
  #   choose(id: 'community_survey_commustyle_1', allow_label_click: true)
  #   fill_in('community_survey[datesurvey]', with: '17/06/2018')
  #   fill_in('community_survey[nhouse]', with: '2')
  #   fill_in('community_survey[nfamily]', with: '1')
  #   fill_in('community_survey[nperson]', with: '4')
  #   choose(id: 'community_survey_dogs_2', allow_label_click: true)
  #   find('input[type="submit"]').click
  #   assert_selector "div", class: 'content', text: "#{I18n.t('community_survey.m_created_label')}"
  # end

  # test "not work creating a Community survey" do
  #   visit community_surveys_url
  #   first('a[href="/community_surveys/new"]').click
  #   assert_selector "div", text: "#{I18n.t('community_survey.add_label')}"

  #   fill_in('community_survey[vname]', with: 'community')
  #   choose(id: 'community_survey_ctype_1', allow_label_click: true)
  #   choose(id: 'community_survey_subtype_12', allow_label_click: true)
  #   select(addr_changwats(:one).label, from: 'community_survey[addr_changwat_id]')
  #   select(addr_amphurs(:one).label, from: 'community_survey[addr_amphur_id]')
  #   select(addr_tambons(:one).label, from: 'community_survey[addr_tambon_id]')
  #   choose(id: 'community_survey_commustyle_1', allow_label_click: true)
  #   fill_in('community_survey[datesurvey]', with: '17/06/2018')
  #   # fill_in('community_survey[nhouse]', with: '2')
  #   fill_in('community_survey[nfamily]', with: '1')
  #   fill_in('community_survey[nperson]', with: '4')
  #   choose(id: 'community_survey_dogs_2', allow_label_click: true)
  #   find('input[type="submit"]').click
  #   assert_selector "div", class: 'content', text: "#{I18n.t('community_survey.m_created_label')}"
  # end
end
