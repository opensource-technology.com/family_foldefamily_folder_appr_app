
require 'csv'
ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'
# require 'capybara/rails'
# require 'capybara/minitest'

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  # fixtures :all

  # Add more helper methods to be used by all tests here...
end

module SetDataInit
  def data_init
    CSV.read('test/csv/community_surveys.csv', headers: true).each do |row|
      community_survey = CommunitySurvey.find_by(vname: row.to_hash['vname'])
      break unless community_survey.nil?

      community_survey = CommunitySurvey.create row.to_hash
      house = House.new
      family_folder = FamilyFolder.new
      person = Person.new
      
      CSV.read('test/csv/houses.csv', headers: true).each do |row|
        house = House.create row.to_hash.merge({ community_survey_id: community_survey.id })
      end
      CSV.read('test/csv/family_folders.csv', headers: true).each do |row|
        family_folder = FamilyFolder.create row.to_hash.merge({ house_id: house.id })
      end
      CSV.read('test/csv/house_health_surveys.csv', headers: true).each do |row|
        HouseHealthSurvey.create row.to_hash.merge({ family_folder_id: family_folder.id })
      end
      CSV.read('test/csv/people.csv', headers: true).each do |row|
        person = Person.create row.to_hash.merge({ family_folder_id: family_folder.id, house_id: house.id})
      end
      CSV.read('test/csv/person_health_surveys.csv', headers: true).each do |row|
        PersonHealthSurvey.create row.to_hash.merge({ family_folder_id: family_folder.id, person_id: person.id})
      end
      CSV.read('test/csv/home_visits.csv', headers: true).each do |row|
        HomeVisit.create row.to_hash.merge({ family_folder_id: family_folder.id, person_id: person.id, house_id: house.id})
      end
    end
  end
end

class ActionDispatch::IntegrationTest
  include SetDataInit
#   # Make the Capybara DSL available in all integration tests
#   include Capybara::DSL
#   # Make `assert_*` methods behave like Minitest assertions
#   include Capybara::Minitest::Assertions

#   # Reset sessions and driver between tests
#   # Use super wherever this method is redefined in your individual test classes
#   def teardown
#     Capybara.reset_sessions!
#     Capybara.use_default_driver
#   end
end

