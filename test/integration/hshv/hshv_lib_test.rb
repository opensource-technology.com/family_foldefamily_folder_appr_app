require 'test_helper'
require 'hshv'

class HSHV::HSHVLibTest < ActionDispatch::IntegrationTest
  def initialize(*args, &blk)
    super(*args, &blk)
    data_init
  end

  test 'should send home_visit_dup success' do
    sleep 1.2 
    home_visit = home_visit_dup
    send_data home_visit
    home_visit = HomeVisit.find(home_visit.hvid) #update assessment
    assert_equal home_visit&.process_result&.status, 1, "รอตรวจสอบข้อมูล"
    assert_equal home_visit&.process_result&.sent_status, 2, "ส่งสำเร็จ"
  end

  test 'should send person_health_survey_dup success' do
    sleep 1.2 
    person_health_survey = person_health_survey_dup
    send_data person_health_survey
    person_health_survey = PersonHealthSurvey.find(person_health_survey.phsurvey_id) #update assessment
    assert_equal person_health_survey&.process_result&.status, 1, "รอตรวจสอบข้อมูล"
    assert_equal person_health_survey&.process_result&.sent_status, 2, "ส่งสำเร็จ"
  end

  test 'should check home_visit_dup success' do
    sleep 1.9
    home_visit = home_visit_dup
    send_data home_visit
    home_visit = HomeVisit.find(home_visit.hvid) #update assessment
    HSHV::Commander.call(HSHV::CheckCommand.new, list: [HSHV::Item.new(home_visit&.process_result)])
    home_visit = HomeVisit.find(home_visit.hvid)
    puts '-:- 2.process_result in return form CheckCommand -:- ', home_visit&.process_result.to_json
    assert_equal home_visit&.process_result&.sent_status, 2, "ส่งสำเร็จ"
  end

  test 'should check person_health_survey_dup success' do
    sleep 1.9
    person_health_survey = person_health_survey_dup
    send_data person_health_survey
    person_health_survey = PersonHealthSurvey.find(person_health_survey.phsurvey_id) #update assessment
    HSHV::Commander.call(HSHV::CheckCommand.new, list: [HSHV::Item.new(person_health_survey&.process_result)])
    person_health_survey = PersonHealthSurvey.find(person_health_survey.phsurvey_id)
    puts '-:- 2.process_result in return form CheckCommand -:- ', person_health_survey&.process_result.to_json
    assert_equal person_health_survey&.process_result&.sent_status, 2, "ส่งสำเร็จ"

  end

  private

  def send_data(data)
    hshv_comand HSHV::SendCommand.new, data
  end

  def hshv_comand(cmd, data)
    item = HSHV::Item.new data
    HSHV::Commander.call cmd, list: [item]
  end


  def home_visit_dup
    home_visit_dup = HomeVisit.last.dup
    home_visit_dup.created_at = Time.now.getutc
    home_visit_dup.process_result_id = nil
    home_visit_dup.save
    home_visit_dup.hvid = Time.now.strftime("%Y%m%d%H%M%S") 
    home_visit_dup.save
    home_visit_dup
  end

  def person_health_survey_dup
    person_health_survey_dup = PersonHealthSurvey.last.dup
    person_health_survey_dup.created_at = Time.now.getutc
    person_health_survey_dup.process_result_id = nil
    person_health_survey_dup.save
    person_health_survey_dup.phsurvey_id = Time.now.strftime("%Y%m%d%H%M%S") 
    person_health_survey_dup.save
    person_health_survey_dup
  end
end
