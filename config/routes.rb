Rails.application.routes.draw do
  # get 'search/people'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'home#index'
  get 'home/index'

  resources :community_surveys do
    resources :houses do
      resources :family_folders do
        resources :house_health_surveys
        get '/people/search/:id', to: 'people#search'
        resources :people do
          resources :person_health_surveys
          resources :home_visits
        end
      end
    end
  end

  # Address
  get 'addr/update_amphur/:id', to: 'addresses#update_amphur'
  get 'addr/update_tambon/:id', to: 'addresses#update_tambon'

  # Authentication
  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  get '/goto_house', to: 'home#house'
  get '/goto_house_health_survey', to: 'home#house_health_survey'
  get '/goto_person', to: 'home#person'
  get '/goto_person_health_survey', to: 'home#person_health_survey'
  get '/goto_home_visit', to: 'home#home_visit'

  get 'communities/search', to: 'community_surveys#search_main'
  resources :diseases

  # Report
  # resources :reports
  # get '/reports', to: 'reports#community_survey'
  get '/reports/overall', to: 'reports#overall'
  get '/reports/community', to: 'reports#community_survey'
  get '/reports/admin/health_survey', to: 'reports#admin_health_survey'
  get '/reports/admin/home_visit', to: 'reports#admin_home_visit'
  get '/reports/admin/houses', to: 'reports#admin_houses'
  get '/reports/admin/people', to: 'reports#admin_people'
  get '/reports/site/health_survey', to: 'reports#site_health_survey'
  get '/reports/site/home_visit', to: 'reports#site_home_visit'
  get '/reports/site/houses', to: 'reports#site_houses'
  get '/reports/site/people', to: 'reports#site_people'

  get '/hospitals', to: 'hospitals#search'
  get '/hospitals/type/:id', to: 'hospitals#by_type'

  # Search
  # get '/search', to: 'search#search_people'
  # Test
  get '/search', to: 'search#people'
end
