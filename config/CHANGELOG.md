# EHHC

## [Unreleased]
### Changed
- Redesign and develop the application to be hosted on cloud
- Authenicate user from NHSO

## [1.1.6] - 2019-03-15
### Changed
- Revert back to verify SSL before authenticate
- Change gregorain to buddhist year

## [1.1.5]
### Changed
- Ignore SSL verification

## [1.1.2] - 2018-12-25
## Changed
- Allow to load env from dotenv

## [1.1.1] - 2018-12-13
### Changed
- Authenticate with NSHO instead local provider
- User has authorize to access report page