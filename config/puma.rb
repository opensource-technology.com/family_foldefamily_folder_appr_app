# Puma can serve each request in a thread from an internal thread pool.
# The `threads` method setting takes two numbers: a minimum and maximum.
# Any libraries that use thread pools should be configured to match
# the maximum value specified for Puma. Default is set to 5 threads for minimum
# and maximum; this matches the default thread size of Active Record.
#
# require 'dotenv'

APP_ROOT = ENV.fetch("APP_ROOT") { Dir.pwd }
# Dotenv.load("#{APP_ROOT}/config/app.env")

threads_count = ENV.fetch("RAILS_MAX_THREADS") { 5 }
threads threads_count, threads_count

env = ENV.fetch('RAILS_ENV') { 'development' }
environment env

# Specifies the `port` that Puma will listen on to receive requests; default is 3000.
if env == 'development' || Gem.win_platform?
  bind "tcp://0.0.0.0:#{ENV.fetch('APP_PORT', 3000)}"
else
  # Dir.mkdir("#{APP_ROOT}/tmp/pids") unless Dir.exist?("#{APP_ROOT}/tmp/pids")
  # bind "unix:///#{APP_ROOT}/tmp/puma.sock?umask=0111"
  # pidfile "#{APP_ROOT}/tmp/pids/server.pid"
  # state_path "#{APP_ROOT}/tmp/pids/puma.state"
  # Dir.mkdir("#{APP_ROOT}/tmp/pids") unless Dir.exist?("#{APP_ROOT}/tmp/pids")
  bind "unix:///#{APP_ROOT}/tmp/puma.sock?umask=0111"
  # bind "tcp://0.0.0.0:#{ENV.fetch('APP_PORT', 3000)}"
  # Specifies the number of `workers` to boot in clustered mode.
  # Workers are forked webserver processes. If using threads and workers together
  # the concurrency of the application would be max `threads` * `workers`.
  # Workers do not work on JRuby or Windows (both of which do not support
  # processes).
  #
end

workers ENV.fetch("WEB_CONCURRENCY") { 2 }

# Use the `preload_app!` method when specifying a `workers` number.
# This directive tells Puma to first boot the application and load code
# before forking the application. This takes advantage of Copy On Write
# process behavior so workers use less memory.
#
preload_app!

on_worker_boot do
  # Valid on Rails 4.1+ using the `config/database.yml` method of setting `pool` size
  ActiveRecord::Base.establish_connection
end

# Allow puma to be restarted by `rails restart` command.
plugin :tmp_restart
