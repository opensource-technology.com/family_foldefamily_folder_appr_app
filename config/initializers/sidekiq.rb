require 'sidekiq_unique_jobs'

Sidekiq.configure_server do |config|
  config.redis = { url: 'redis://localhost:6379/1' }
end

Sidekiq.configure_client do |config|
  config.redis = { url: 'redis://localhost:6379/1' }
end

SidekiqUniqueJobs.configure do |config|
  config.enabled = true
  # config.debug_lua = false
  # config.lock_info = true
  # config.lock_ttl = 10.minutes
  # config.lock_timeout = 10.minutes
  # config.logger = Sidekiq.logger
  # config.max_history = 10_000
  # config.reaper = :lua
  # config.reaper_count = 100
  # config.reaper_interval = 10
  # config.reaper_timeout = 5
end
