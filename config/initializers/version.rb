require 'yaml'

class Version
  attr_reader :build

  def initialize(major:, minor:, patch:, build:, hash:)
    @major = major
    @minor = minor
    @patch = patch
    @build = build
    @hash = hash
  end

  def release
    "#{@major}.#{@minor}.#{@patch}"
  end
end

version = Version.new(YAML.safe_load(File.read('config/version.yml')).symbolize_keys)

Rails.application.config.app_version = version.release
Rails.application.config.app_build_number = version.build
