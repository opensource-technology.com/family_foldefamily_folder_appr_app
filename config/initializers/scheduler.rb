require 'rufus-scheduler'
# require 'rufus-scheduler'
scheduler = Rufus::Scheduler.new

# Schedule: Run after midnight
scheduler.cron '0 16-23 * * *' do
  # HshvSendDataJob.perform_now
end

scheduler.cron '0 00-06 * * *' do
  # HshvCheckerJob.perform_now
end
