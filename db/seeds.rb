# # This file should contain all the record creation needed to seed the database with its default values.
# # The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
# #
# # Examples:
# #
# #   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
# #   Character.create(name: 'Luke', movie: movies.first)
# require 'csv'

# unless User.find_by_username('admin')
#   User.create username: 'admin', password: 'adminadmin',
#               password_confirmation: 'adminadmin', role: :admin
# end

# unless User.find_by_username('user1')
#   User.create username: 'user1', password: 'useruser',
#               password_confirmation: 'useruser', hospcode: '11482'
# end

# unless User.find_by_username('user2')
#   User.create username: 'user2', password: 'useruser',
#               password_confirmation: 'useruser', hospcode: '11535'
# end

# unless User.find_by_username('user3')
#   User.create username: 'user3', password: 'useruser',
#               password_confirmation: 'useruser', hospcode: '13674'
# end

# if Hospital.first.nil?
# CSV.readlines('db/load/hospitals.csv', col_sep: '|').each do |row|
#   puts "#{row[0].strip} : #{row[1].strip}"
#   #     Hospital.create code: row[0], label: row[1]
# end

# CSV.readlines('db/load/new_hospital_list.csv').each do |row|
#   puts "#{row[0]} : #{row[1]}"
#   begin
#     Hospital.create code: row[0], label: row[1]
#   rescue ActiveRecord::RecordNotUnique => e
#     # ignore
#     puts e.message
#   end
# end

# if AddrChangwat.first.nil?
#   CSV.readlines('db/load/addr_changwat_list.csv').each do |row|
#     puts "#{row[0]} : #{row[1]}"
#     AddrChangwat.create code: row[0], label: row[1] unless row[1].include?('*')
#   end
# end

# if AddrAmphur.first.nil?
#   CSV.readlines('db/load/addr_amphur_list.csv').each do |row|
#     puts "#{row[0]} : #{row[1]}" unless row[1].include?('*')
#     AddrAmphur.create code: row[0], label: row[1] unless row[1].include?('*')
#   end
# end

# if AddrTambon.first.nil?
#   CSV.readlines('db/load/addr_tambon_list.csv').each do |row|
#     puts "#{row[0]} : #{row[1]}" unless row[1].include?('*')
#     AddrTambon.create code: row[0], label: row[1] unless row[1].include?('*')
#   end
# end

# if Title.first.nil?
#   CSV.readlines('db/load/title_list.csv').each do |row|
#     puts "#{row[0]} : #{row[1]}"
#     Title.create code: row[0], label: row[1]
#   end
# end

# if Nationality.first.nil?
#   CSV.readlines('db/load/nationality_list.csv').each do |row|
#     puts "#{row[0].rjust(3, '0')} : #{row[1]}"
#     Nationality.create code: row[0].rjust(3, '0'), label: row[1]
#   end
# end

# if Race.first.nil?
#   CSV.readlines('db/load/nationality_list.csv').each do |row|
#     puts "#{row[0].rjust(3, '0')} : #{row[1]}"
#     Race.create code: row[0].rjust(3, '0'), label: row[1]
#   end
# end

# if Religion.first.nil?
#   CSV.readlines('db/load/religion_list.csv').each do |row|
#     puts "#{row[0].rjust(2, '0')} : #{row[1]}"
#     Religion.create code: row[0].rjust(2, '0'), label: row[1]
#   end
# end
# puts 'Seed success!!!'

# require 'parallel'

# puts CommunitySurvey.group(:hospcode).count

# CommunitySurvey.joins("inner join hospitals on hospitals.code = community_surveys.hospcode").distinct.order(hospcode: :asc).pluck(:label, :hospcode).each do |label, hospcode|
#   communitys = CommunitySurvey.where(hospcode: hospcode).count
#   houses = House.where(hospcode: hospcode).count
#   family_folders = FamilyFolder.where(hospcode: hospcode).count
#   house_health_surveys = HouseHealthSurvey.where(hospcode: hospcode).count
#   people = Person.where(hospcode: hospcode).count
#   person_health_surveys = PersonHealthSurvey.where(hospcode: hospcode).count
#   home_visits = HomeVisit.where(hospcode: hospcode).count
#   puts "#{hospcode}, #{label}, #{communitys}, #{houses}, #{family_folders}, #{house_health_surveys}, #{people}, #{person_health_surveys}, #{home_visits}"
# end


# Delete data

# CSV.readlines('./lib/communitysurveys_20190509.csv', col_sep: '|', headers: true).each do |row|
#   puts "#{row[0]} #{row[1]}"
#   c = CommunitySurvey.find_by(vid: row[0], hospcode: row[1], datesurvey: row[2])
#   c&.destroy
# end

# CSV.readlines('./lib/houses_20190509.csv', col_sep: '|', headers: true).each do |row|
#   puts "#{row[0]} #{row[1]}"
#   h = House.find_by(hid: row[0], community_survey_id: row[1], hospcode: row[2], house_id: row[3], datesurvey: row[4])
#   h&.destroy
# end

# CSV.readlines('./lib/familyfolders_20190509.csv', col_sep: '|', headers: true).each do |row|
#   puts "#{row[0]} #{row[1]}"
#   f = FamilyFolder.find_by(ffolder_id: row[0], house_id: row[1], hospcode: row[5])
#   f&.destroy
# end

# CSV.readlines('./lib/house_healthsurveys_20190509.csv', col_sep: '|', headers: true).each do |row|
#   puts "#{row[0]} #{row[1]}"
#   f = HouseHealthSurvey.find_by(hhsurvey_id: row[0], house_id: row[1], hospcode: row[5])
#   f&.destroy
# end

# Covid
# CSV.readlines('db/load/covid.csv').each do |row|
#   puts "#{row[0].strip} : #{row[1].strip}"
#   #     Hospital.create code: row[0], label: row[1]
#   c = CommunitySurvey.create vid: "#{row[0].strip}000000000", hospcode: row[0].strip, datesurvey: Date.new(2020, 4, 1), vname: "ชุมชม ดูแลสุขภาพประชาชนช่วงที่มีการระบาด covid 19", ctype: 1, subtype: 11, commustyle: 4, nhouse: 0, catm: 10410100, addr_changwat_id: 1, addr_amphur_id: 41, addr_tambon_id: 153, dogs: 0
#   pp c.errors
# end

# require_relative './process'
# require_relative './seeds/communities'
require_relative './seeds/houses'

puts "Seeded"
