# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_12_06_092937) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addr_amphurs", force: :cascade do |t|
    t.string "code", null: false
    t.string "label", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["code"], name: "index_addr_amphurs_on_code", unique: true
  end

  create_table "addr_changwats", force: :cascade do |t|
    t.string "code", null: false
    t.string "label", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["code"], name: "index_addr_changwats_on_code", unique: true
  end

  create_table "addr_tambons", force: :cascade do |t|
    t.string "code", null: false
    t.string "label", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["code"], name: "index_addr_tambons_on_code", unique: true
  end

  create_table "audit_logs", force: :cascade do |t|
    t.string "user"
    t.string "category"
    t.string "action"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "community_surveys", primary_key: "vid", id: :string, limit: 14, force: :cascade do |t|
    t.string "hospcode", limit: 5, null: false
    t.date "datesurvey", null: false
    t.string "vname", null: false
    t.string "catm", limit: 8, null: false
    t.string "ctype", limit: 1, null: false
    t.string "subtype", limit: 2, null: false
    t.string "mainvid", limit: 255
    t.string "commustyle", limit: 1, null: false
    t.integer "nhouse"
    t.integer "nfamily"
    t.integer "nperson"
    t.string "waste", limit: 1
    t.string "flood", limit: 1
    t.string "dogs", limit: 1, null: false
    t.string "environment", limit: 1
    t.string "riskenvironment", limit: 1
    t.string "riskentype11", limit: 2
    t.string "riskentype12", limit: 2
    t.string "riskentype13", limit: 2
    t.integer "nclinic"
    t.integer "ndrugstore"
    t.integer "npchc"
    t.integer "ncompha"
    t.integer "ngovernm"
    t.integer "ntemple"
    t.integer "npschool"
    t.integer "nsschool"
    t.integer "ntschool"
    t.integer "nuniversity"
    t.integer "nhospital"
    t.integer "nchildcenter"
    t.integer "nj"
    t.integer "npolice"
    t.integer "nfire"
    t.integer "nembass"
    t.integer "nhealcare"
    t.integer "nfound"
    t.integer "nassoci"
    t.integer "nbroadcast"
    t.integer "nradio"
    t.integer "nsubhealcare"
    t.integer "nsports"
    t.integer "nursinghome"
    t.integer "nspa"
    t.integer "nabattoir"
    t.integer "ngas"
    t.integer "ngame"
    t.integer "nfixcar"
    t.integer "nfix"
    t.integer "ngasst"
    t.integer "nfactory"
    t.integer "noilst"
    t.integer "npub"
    t.integer "nware"
    t.integer "nfarm"
    t.integer "nhabitat"
    t.integer "nhotel"
    t.integer "nmarket"
    t.integer "ntotor"
    t.integer "nvdo"
    t.integer "nlaundry"
    t.integer "nfooddrink"
    t.integer "ngold"
    t.integer "ncompany"
    t.integer "nbank"
    t.integer "nshop"
    t.integer "ndepartment"
    t.integer "noffice"
    t.integer "nbook"
    t.datetime "u_date", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "addr_changwat_id"
    t.bigint "addr_amphur_id"
    t.bigint "addr_tambon_id"
    t.string "location"
    t.string "moo"
    t.string "soimain"
    t.string "soisum"
    t.string "road"
    t.integer "nsalon"
    t.integer "nother"
    t.index ["addr_amphur_id"], name: "index_community_surveys_on_addr_amphur_id"
    t.index ["addr_changwat_id"], name: "index_community_surveys_on_addr_changwat_id"
    t.index ["addr_tambon_id"], name: "index_community_surveys_on_addr_tambon_id"
  end

  create_table "diseases", force: :cascade do |t|
    t.string "name", null: false
    t.string "dtype", limit: 1, null: false
    t.date "sick_at"
    t.string "treat_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "person_id"
    t.index ["person_id"], name: "index_diseases_on_person_id"
  end

  create_table "event_logs", force: :cascade do |t|
    t.string "name", null: false
    t.string "message"
    t.string "actor", null: false
    t.integer "level", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "family_folders", primary_key: "ffolder_id", id: :string, limit: 14, force: :cascade do |t|
    t.string "house_id"
    t.datetime "u_date", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "hospcode", limit: 5, null: false
    t.index ["house_id"], name: "index_family_folders_on_house_id"
  end

  create_table "home_visits", primary_key: "hvid", id: :string, limit: 15, force: :cascade do |t|
    t.string "person_id"
    t.string "house_id"
    t.string "hospcode", limit: 5, null: false
    t.date "datesurvey", null: false
    t.string "gtype", limit: 4
    t.float "btemp"
    t.integer "pr"
    t.integer "rr"
    t.float "weight"
    t.float "height"
    t.integer "waist_cm"
    t.float "bmi"
    t.integer "hip_cm"
    t.float "dtx"
    t.integer "sbp"
    t.integer "dbp"
    t.string "healthscr", limit: 1
    t.string "mentalscn", limit: 1
    t.string "healthadvice", limit: 1
    t.string "develope", limit: 1
    t.string "demonbodychk", limit: 1
    t.string "demonhealthpro", limit: 1
    t.string "behaviormodifi", limit: 1
    t.string "bodychk", limit: 1
    t.string "wiping", limit: 1
    t.string "eyechek", limit: 1
    t.string "drugplan", limit: 1
    t.string "oralhyg", limit: 1
    t.string "uterus", limit: 1
    t.string "breastscr", limit: 1
    t.string "firstaid", limit: 1
    t.string "blood", limit: 1
    t.string "inject", limit: 1
    t.string "specimen", limit: 1
    t.string "wound", limit: 1
    t.datetime "u_date", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "family_folder_id"
    t.integer "surveyor_time", default: 1, null: false
    t.string "surveyor_fullname"
    t.bigint "process_result_id"
    t.datetime "user_last_updated_at", default: "2019-12-06 09:31:24"
    t.index ["family_folder_id"], name: "index_home_visits_on_family_folder_id"
    t.index ["house_id"], name: "index_home_visits_on_house_id"
    t.index ["person_id"], name: "index_home_visits_on_person_id"
    t.index ["process_result_id"], name: "index_home_visits_on_process_result_id"
  end

  create_table "hospitals", force: :cascade do |t|
    t.string "code", null: false
    t.string "label", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "htype", default: 0
    t.index ["code"], name: "index_hospitals_on_code", unique: true
  end

  create_table "house_health_surveys", primary_key: "hhsurvey_id", id: :string, limit: 14, force: :cascade do |t|
    t.string "family_folder_id"
    t.string "hospcode", limit: 5, null: false
    t.date "datesurvey", null: false
    t.string "explorsit", limit: 1, null: false
    t.string "transparent", limit: 1, null: false
    t.string "cleanliness", limit: 1, null: false
    t.string "wateruse", limit: 1, null: false
    t.string "foodsanit", limit: 1, null: false
    t.string "floodhouse", limit: 1, null: false
    t.string "homeenvirom", limit: 1, null: false
    t.string "animaldisease", limit: 1, null: false
    t.string "rat", limit: 1
    t.string "mosquito", limit: 1
    t.string "cockroach", limit: 1
    t.string "fly", limit: 1
    t.string "snake", limit: 1
    t.string "pets", limit: 1, null: false
    t.integer "ndog"
    t.integer "ndogsterilize"
    t.integer "ndogvaccine"
    t.integer "ndogmicro"
    t.integer "ncat"
    t.integer "ncatsterilize"
    t.integer "ncatvaccine"
    t.integer "nmouse"
    t.integer "nmousevaccine"
    t.integer "nrabbit"
    t.integer "nrabbitvaccine"
    t.integer "nsquirrel"
    t.integer "nsquirrelvaccine"
    t.integer "nmonkey"
    t.integer "nmonkeyvaccine"
    t.integer "nbirds"
    t.integer "nducks"
    t.integer "ncows"
    t.integer "npigs"
    t.integer "nchicken"
    t.integer "nfish"
    t.integer "nbuffalo"
    t.integer "ngoat"
    t.integer "nreptile"
    t.datetime "u_date", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "surveyor_fullname"
    t.string "surveyor_position"
    t.string "lightning", limit: 1
    t.string "drinking_water", limit: 1
    t.string "garbage", limit: 1
    t.string "toilet", limit: 1
    t.string "toilet_hygienic", limit: 1
    t.date "ndogvaccine_at"
    t.date "ncatvaccine_at"
    t.date "nmousevaccine_at"
    t.date "nrabbitvaccine_at"
    t.date "nsquirrelvaccine_at"
    t.date "nmonkeyvaccine_at"
    t.string "explorsit_reason"
    t.index ["family_folder_id"], name: "index_house_health_surveys_on_family_folder_id"
  end

  create_table "houses", primary_key: "hid", id: :string, limit: 14, force: :cascade do |t|
    t.string "community_survey_id"
    t.string "hospcode", limit: 5, null: false
    t.string "house_id", limit: 11
    t.date "datesurvey", null: false
    t.string "namesurvey", null: false
    t.string "positionsurvey", null: false
    t.string "house", limit: 75
    t.string "soisum"
    t.string "soimain"
    t.string "road"
    t.string "villaname"
    t.string "catm", limit: 8, null: false
    t.float "latitude"
    t.float "longitude"
    t.string "postcode", limit: 5
    t.string "phonenumber"
    t.string "housestyletype", limit: 1, null: false
    t.string "housetype", limit: 1, null: false
    t.string "houseutili", limit: 1
    t.string "nperson", limit: 3
    t.string "businesstype", limit: 3
    t.string "namebusiness"
    t.string "ncustomer", limit: 3
    t.string "accident", limit: 1
    t.string "disease", limit: 1
    t.datetime "u_date", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "addr_changwat_id"
    t.bigint "addr_amphur_id"
    t.bigint "addr_tambon_id"
    t.string "business_contact_fullname"
    t.string "business_contact_phonenumber"
    t.string "moo"
    t.string "housestyletype_other"
    t.string "accident_other"
    t.string "disease_other"
    t.index ["addr_amphur_id"], name: "index_houses_on_addr_amphur_id"
    t.index ["addr_changwat_id"], name: "index_houses_on_addr_changwat_id"
    t.index ["addr_tambon_id"], name: "index_houses_on_addr_tambon_id"
    t.index ["community_survey_id"], name: "index_houses_on_community_survey_id"
  end

  create_table "nationalities", force: :cascade do |t|
    t.string "code", null: false
    t.string "label", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "people", primary_key: "person_id", id: :string, limit: 13, force: :cascade do |t|
    t.string "house_id"
    t.string "hospcode", limit: 5, null: false
    t.date "datesurvey", null: false
    t.string "pid", limit: 13
    t.string "aid", limit: 25
    t.string "sex", limit: 1, null: false
    t.float "weight"
    t.float "height"
    t.float "waistline"
    t.float "hips"
    t.float "bmi"
    t.string "bloodgroup", limit: 1
    t.string "bloodrh", limit: 1
    t.string "nameregis", limit: 1
    t.string "statusperson", limit: 1
    t.string "maritalstate", limit: 1
    t.string "educate", limit: 1
    t.string "career", limit: 2
    t.string "commustate", limit: 1
    t.string "maininscl", limit: 1
    t.string "familystate", limit: 1
    t.date "datemovin"
    t.string "healthcheck", limit: 1
    t.string "oralhealth", limit: 1
    t.string "pracsick", limit: 2
    t.string "drugallergic", limit: 1
    t.string "detaildrug"
    t.string "foodallergy", limit: 1
    t.string "detailfood"
    t.string "riskbehav", limit: 1
    t.string "smoke", limit: 1
    t.string "allgoho", limit: 1
    t.string "energydrink", limit: 1
    t.string "addictive", limit: 1
    t.string "workdanger", limit: 1
    t.string "foodbehav", limit: 1
    t.string "overbmi", limit: 1
    t.string "geneticdiseas", limit: 1
    t.string "drugright", limit: 1
    t.string "dentprob", limit: 1
    t.string "nothealch", limit: 1
    t.string "sexrisk", limit: 1
    t.string "otherrisk", limit: 1
    t.string "exercis", limit: 1
    t.string "strain", limit: 1
    t.string "sumhealth", limit: 1
    t.datetime "u_date", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "family_folder_id"
    t.bigint "title_id", null: false
    t.string "first_name", null: false
    t.string "last_name"
    t.date "birth_date"
    t.string "pid_father"
    t.string "pid_mother"
    t.string "pid_spouse"
    t.string "nationality", default: "099", null: false
    t.string "race", default: "099"
    t.string "religion", default: "01"
    t.string "maininscl_type", limit: 1
    t.string "geneticdiseas_other"
    t.index ["family_folder_id"], name: "index_people_on_family_folder_id"
    t.index ["house_id"], name: "index_people_on_house_id"
    t.index ["title_id"], name: "index_people_on_title_id"
  end

  create_table "person_health_surveys", primary_key: "phsurvey_id", id: :string, limit: 14, force: :cascade do |t|
    t.string "person_id"
    t.string "family_folder_id"
    t.string "hospcode", limit: 5, null: false
    t.date "datesurvey", null: false
    t.string "persontype", limit: 2, null: false
    t.string "firstpreg", limit: 1
    t.string "antenatalcare1", limit: 1
    t.string "edd", limit: 1
    t.string "comppreg", limit: 1
    t.string "drugpreg", limit: 1
    t.string "birthmethod", limit: 1
    t.string "antenatalcare2", limit: 1
    t.string "postcomp", limit: 1
    t.string "drugpost", limit: 1
    t.string "followpost", limit: 1
    t.string "birthwright", limit: 1
    t.string "thyroid", limit: 1
    t.string "bcg", limit: 1
    t.string "food1", limit: 1
    t.string "vaccine1", limit: 1
    t.string "development", limit: 1
    t.string "weightheight", limit: 1
    t.string "food2", limit: 1
    t.string "vaccine2", limit: 1
    t.string "sexbehav1", limit: 1
    t.string "substabuse1", limit: 1
    t.string "healthscr", limit: 1
    t.string "sexbehav2", limit: 1
    t.string "substabuse2", limit: 1
    t.string "riskscr1", limit: 1
    t.string "papsmear", limit: 1
    t.string "breastscr", limit: 1
    t.string "servicecare", limit: 1
    t.string "adl", limit: 1
    t.string "oldtype", limit: 1
    t.string "riskscr2", limit: 1
    t.string "oralcheck", limit: 1
    t.string "movement", limit: 1
    t.string "hearing", limit: 1
    t.string "vision", limit: 1
    t.string "intellect", limit: 1
    t.string "learning", limit: 1
    t.string "autistic", limit: 1
    t.string "behavior", limit: 1
    t.string "help", limit: 1
    t.string "regiscripp", limit: 1
    t.string "healthdesk", limit: 1
    t.datetime "u_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "surveyor_time", default: 1, null: false
    t.string "surveyor_fullname"
    t.string "antenatalcare1_other"
    t.string "edd_other"
    t.string "comppreg_other"
    t.string "drugpreg_other"
    t.string "birthmethod_other"
    t.string "antenatalcare2_other"
    t.string "postcomp_other"
    t.string "drugpost_other"
    t.string "followpost_other"
    t.string "food1_other"
    t.string "servicecare_other"
    t.string "healthdesk_other"
    t.bigint "process_result_id"
    t.datetime "user_last_updated_at", default: "2019-12-06 09:31:24"
    t.index ["family_folder_id"], name: "index_person_health_surveys_on_family_folder_id"
    t.index ["person_id"], name: "index_person_health_surveys_on_person_id"
    t.index ["process_result_id"], name: "index_person_health_surveys_on_process_result_id"
  end

  create_table "process_results", force: :cascade do |t|
    t.integer "process_type", limit: 2, null: false
    t.string "pid", limit: 13
    t.string "code_check", limit: 20
    t.string "client_code", limit: 15, null: false
    t.string "hospcode", limit: 5, null: false
    t.integer "status", limit: 2, default: 1, null: false
    t.string "status_description"
    t.integer "sent_status", limit: 2, default: 1, null: false
    t.string "sent_errors"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["client_code", "process_type"], name: "index_process_results_on_client_code_and_process_type", unique: true
  end

  create_table "races", force: :cascade do |t|
    t.string "code", null: false
    t.string "label", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "religions", force: :cascade do |t|
    t.string "code", null: false
    t.string "label", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sites", force: :cascade do |t|
    t.string "hospcode", limit: 5, null: false
    t.integer "cs_no", default: 0
    t.integer "h_no", default: 0
    t.integer "ff_no", default: 0
    t.integer "hhs_no", default: 0
    t.integer "phs_no", default: 0
    t.integer "p_no", default: 0
    t.integer "hv_no", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "titles", force: :cascade do |t|
    t.string "code", null: false
    t.string "label", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "username", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "password_digest"
    t.string "hospcode"
    t.integer "role", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["username"], name: "index_users_on_username", unique: true
  end

  add_foreign_key "family_folders", "houses", primary_key: "hid", on_delete: :cascade
  add_foreign_key "home_visits", "family_folders", primary_key: "ffolder_id", on_delete: :cascade
  add_foreign_key "home_visits", "houses", primary_key: "hid", on_delete: :cascade
  add_foreign_key "home_visits", "people", primary_key: "person_id", on_delete: :cascade
  add_foreign_key "house_health_surveys", "family_folders", primary_key: "ffolder_id", on_delete: :cascade
  add_foreign_key "houses", "community_surveys", primary_key: "vid", on_delete: :cascade
  add_foreign_key "people", "family_folders", primary_key: "ffolder_id", on_delete: :cascade
  add_foreign_key "people", "houses", primary_key: "hid", on_delete: :cascade
  add_foreign_key "people", "titles"
  add_foreign_key "person_health_surveys", "family_folders", primary_key: "ffolder_id", on_delete: :cascade
  add_foreign_key "person_health_surveys", "people", primary_key: "person_id", on_delete: :cascade
end
