class CreateHospitals < ActiveRecord::Migration[5.2]
  def change
    create_table :hospitals do |t|
      t.string :code, null: false
      t.string :label, null: false
      t.timestamps
    end

    add_index :hospitals, :code, unique: true
  end
end
