class ChangeSalonTypeToCommunitySurveys < ActiveRecord::Migration[5.2]
  def up
    change_column :community_surveys, :nsalon, 'integer USING CAST(nsalon AS integer)', limit: 3, null: true
  end

  def down
    change_column :community_surveys, :nsalon, :string, limit: 3, null: true
  end
end
