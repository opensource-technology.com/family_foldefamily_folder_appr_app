class AddTypeToHospitals < ActiveRecord::Migration[5.2]
  def change
    add_column :hospitals, :htype, :integer, default: 0
  end
end
