class AddGeneticdiseasesOtherToPeople < ActiveRecord::Migration[5.2]
  def change
    add_column :people, :geneticdiseas_other, :string, null: true
  end
end
