class CreateFamilyFolders < ActiveRecord::Migration[5.2]
  def change
    create_table :family_folders, id: :string, primary_key: :ffolder_id,
                                  limit: 13, null: false do |t|
      t.references :house
      t.datetime :u_date, null: false
      t.timestamps
    end
  end
end
