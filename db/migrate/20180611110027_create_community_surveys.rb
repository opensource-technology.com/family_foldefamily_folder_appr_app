class CreateCommunitySurveys < ActiveRecord::Migration[5.2]
  def change
    create_table :community_surveys, id: :string, primary_key: :vid, limit: 8, null: false do |t|
      t.string :hospcode, limit: 5, null: false
      t.date :datesurvey, null: false
      t.string :vname, null: false
      t.string :catm, limit: 8, null: false
      t.string :type, limit: 1, null: false
      t.string :subtype, limit: 2, null: false
      t.string :mainvid, limit: 8, null: true
      t.string :commustyle, limit: 1, null: false
      t.integer :nhouse, null: true
      t.integer :nfamily, null: true
      t.integer :nperson, null: true
      t.string :waste, limit: 1, null: true
      t.string :flood, limit: 1, null: true
      t.string :dogs, limit: 1, null: false
      t.string :environment, limit: 1, null: true
      t.string :riskenvironment, limit: 1, null: true
      t.string :riskentype11, limit: 1, null: true
      t.string :riskentype12, limit: 1, null: true
      t.string :riskentype13, limit: 1, null: true
      t.integer :nclinic, limit: 3, null: true
      t.integer :ndrugstore, limit: 3, null: true
      t.integer :npchc, limit: 3, null: true
      t.integer :ncompha, limit: 3, null: true
      t.integer :ngovernm, limit: 3, null: true
      t.integer :ntemple, limit: 3, null: true
      t.integer :npschool, limit: 3, null: true
      t.integer :nsschool, limit: 3, null: true
      t.integer :ntschool, limit: 3, null: true
      t.integer :nuniversity, limit: 3, null: true
      t.integer :nhospital, limit: 3, null: true
      t.integer :nchildcenter, limit: 3, null: true
      t.integer :nj, limit: 3, null: true
      t.integer :npolice, limit: 3, null: true
      t.integer :nfire, limit: 3, null: true
      t.integer :nembass, limit: 3, null: true
      t.integer :nhealcare, limit: 3, null: true
      t.integer :nfound, limit: 3, null: true
      t.integer :nassoci, limit: 3, null: true
      t.integer :nbroadcast, limit: 3, null: true
      t.integer :nradio, limit: 3, null: true
      t.integer :nsubhealcare, limit: 3, null: true
      t.integer :nsports, limit: 3, null: true
      t.integer :nursinghome, limit: 3, null: true
      t.integer :nspa, limit: 3, null: true
      t.integer :nabattoir, limit: 3, null: true
      t.integer :ngas, limit: 3, null: true
      t.integer :ngame, limit: 3, null: true
      t.integer :nfixcar, limit: 3, null: true
      t.integer :nfix, limit: 3, null: true
      t.integer :ngasst, limit: 3, null: true
      t.integer :nfactory, limit: 3, null: true
      t.integer :noilst, limit: 3, null: true
      t.integer :npub, limit: 3, null: true
      t.integer :nware, limit: 3, null: true
      t.integer :nfarm, limit: 3, null: true
      t.integer :nhabitat, limit: 3, null: true
      t.integer :nhotel, limit: 3, null: true
      t.integer :nmarket, limit: 3, null: true
      t.integer :ntotor, limit: 3, null: true
      t.integer :nvdo, limit: 3, null: true
      t.integer :nlaundry, limit: 3, null: true
      t.integer :nfooddrink, limit: 3, null: true
      t.integer :ngold, limit: 3, null: true
      t.integer :ncompany, limit: 3, null: true
      t.integer :nbank, limit: 3, null: true
      t.integer :nshop, limit: 3, null: true
      t.integer :ndepartment, limit: 3, null: true
      t.integer :noffice, limit: 3, null: true
      t.integer :nbook, limit: 3, null: true
      t.datetime :u_date, null: false
      t.timestamps
    end
  end
end
