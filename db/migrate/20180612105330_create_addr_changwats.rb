class CreateAddrChangwats < ActiveRecord::Migration[5.2]
  def change
    create_table :addr_changwats do |t|
      t.string :code, null: false
      t.string :label, null: false
      t.timestamps
    end

    add_index :addr_changwats, :code, unique: true
  end
end
