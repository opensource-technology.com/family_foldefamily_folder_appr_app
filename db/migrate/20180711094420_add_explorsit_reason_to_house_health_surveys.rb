class AddExplorsitReasonToHouseHealthSurveys < ActiveRecord::Migration[5.2]
  def change
    add_column :house_health_surveys, :explorsit_reason, :string, null: true
  end
end
