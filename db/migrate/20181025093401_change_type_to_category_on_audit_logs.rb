class ChangeTypeToCategoryOnAuditLogs < ActiveRecord::Migration[5.2]
  def change
    rename_column :audit_logs, :type, :category
  end
end
