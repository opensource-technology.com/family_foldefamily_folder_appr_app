class AddAnotherAddressToCommunitySurveys < ActiveRecord::Migration[5.2]
  def change
    add_column :community_surveys, :location, :string, null: true
    add_column :community_surveys, :moo, :string, null: true
    add_column :community_surveys, :soimain, :string, null: true
    add_column :community_surveys, :soisum, :string, null: true
    add_column :community_surveys, :road, :string, null: true
  end
end
