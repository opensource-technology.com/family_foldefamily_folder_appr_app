class CreateAddrTambons < ActiveRecord::Migration[5.2]
  def change
    create_table :addr_tambons do |t|
      t.string :code, null: false
      t.string :label, null: false
      t.timestamps
    end

    add_index :addr_tambons, :code, unique: true
  end
end
