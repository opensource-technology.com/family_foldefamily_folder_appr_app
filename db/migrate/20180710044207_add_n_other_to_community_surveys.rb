class AddNOtherToCommunitySurveys < ActiveRecord::Migration[5.2]
  def change
    add_column :community_surveys, :nother, :integer, limit: 3, null: true
  end
end
