class AddColumnsToHouses < ActiveRecord::Migration[5.2]
  def change
    add_column :houses, :housestyletype_other, :string, null: true
    add_column :houses, :accident_other, :string, null: true
    add_column :houses, :disease_other, :string, null: true
  end
end
