class AddSurveyTimeToHealthSurveys < ActiveRecord::Migration[5.2]
  def change
    add_column :person_health_surveys, :surveyor_time, :integer, null: false, default: 1
    add_column :person_health_surveys, :surveyor_fullname, :string, null: true
  end
end
