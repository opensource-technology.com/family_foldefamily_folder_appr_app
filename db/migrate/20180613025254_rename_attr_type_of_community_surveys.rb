class RenameAttrTypeOfCommunitySurveys < ActiveRecord::Migration[5.2]
  def change
    rename_column :community_surveys, :type, :ctype
  end
end
