class ChangeDateTimeToDateToHouseHealthSurveys < ActiveRecord::Migration[5.2]
  def up
    change_column :house_health_surveys, :ndogvaccine_at, :date, null: true
    change_column :house_health_surveys, :ncatvaccine_at, :date, null: true
    change_column :house_health_surveys, :nmousevaccine_at, :date, null: true
    change_column :house_health_surveys, :nrabbitvaccine_at, :date, null: true
    change_column :house_health_surveys, :nsquirrelvaccine_at, :date, null: true
    change_column :house_health_surveys, :nmonkeyvaccine_at, :date, null: true
  end

  def down
    change_column :house_health_surveys, :ndogvaccine_at, :datetime, null: true
    change_column :house_health_surveys, :ncatvaccine_at, :datetime, null: true
    change_column :house_health_surveys, :nmousevaccine_at, :datetime, null: true
    change_column :house_health_surveys, :nrabbitvaccine_at, :datetime, null: true
    change_column :house_health_surveys, :nsquirrelvaccine_at, :datetime, null: true
    change_column :house_health_surveys, :nmonkeyvaccine_at, :datetime, null: true
  end
end
