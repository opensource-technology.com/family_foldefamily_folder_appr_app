class CreateRaces < ActiveRecord::Migration[5.2]
  def change
    create_table :races do |t|
      t.string :code, null: false
      t.string :label, null: false
      t.timestamps
    end
  end
end
