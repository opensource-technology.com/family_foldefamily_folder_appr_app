class AddUserLastUpdatedAtToHshv < ActiveRecord::Migration[6.0]
  def change
    add_column :person_health_surveys, :user_last_updated_at, :datetime, default: Time.now
    add_column :home_visits, :user_last_updated_at, :datetime, default: Time.now
  end
end
