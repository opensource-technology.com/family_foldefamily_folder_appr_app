class ChangePersonHealthSurveySize < ActiveRecord::Migration[5.2]
  def change
    change_column :person_health_surveys, :persontype, :string, limit: 2
  end
end
