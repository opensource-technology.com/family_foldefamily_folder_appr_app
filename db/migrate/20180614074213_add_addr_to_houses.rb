class AddAddrToHouses < ActiveRecord::Migration[5.2]
  def change
    add_reference :houses, :addr_changwat
    add_reference :houses, :addr_amphur
    add_reference :houses, :addr_tambon
  end
end
