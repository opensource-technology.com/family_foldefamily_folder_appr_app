class AddFamilyFolderToPeople < ActiveRecord::Migration[5.2]
  def change
    add_reference :people, :family_folder
  end
end
