class ChangeReferenceTypes < ActiveRecord::Migration[5.2]
  def change
    change_column :houses, :community_survey_id, :string
    change_column :family_folders, :house_id, :string
    change_column :people, :house_id, :string
    change_column :people, :family_folder_id, :string
    change_column :house_health_surveys, :family_folder_id, :string
    change_column :person_health_surveys, :family_folder_id, :string
    change_column :person_health_surveys, :person_id, :string
  end
end
