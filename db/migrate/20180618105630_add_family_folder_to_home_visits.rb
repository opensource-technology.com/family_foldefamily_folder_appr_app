class AddFamilyFolderToHomeVisits < ActiveRecord::Migration[5.2]
  def change
    add_reference :home_visits, :family_folder
  end
end
