class CreatePeople < ActiveRecord::Migration[5.2]
  def change
    create_table :people, id: :string, primary_key: :person_id,
                          limit: 13, null: false do |t|
      t.references :house
      t.string :hospcode, limit: 5, null: false
      t.date :datesurvey, null: false
      t.string :pid, limit: 13, null: true
      t.string :aid, limit: 25, null: true
      t.string :sex, limit: 1, null: false
      t.float :weight, null: true
      t.float :height, null: true
      t.float :waistline, null: true
      t.float :hips, null: true
      t.float :bmi, null: true
      t.string :bloodgroup, limit: 1, null: true
      t.string :bloodrh, limit: 1, null: true
      t.string :nameregis, limit: 1, null: true
      t.string :statusperson, limit: 1, null: true
      t.string :maritalstate, limit: 1, null: true
      t.string :educate, limit: 1, null: true
      t.string :career, limit: 1, null: true
      t.string :commustate, limit: 1, null: true
      t.string :maininscl, limit: 1, null: true
      t.string :familystate, limit: 1, null: true
      t.date :datemovin, null: true
      t.string :healthcheck, limit: 1, null: true
      t.string :oralhealth, limit: 1, null: true
      t.string :pracsick, limit: 1, null: true
      t.string :drugallergic, limit: 1, null: true
      t.string :detaildrug, null: true
      t.string :foodallergy, limit: 1, null: true
      t.string :detailfood, null: true
      t.string :riskbehav, limit: 1, null: true
      t.string :smoke, limit: 1, null: true
      t.string :allgoho, limit: 1, null: true
      t.string :energydrink, limit: 1, null: true
      t.string :addictive, limit: 1, null: true
      t.string :workdanger, limit: 1, null: true
      t.string :foodbehav, limit: 1, null: true
      t.string :overbmi, limit: 1, null: true
      t.string :geneticdiseas, limit: 1, null: true
      t.string :drugright, limit: 1, null: true
      t.string :dentprob, limit: 1, null: true
      t.string :nothealch, limit: 1, null: true
      t.string :sexrisk, limit: 1, null: true
      t.string :otherrisk, limit: 1, null: true
      t.string :exercis, limit: 1, null: true
      t.string :strain, limit: 1, null: true
      t.string :sumhealth, limit: 1, null: true
      t.date :u_date, null: false
      t.timestamps
    end
  end
end
