class AddForeignKeys < ActiveRecord::Migration[5.2]
  def change
    add_foreign_key :houses, :community_surveys, primary_key: 'vid', on_delete: :cascade
    add_foreign_key :family_folders, :houses, primary_key: 'hid', on_delete: :cascade
    add_foreign_key :house_health_surveys, :family_folders, primary_key: 'ffolder_id', on_delete: :cascade
    add_foreign_key :people, :houses, primary_key: 'hid', on_delete: :cascade
    add_foreign_key :people, :family_folders, primary_key: 'ffolder_id', on_delete: :cascade
    add_foreign_key :person_health_surveys, :people, primary_key: 'person_id', on_delete: :cascade
    add_foreign_key :person_health_surveys, :family_folders, primary_key: 'ffolder_id', on_delete: :cascade
    add_foreign_key :home_visits, :houses, primary_key: 'hid', on_delete: :cascade
    add_foreign_key :home_visits, :people, primary_key: 'person_id', on_delete: :cascade
    add_foreign_key :home_visits, :family_folders, primary_key: 'ffolder_id', on_delete: :cascade
  end
end
