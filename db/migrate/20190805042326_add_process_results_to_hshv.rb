class AddProcessResultsToHshv < ActiveRecord::Migration[5.2]
  def change
    add_reference :home_visits, :process_result
    add_reference :person_health_surveys, :process_result
  end
end
