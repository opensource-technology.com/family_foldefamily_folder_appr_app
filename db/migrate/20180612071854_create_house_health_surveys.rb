class CreateHouseHealthSurveys < ActiveRecord::Migration[5.2]
  def change
    create_table :house_health_surveys, id: :string, primary_key: :hhsurvey_id,
                                        limit: 13, null: false do |t|
      t.references :family_folder
      t.string :hospcode, limit: 5, null: false
      t.date :datesurvey, null: false
      t.string :explorsit, limit: 1, null: false
      t.string :transparent, limit: 1, null: false
      t.string :cleanliness, limit: 1, null: false
      t.string :wateruse, limit: 1, null: false
      t.string :foodsanit, limit: 1, null: false
      t.string :floodhouse, limit: 1, null: false
      t.string :homeenvirom, limit: 1, null: false
      t.string :animaldisease, limit: 1, null: false
      t.string :rat, limit: 1, null: true
      t.string :mosquito, limit: 1, null: true
      t.string :cockroach, limit: 1, null: true
      t.string :fly, limit: 1, null: true
      t.string :snake, limit: 1, null: true
      t.string :pets, limit: 1, null: false
      t.integer :ndog, limit: 3, null: true
      t.integer :ndogsterilize, limit: 3, null: true
      t.integer :ndogvaccine, limit: 3, null: true
      t.integer :ndogmicro, limit: 3, null: true
      t.integer :ncat, limit: 3, null: true
      t.integer :ncatsterilize, limit: 3, null: true
      t.integer :ncatvaccine, limit: 3, null: true
      t.integer :nmouse, limit: 3, null: true
      t.integer :nmousevaccine, limit: 3, null: true
      t.integer :nrabbit, limit: 3, null: true
      t.integer :nrabbitvaccine, limit: 3, null: true
      t.integer :nsquirrel, limit: 3, null: true
      t.integer :nsquirrelvaccine, limit: 3, null: true
      t.integer :nmonkey, limit: 3, null: true
      t.integer :nmonkeyvaccine, limit: 3, null: true
      t.integer :nbirds, limit: 3, null: true
      t.integer :nducks, limit: 3, null: true
      t.integer :ncows, limit: 3, null: true
      t.integer :npigs, limit: 3, null: true
      t.integer :nchicken, limit: 3, null: true
      t.integer :nfish, limit: 3, null: true
      t.integer :nbuffalo, limit: 3, null: true
      t.integer :ngoat, limit: 3, null: true
      t.integer :nreptile, limit: 3, null: true
      t.datetime :u_date, null: false
      t.timestamps
    end
  end
end
