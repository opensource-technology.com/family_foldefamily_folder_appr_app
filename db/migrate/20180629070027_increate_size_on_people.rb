class IncreateSizeOnPeople < ActiveRecord::Migration[5.2]
  def up
    change_column :people, :pracsick, :string, limit: 2
    change_column :people, :career, :string, limit: 2
  end

  def down
    change_column :people, :pracsick, :string, limit: 1
    change_column :people, :career, :string, limit: 1
  end
end
