class AllowPidToBeNullInProcessResults < ActiveRecord::Migration[5.2]
  def change
    change_column_null :process_results, :pid, true
  end
end
