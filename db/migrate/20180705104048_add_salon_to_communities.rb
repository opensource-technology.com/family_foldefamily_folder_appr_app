class AddSalonToCommunities < ActiveRecord::Migration[5.2]
  def change
    add_column :community_surveys, :nsalon, :string, limit: 3, null: true
  end
end
