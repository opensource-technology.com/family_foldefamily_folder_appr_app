class AddOthersToHomeVisits < ActiveRecord::Migration[5.2]
  def change
    add_column :home_visits, :surveyor_time, :integer, null: false, default: 1
    add_column :home_visits, :surveyor_fullname, :string, null: true
  end
end
