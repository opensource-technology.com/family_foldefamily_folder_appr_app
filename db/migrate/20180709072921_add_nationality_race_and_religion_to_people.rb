class AddNationalityRaceAndReligionToPeople < ActiveRecord::Migration[5.2]
  def change
    add_column :people, :nationality, :string, null: false, default: '099'
    add_column :people, :race, :string, null: true, default: '099'
    add_column :people, :religion, :string, null: true, default: '01'
  end
end
