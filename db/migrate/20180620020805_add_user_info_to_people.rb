class AddUserInfoToPeople < ActiveRecord::Migration[5.2]
  def change
    add_reference :people, :title, foreign_key: true, null: false
    add_column :people, :first_name, :string, null: false
    add_column :people, :last_name, :string, null: true
    add_column :people, :birth_date, :date
    add_column :people, :father_cid, :string, null: true
    add_column :people, :mother_cid, :string, null: true
    add_column :people, :spouse_cid, :string, null: true
  end
end
