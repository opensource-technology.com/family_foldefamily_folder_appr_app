class AddOthersToPersonHealthSurveys < ActiveRecord::Migration[5.2]
  def change
    add_column :person_health_surveys, :antenatalcare1_other, :string, null: true
    add_column :person_health_surveys, :edd_other, :string, null: true
    add_column :person_health_surveys, :comppreg_other, :string, null: true
    add_column :person_health_surveys, :drugpreg_other, :string, null: true
    add_column :person_health_surveys, :birthmethod_other, :string, null: true
    add_column :person_health_surveys, :antenatalcare2_other, :string, null: true
    add_column :person_health_surveys, :postcomp_other, :string, null: true
    add_column :person_health_surveys, :drugpost_other, :string, null: true
    add_column :person_health_surveys, :followpost_other, :string, null: true
    add_column :person_health_surveys, :food1_other, :string, null: true
    add_column :person_health_surveys, :servicecare_other, :string, null: true
    add_column :person_health_surveys, :healthdesk_other, :string, null: true
  end
end
