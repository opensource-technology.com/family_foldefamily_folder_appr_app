class AddColumnsToHouseHealthSurveys < ActiveRecord::Migration[5.2]
  def change
    add_column :house_health_surveys, :lightning, :string, limit: 1, null: true
    add_column :house_health_surveys, :drinking_water, :string, limit: 1, null: true
    add_column :house_health_surveys, :garbage, :string, limit: 1, null: true
    add_column :house_health_surveys, :toilet, :string, limit: 1, null: true
  end
end
