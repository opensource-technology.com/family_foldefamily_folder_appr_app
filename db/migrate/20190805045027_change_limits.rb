class ChangeLimits < ActiveRecord::Migration[5.2]
  def change
    change_column :house_health_surveys, :hhsurvey_id, :string, limit: 14
    change_column :person_health_surveys, :phsurvey_id, :string, limit: 14
    change_column :family_folders, :ffolder_id, :string, limit: 14
  end
end
