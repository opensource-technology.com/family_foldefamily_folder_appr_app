class AddContactToHouses < ActiveRecord::Migration[5.2]
  def change
    add_column :houses, :business_contact_fullname, :string, null: true
    add_column :houses, :business_contact_phonenumber, :string, null: true
  end
end
