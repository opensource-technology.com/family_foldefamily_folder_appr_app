class ChangeSizeDependsOnV1202 < ActiveRecord::Migration[5.2]
  def change
    # Community Surveys
    change_column :community_surveys, :vid, :string, limit: 14
    change_column :community_surveys, :riskentype11, :string, limit: 2
    change_column :community_surveys, :riskentype12, :string, limit: 2
    change_column :community_surveys, :riskentype13, :string, limit: 2

    # Houses
    change_column :houses, :businesstype, :string, limit: 3
  end
end
