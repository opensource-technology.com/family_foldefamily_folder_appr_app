class RenameTypeInDiseases < ActiveRecord::Migration[5.2]
  def change
    rename_column :diseases, :type, :dtype
    add_reference :diseases, :person, index: true
  end
end
