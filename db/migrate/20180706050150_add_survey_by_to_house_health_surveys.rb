class AddSurveyByToHouseHealthSurveys < ActiveRecord::Migration[5.2]
  def change
    add_column :house_health_surveys, :surveyor_fullname, :string, null: true
    add_column :house_health_surveys, :surveyor_position, :string, null: true
  end
end
