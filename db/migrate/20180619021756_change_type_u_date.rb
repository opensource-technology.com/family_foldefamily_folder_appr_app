class ChangeTypeUDate < ActiveRecord::Migration[5.2]
  def up
    change_column :home_visits, :u_date, :datetime
    change_column :people, :u_date, :datetime
    change_column :person_health_surveys, :u_date, :datetime
  end

  def down
    change_column :home_visits, :u_date, :date
    change_column :people, :u_date, :date
    change_column :person_health_surveys, :u_date, :date
  end

  # def change
  #   change_column :home_visits, :u_date, :datetime
  #   change_column :people, :u_date, :datetime
  #   change_column :person_health_surveys, :u_date, :datetime
  # end
end
