class CreatePersonHealthSurveys < ActiveRecord::Migration[5.2]
  def change
    create_table :person_health_surveys, id: :string, primary_key: :phsurvey_id,
                                         limit: 13, null: false do |t|
      t.references :person
      t.references :family_folder
      t.string :hospcode, limit: 5, null: false
      t.date :datesurvey, null: false
      t.string :persontype, limit: 1, null: false
      t.string :firstpreg, limit: 1, null: true
      t.string :antenatalcare1, limit: 1, null: true
      t.string :edd, limit: 1, null: true
      t.string :comppreg, limit: 1, null: true
      t.string :drugpreg, limit: 1, null: true
      t.string :birthmethod, limit: 1, null: true
      t.string :antenatalcare2, limit: 1, null: true
      t.string :postcomp, limit: 1, null: true
      t.string :drugpost, limit: 1, null: true
      t.string :followpost, limit: 1, null: true
      t.string :birthwright, limit: 1, null: true
      t.string :thyroid, limit: 1, null: true
      t.string :bcg, limit: 1, null: true
      t.string :food1, limit: 1, null: true
      t.string :vaccine1, limit: 1, null: true
      t.string :development, limit: 1, null: true
      t.string :weightheight, limit: 1, null: true
      t.string :food2, limit: 1, null: true
      t.string :vaccine2, limit: 1, null: true
      t.string :sexbehav1, limit: 1, null: true
      t.string :substabuse1, limit: 1, null: true
      t.string :healthscr, limit: 1, null: true
      t.string :sexbehav2, limit: 1, null: true
      t.string :substabuse2, limit: 1, null: true
      t.string :riskscr1, limit: 1, null: true
      t.string :papsmear, limit: 1, null: true
      t.string :breastscr, limit: 1, null: true
      t.string :servicecare, limit: 1, null: true
      t.string :adl, limit: 1, null: true
      t.string :oldtype, limit: 1, null: true
      t.string :riskscr2, limit: 1, null: true
      t.string :oralcheck, limit: 1, null: true
      t.string :movement, limit: 1, null: true
      t.string :hearing, limit: 1, null: true
      t.string :vision, limit: 1, null: true
      t.string :intellect, limit: 1, null: true
      t.string :learning, limit: 1, null: true
      t.string :autistic, limit: 1, null: true
      t.string :behavior, limit: 1, null: true
      t.string :help, limit: 1, null: true
      t.string :regiscripp, limit: 1, null: true
      t.string :healthdesk, limit: 1, null: true
      t.date :u_date
      t.timestamps
    end
  end
end
