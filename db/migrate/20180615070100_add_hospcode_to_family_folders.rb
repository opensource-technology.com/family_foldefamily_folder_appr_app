class AddHospcodeToFamilyFolders < ActiveRecord::Migration[5.2]
  def change
    add_column :family_folders, :hospcode, :string, limit: 5, null: false
  end
end
