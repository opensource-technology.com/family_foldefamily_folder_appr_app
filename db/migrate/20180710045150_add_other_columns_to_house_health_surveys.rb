class AddOtherColumnsToHouseHealthSurveys < ActiveRecord::Migration[5.2]
  def change
    add_column :house_health_surveys, :ndogvaccine_at, :datetime, null: true
    add_column :house_health_surveys, :ncatvaccine_at, :datetime, null: true
    add_column :house_health_surveys, :nmousevaccine_at, :datetime, null: true
    add_column :house_health_surveys, :nrabbitvaccine_at, :datetime, null: true
    add_column :house_health_surveys, :nsquirrelvaccine_at, :datetime, null: true
    add_column :house_health_surveys, :nmonkeyvaccine_at, :datetime, null: true
  end
end
