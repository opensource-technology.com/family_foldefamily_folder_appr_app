class CreateSites < ActiveRecord::Migration[5.2]
  def change
    create_table :sites do |t|
      t.string :hospcode, limit: 5, null: false
      t.integer :cs_no, default: 0
      t.integer :h_no, default: 0
      t.integer :ff_no, default: 0
      t.integer :hhs_no, default: 0
      t.integer :phs_no, default: 0
      t.integer :p_no, default: 0
      t.integer :hv_no, default: 0
      t.timestamps
    end
  end
end
