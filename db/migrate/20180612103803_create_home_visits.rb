class CreateHomeVisits < ActiveRecord::Migration[5.2]
  def change
    create_table :home_visits, id: :string, primary_key: :hvid,
                               limit: 15, null: false do |t|
      t.references :person
      t.references :house
      t.string :hospcode, limit: 5, null: false
      t.date :datesurvey, null: false
      t.string :gtype, limit: 4, null: true
      t.float :btemp, null: true
      t.integer :pr, limit: 3, null: true
      t.integer :rr, limit: 3, null: true
      t.float :weight, null: true
      t.float :height, null: true
      t.integer :waist_cm, limit: 3, null: true
      t.float :bmi, null: true
      t.integer :hip_cm, limit: 3, null: true
      t.float :dtx, null: true
      t.integer :sbp, limit: 3, null: true
      t.integer :dbp, limit: 3, null: true
      t.string :healthscr, limit: 1, null: true
      t.string :mentalscn, limit: 1, null: true
      t.string :healthadvice, limit: 1, null: true
      t.string :develope, limit: 1, null: true
      t.string :demonbodychk, limit: 1, null: true
      t.string :demonhealthpro, limit: 1, null: true
      t.string :behaviormodifi, limit: 1, null: true
      t.string :bodychk, limit: 1, null: true
      t.string :wiping, limit: 1, null: true
      t.string :eyechek, limit: 1, null: true
      t.string :drugplan, limit: 1, null: true
      t.string :oralhyg, limit: 1, null: true
      t.string :uterus, limit: 1, null: true
      t.string :breastscr, limit: 1, null: true
      t.string :firstaid, limit: 1, null: true
      t.string :blood, limit: 1, null: true
      t.string :inject, limit: 1, null: true
      t.string :specimen, limit: 1, null: true
      t.string :wound, limit: 1, null: true
      t.date :u_date, null: false
      t.timestamps
    end
  end
end
