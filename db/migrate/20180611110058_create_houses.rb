class CreateHouses < ActiveRecord::Migration[5.2]
  def change
    create_table :houses, id: :string, primary_key: :hid, limit: 14, null: false do |t|
      t.references :community_survey
      t.string :hospcode, limit: 5, null: false
      t.string :house_id, limit: 11, null: true
      t.date :datesurvey, null: false
      t.string :namesurvey, null: false
      t.string :positionsurvey, null: false
      t.string :house, limit: 75, null: true
      t.string :soisum, null: true
      t.string :soimain, null: true
      t.string :road, null: true
      t.string :villaname, null: true
      t.string :catm, limit: 8, null: false
      t.float :latitude, null: true
      t.float :longitude, null: true
      t.string :postcode, limit: 5, null: true
      t.string :phonenumber, null: true
      t.string :housestyletype, limit: 1, null: false
      t.string :housetype, limit: 1, null: false
      t.string :houseutili, limit: 1, null: true
      t.string :nperson, limit: 3, null: true
      t.string :businesstype, limit: 1, null: true
      t.string :namebusiness, null: true
      t.string :ncustomer, limit: 3, null: true
      t.string :accident, limit: 1, null: true
      t.string :disease, limit: 1, null: true
      t.datetime :u_date, null: false
      t.timestamps
    end
  end
end
