class AddAddrToCommunitySurveys < ActiveRecord::Migration[5.2]
  def change
    add_reference :community_surveys, :addr_changwat
    add_reference :community_surveys, :addr_amphur
    add_reference :community_surveys, :addr_tambon
  end
end
