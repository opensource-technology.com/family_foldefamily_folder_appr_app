class AddPidsToPeople < ActiveRecord::Migration[5.2]
  def change
    rename_column :people, :father_cid, :pid_father
    rename_column :people, :mother_cid, :pid_mother
    rename_column :people, :spouse_cid, :pid_spouse

    add_column :people, :maininscl_type, :string, limit: 1, null: true
  end
end
