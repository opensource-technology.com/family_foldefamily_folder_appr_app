class CreateDiseases < ActiveRecord::Migration[5.2]
  def change
    create_table :diseases do |t|
      t.string :name, null: false
      t.string :type, limit: 1, null: false
      t.datetime :sick_at, null: false
      t.string :treat_at, null: true
      t.timestamps
    end
  end
end
