class ChangeDateTimeToDateToDiseases < ActiveRecord::Migration[5.2]
  def up
    change_column :diseases, :sick_at, :date, null: true
  end

  def down
    change_column :diseases, :sick_at, :datetime, null: true
  end
end
