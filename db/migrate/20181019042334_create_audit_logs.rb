class CreateAuditLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :audit_logs do |t|
      t.string :user
      t.string :type
      t.string :action
      t.timestamps
    end
  end
end
