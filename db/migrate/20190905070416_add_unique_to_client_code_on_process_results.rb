class AddUniqueToClientCodeOnProcessResults < ActiveRecord::Migration[5.2]
  def change
    add_index :process_results, [:client_code, :process_type], unique: true
  end
end
