class ChangeReferrenceTypeOnHomeVisits < ActiveRecord::Migration[5.2]
  def change
    change_column :home_visits, :house_id, :string
    change_column :home_visits, :person_id, :string
    change_column :home_visits, :family_folder_id, :string
  end
end
