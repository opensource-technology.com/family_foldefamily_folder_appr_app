class AddToiletHygienicToHouseHealthSurveys < ActiveRecord::Migration[5.2]
  def change
    add_column :house_health_surveys, :toilet_hygienic, :string, limit: 1, null: true
  end
end
