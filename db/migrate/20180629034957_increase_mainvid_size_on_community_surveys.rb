class IncreaseMainvidSizeOnCommunitySurveys < ActiveRecord::Migration[5.2]
  def up
    change_column :community_surveys, :mainvid, :string, limit: 255
  end

  def down
    change_column :community_surveys, :mainvid, :string, limit: 8
  end
end
