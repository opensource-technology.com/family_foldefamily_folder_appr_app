class CreateProcessResults < ActiveRecord::Migration[5.2]
  def change
    create_table :process_results do |t|
      t.integer :process_type, null: false, limit: 1
      t.string :pid, null: false, limit: 13
      t.string :code_check, null: true, limit: 20
      t.string :client_code, null: false, limit: 15
      t.string :hospcode, null: false, limit: 5
      t.integer :status, null: false, limit: 1, default: 1
      t.string :status_description
      t.integer :sent_status, null: false, limit: 1, default: 1
      t.string :sent_errors
      t.timestamps
    end
  end
end
