require 'hshv'
class HshvSendDataJob < ApplicationJob
  
  include HSHV::Process
  queue_as :default

  def perform
    puts "[Send] Prepare data"
    processor([load_home_visit, load_health_survey, load_health_survey_token_expired, load_home_visit_token_expired]) do |items|
      send_process items
    end
  end

  def send_process(list)
    c = HSHV::SendCommand.new
    # puts "Send data to cloud"
    HSHV::Commander.call c, list: list
  end

  def processor(types)
    types.each do |data|
      items = data.map { |i| HSHV::Item.new i }
      yield items if block_given?
    end
  end
end
