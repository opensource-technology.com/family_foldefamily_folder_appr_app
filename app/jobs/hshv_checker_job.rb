require 'hshv'
class HshvCheckerJob < ApplicationJob
  include HSHV::Process
  queue_as :default

  def perform
    puts "[CHECK] Prepare data"
    results = load_process_result
    items = results.map { |r| HSHV::Item.new(r) }
    c = HSHV::CheckCommand.new
    HSHV::Commander.call c, list: items
  end
end
