function initHouseInfo() {
  const latitude = parseFloat($('input[name="latitude"]').val());
  if (Number.isNaN(latitude)) $('input[name="latitude"]').val('');
  else $('input[name="latitude"]').val(latitude.toFixed(6));
  
  const longitude = parseFloat($('input[name="longitude"]').val());
  if (Number.isNaN(longitude)) $('input[name="longitude"]').val('');
  else $('input[name="longitude"]').val(longitude.toFixed(6));
}

$(document).ready(() => {
  initHouseInfo();
});

$(document).on('turbolinks:load', () => {
  initHouseInfo();
});
