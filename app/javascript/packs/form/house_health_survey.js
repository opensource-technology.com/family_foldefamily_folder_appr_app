function initHouseHealthSurveys() {
  if (!$('.form-container').length) {return;}

  var el = [
    'datesurvey',
    'ncatvaccine_at',
    'ndogvaccine_at',
    'nmousevaccine_at',
    'nsquirrelvaccine_at',
    'nrabbitvaccine_at',
    'nmonkeyvaccine_at'
  ];

  el.forEach(function(item) {
    enableCalendar(`#${item}`);
  });

  // toilet
  $('input[name="house_health_survey[toilet]"]:checked').val() === '2' || $('#house_health_survey_toilet_hygienic').hide();
  $('input[name="house_health_survey[toilet]"]').on('change', function(event) {
    // $('input[name="house_health_survey[toilet_hygienic]"]:checked').removeAttr('checked');
    if (event.target.value === '2') {
      $('#house_health_survey_toilet_hygienic').show('fast');
      // set required
      $('input[name="house_health_survey[toilet_hygienic]"]').attr('required', 'true');
    } else {
      $('#house_health_survey_toilet_hygienic').hide('fast')
      // unset required
      $('input[name="house_health_survey[toilet_hygienic]"]').removeAttr('required');
      $('input[name="house_health_survey[toilet_hygienic]"]').removeAttr('checked');
    }
  });



  //////////// explorsit
  $('input[name="house_health_survey[explorsit]"]:checked').val() === '3' || $('#explorsit_reason').hide();
  $('input[name="house_health_survey[explorsit]"]').on('change', function(event) {
    if (event.target.value === '3') {
      $('#explorsit_reason').show('fast');
    } else {
      $('input[name="house_health_survey[explorsit_reason]"]:checked').removeAttr('checked');
      $('#explorsit_reason').hide('fast')
    }

  });
  //////////// animaldisease
  $('input[name="house_health_survey[animaldisease]"]:checked').val() === '2' || $('#house_health_survey_animaldisease').hide();
  $('input[name="house_health_survey[animaldisease]"]').on('change', function(event) {
    $('input[name="house_health_survey[rat]"]:checked').removeAttr('checked');
    $('input[name="house_health_survey[mosquito]"]:checked').removeAttr('checked');
    $('input[name="house_health_survey[cockroach]"]:checked').removeAttr('checked');
    $('input[name="house_health_survey[fly]"]:checked').removeAttr('checked');
    $('input[name="house_health_survey[snake]"]:checked').removeAttr('checked');
    event.target.value ==='2'? $('#house_health_survey_animaldisease').show('fast'):$('#house_health_survey_animaldisease').hide('fast');

  });

  //////////// pets
  $('input[name="house_health_survey[pets]"]:checked').val() === '2' || $('#house_health_survey_pets').hide();
  $('input[name="house_health_survey[pets]"]').on('change', function(event) {
    $('input[name="house_health_survey[ndog]"]').val('');
    $('input[name="house_health_survey[ndogsterilize]"]').val('');
    $('input[name="house_health_survey[ndogvaccine]"]').val('');
    $('input[name="house_health_survey[ndogmicro]"]').val('');
    $('input[name="house_health_survey[ncat]"]').val('');
    $('input[name="house_health_survey[ncatsterilize]"]').val('');
    $('input[name="house_health_survey[ncatvaccine]"]').val('');
    $('input[name="house_health_survey[nmouse]"]').val('');
    $('input[name="house_health_survey[nmousevaccine]"]').val('');
    $('input[name="house_health_survey[nrabbit]"]').val('');
    $('input[name="house_health_survey[nrabbitvaccine]"]').val('');
    $('input[name="house_health_survey[nsquirrel]"]').val('');
    $('input[name="house_health_survey[nsquirrelvaccine]"]').val('');
    $('input[name="house_health_survey[nmonkey]"]').val('');
    $('input[name="house_health_survey[nmonkeyvaccine]"]').val('');
    $('input[name="house_health_survey[nbirds]"]').val('');
    $('input[name="house_health_survey[nducks]"]').val('');
    $('input[name="house_health_survey[ncows]"]').val('');
    $('input[name="house_health_survey[npigs]"]').val('');
    $('input[name="house_health_survey[nchicken]"]').val('');
    $('input[name="house_health_survey[nfish]"]').val('');
    $('input[name="house_health_survey[nbuffalo]"]').val('');
    $('input[name="house_health_survey[ngoat]"]').val('');
    $('input[name="house_health_survey[nreptile]"]').val('');

    $('input[name="house_health_survey[ndogvaccine_at]"]').val('');
    $('input[name="house_health_survey[ncatvaccine_at]"]').val('');
    $('input[name="house_health_survey[nmousevaccine_at]"]').val('');
    $('input[name="house_health_survey[nrabbitvaccine_at]"]').val('');
    $('input[name="house_health_survey[nsquirrelvaccine_at]"]').val('');
    $('input[name="house_health_survey[nmonkeyvaccine_at]"]').val('');
    event.target.value ==='2'? $('#house_health_survey_pets').show('fast'):$('#house_health_survey_pets').hide('fast');
  });
}

$(document).ready(() => {
  initHouseHealthSurveys();
});

$(document).on('turbolinks:load', () => {
  initHouseHealthSurveys();
});
