function initHomeVisits() {
  if (!$('.form-container').length) {return;}
  enableCalendar('#datesurvey');
  $('.ui.selection.dropdown').dropdown({
    clearable: true
  });
  $('.form-container').submit(function (event) {
    if ($('input[type="radio"]:checked').length == 0) {
      $('input[type="radio"]:first').focus();
      return false;
      // event.preventDefault();
    }
  });
  ///// BMI
  const setBMI= () => {
    if($('input[name="home_visit[weight]"]').val()&&$('input[name="home_visit[height]"]').val()){
      const weight =$('input[name="home_visit[weight]"]').val();
      const height =$('input[name="home_visit[height]"]').val();
      const isHeight = (height/100);
      const isBMI = weight/(isHeight*isHeight)
      $('input[name="home_visit[bmi]"]').val(isBMI.toFixed(2));
    }else{
      $('input[name="home_visit[bmi]"]').val(null);
    }
  };
  $('input[name="home_visit[weight]"]').on('change', function(event) {
    setBMI();
  });
  $('input[name="home_visit[height]"]').on('change', function(event) {
    setBMI();
  });
}

$(document).ready(() => {
  initHomeVisits();
});

$(document).on('turbolinks:load', () => {
  initHomeVisits();
});
