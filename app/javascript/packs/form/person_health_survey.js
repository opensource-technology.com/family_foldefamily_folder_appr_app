function initPersonHealthSurveys() {

  if (!$('.form-container').length) {return;}
  enableCalendar('#datesurvey');
  $('.ui.selection.dropdown').dropdown({
    clearable: true
  });
  //////////// persontype
  const persontype =$('select[name="person_health_survey[persontype]"]');
  for ( i = 1; i <= 9; i++) {
    persontype.val() === `0${i}` || $(`#person_types_0${i}`).hide();
  }

  persontype.on('change', function(event) {
    clear_val_person_type();
    let status_edit = true;
    for ( i = 1; i <= 9; i++) {
      if(!$(`#person_types_0${i}`).is(':hidden')){
        show_person_type(`0${i}`,event.target.value);
        status_edit = false;
        break;
      }
    }
    status_edit &&  $(`#person_types_${event.target.value}`).show('fast');
  });
  const show_person_type = (indexHide,indexShow) => {
    $(`#person_types_${indexHide}`).hide('fast', () => {
      $(`#person_types_${indexShow}`).show('fast');
    })
    return true;
  }
  const clear_val_person_type = () =>{
    $('input[name="person_health_survey[firstpreg]"]:checked').removeAttr('checked');
    $('select[name="person_health_survey[antenatalcare1]"]').val('');
    $('input[name="person_health_survey[edd]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[comppreg]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[drugpreg]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[birthmethod]"]:checked').removeAttr('checked');
    $('select[name="person_health_survey[antenatalcare2]"]').val('');
    $('input[name="person_health_survey[postcomp]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[drugpost]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[followpost]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[birthwright]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[thyroid]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[bcg]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[food1]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[vaccine1]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[development]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[weightheight]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[food2]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[vaccine2]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[sexbehav1]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[substabuse1]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[healthscr]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[sexbehav2]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[substabuse2]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[riskscr1]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[papsmear]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[breastscr]"]:checked').removeAttr('checked');
    $('select[name="person_health_survey[servicecare]"]').val('');
    $('input[name="person_health_survey[adl]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[oldtype]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[riskscr2]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[oralcheck]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[movement]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[hearing]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[vision]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[intellect]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[learning]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[autistic]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[behavior]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[help]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[regiscripp]"]:checked').removeAttr('checked');
    $('input[name="person_health_survey[healthdesk]"]:checked').removeAttr('checked');

    $('#edd_other').hide();
    $('#comppreg_other').hide();
    $('#drugpreg_other').hide();
    $('#antenatalcare1_other').hide();
    $('input[name="person_health_survey[edd_other]"]').val('');
    $('input[name="person_health_survey[comppreg_other]"]').val('');
    $('input[name="person_health_survey[drugpreg_other]"]').val('');
    $('input[name="person_health_survey[antenatalcare1_other]"]').val('');


    $('#birthmethod_other').hide();
    $('#antenatalcare2_other').hide();
    $('#postcomp_other').hide();
    $('#drugpost_other').hide();
    $('#followpost_other').hide();
    $('input[name="person_health_survey[birthmethod_other]"]').val('');
    $('input[name="person_health_survey[antenatalcare2_other]"]').val('');
    $('input[name="person_health_survey[postcomp_other]"]').val('');
    $('input[name="person_health_survey[drugpost_other]"]').val('');
    $('input[name="person_health_survey[followpost_other]"]').val('');

    $('#food1_other').hide();
    $('input[name="person_health_survey[food1_other]"]').val('');

    $('#servicecare_other').hide();
    $('input[name="person_health_survey[servicecare_other]"]').val('');

    $('#healthdesk_other').hide();
    $('input[name="person_health_survey[healthdesk_other]"]').val('');
  }

  ///// edd_other
  $('input[name="person_health_survey[edd]"]:checked').val() === '1' || $('#edd_other').hide();
  $('input[name="person_health_survey[edd]"]').on('change', function (event) {
    console.log('data in ',event.target.value)
    if(event.target.value ==='1'){
      $('#edd_other').show('fast');
    }else{
      $('input[name="person_health_survey[edd_other]"]').val('');
      $('#edd_other').hide('fast');
    }
  });

  ///// comppreg_other
  $('input[name="person_health_survey[comppreg]"]:checked').val() === '1' || $('#comppreg_other').hide();
  $('input[name="person_health_survey[comppreg]"]').on('change', function (event) {
    console.log('data in ',event.target.value)
    if(event.target.value ==='1'){
      $('#comppreg_other').show('fast');
    }else{
      $('input[name="person_health_survey[comppreg_other]"]').val('');
      $('#comppreg_other').hide('fast');
    }
  });

  ///// drugpreg_other
  $('input[name="person_health_survey[drugpreg]"]:checked').val() === '1' || $('#drugpreg_other').hide();
  $('input[name="person_health_survey[drugpreg]"]').on('change', function (event) {
    console.log('data in ',event.target.value)
    if(event.target.value ==='1'){
      $('#drugpreg_other').show('fast');
    }else{
      $('input[name="person_health_survey[drugpreg_other]"]').val('');
      $('#drugpreg_other').hide('fast');
    }
  });

  ///// antenatalcare1_other
  $('select[name="person_health_survey[antenatalcare1]"]').val() === '5' || $('#antenatalcare1_other').hide();
  $('select[name="person_health_survey[antenatalcare1]"]').on('change', function (event) {
    console.log('data in ',event.target.value)
    if(event.target.value ==='5'){
      $('#antenatalcare1_other').show('fast');
    }else{
      $('input[name="person_health_survey[antenatalcare1_other]"]').val('');
      $('#antenatalcare1_other').hide('fast');
    }
  });

  ///// birthmethod_other
  $('input[name="person_health_survey[birthmethod]"]:checked').val() === '3' || $('#birthmethod_other').hide();
  $('input[name="person_health_survey[birthmethod]"]').on('change', function (event) {
    console.log('data in ',event.target.value)
    if(event.target.value ==='3'){
      $('#birthmethod_other').show('fast');
    }else{
      $('input[name="person_health_survey[birthmethod_other]"]').val('');
      $('#birthmethod_other').hide('fast');
    }
  });

  ///// antenatalcare2_other
  $('select[name="person_health_survey[antenatalcare2]"]').val() === '5' || $('#antenatalcare2_other').hide();
  $('select[name="person_health_survey[antenatalcare2]"]').on('change', function (event) {
    console.log('data in ',event.target.value)
    if(event.target.value ==='5'){
      $('#antenatalcare2_other').show('fast');
    }else{
      $('input[name="person_health_survey[antenatalcare2_other]"]').val('');
      $('#antenatalcare2_other').hide('fast');
    }
  });

  ///// postcomp_other
  $('input[name="person_health_survey[postcomp]"]:checked').val() === '1' || $('#postcomp_other').hide();
  $('input[name="person_health_survey[postcomp]"]').on('change', function (event) {
    console.log('data in ',event.target.value)
    if(event.target.value ==='1'){
      $('#postcomp_other').show('fast');
    }else{
      $('input[name="person_health_survey[postcomp_other]"]').val('');
      $('#postcomp_other').hide('fast');
    }
  });

  ///// drugpost_other
  $('input[name="person_health_survey[drugpost]"]:checked').val() === '1' || $('#drugpost_other').hide();
  $('input[name="person_health_survey[drugpost]"]').on('change', function (event) {
    console.log('data in ',event.target.value)
    if(event.target.value ==='1'){
      $('#drugpost_other').show('fast');
    }else{
      $('input[name="person_health_survey[drugpost_other]"]').val('');
      $('#drugpost_other').hide('fast');
    }
  });

  ///// followpost_other
  $('input[name="person_health_survey[followpost]"]:checked').val() === '1' || $('#followpost_other').hide();
  $('input[name="person_health_survey[followpost]"]').on('change', function (event) {
    console.log('data in ',event.target.value)
    if(event.target.value ==='1'){
      $('#followpost_other').show('fast');
    }else{
      $('input[name="person_health_survey[followpost_other]"]').val('');
      $('#followpost_other').hide('fast');
    }
  });

  ///// food1_other
  $('input[name="person_health_survey[food1]"]:checked').val() === '3' || $('#food1_other').hide();
  $('input[name="person_health_survey[food1]"]').on('change', function (event) {
    console.log('data in ',event.target.value)
    if(event.target.value ==='3'){
      $('#food1_other').show('fast');
    }else{
      $('input[name="person_health_survey[food1_other]"]').val('');
      $('#food1_other').hide('fast');
    }
  });

  ///// servicecare_other
  $('select[name="person_health_survey[servicecare]"]').val() === '5' || $('#servicecare_other').hide();
  $('select[name="person_health_survey[servicecare]"]').on('change', function (event) {
    console.log('data in ',event.target.value)
    if(event.target.value ==='5'){
      $('#servicecare_other').show('fast');
    }else{
      $('input[name="person_health_survey[servicecare_other]"]').val('');
      $('#servicecare_other').hide('fast');
    }
  });

  ///// healthdesk_other
  $('input[name="person_health_survey[healthdesk]"]:checked').val() === '3' || $('#healthdesk_other').hide();
  $('input[name="person_health_survey[healthdesk]"]').on('change', function (event) {
    console.log('data in ',event.target.value)
    if(event.target.value ==='3'){
      $('#healthdesk_other').show('fast');
    }else{
      $('input[name="person_health_survey[healthdesk_other]"]').val('');
      $('#healthdesk_other').hide('fast');
    }
  });
}

$(document).ready(() => {
  initPersonHealthSurveys();
});

$(document).on('turbolinks:load', () => {
  initPersonHealthSurveys();
});