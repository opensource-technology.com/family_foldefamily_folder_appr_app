function initCommunities() {
  enableCalendar('#datesurvey');
  load_address('community_survey');
  //////////// ctype
  const ctype =$('input[name="community_survey[ctype]"]:checked');
  ctype.val() === '1' || $('#ctype_subtype_main').hide();
  ctype.val() === '2' || $('#ctype_subtype_sub').hide();
  $('input[name="community_survey[ctype]"]').on('change', function(event) {
    $('input[name="community_survey[subtype]"]:checked').removeAttr('checked');
    $('select[name="community_survey[mainvid]"]').val(''); 
    event.target.value ==='1'? $('#ctype_subtype_sub').hide('fast', function() {
      $('#mainvid').hide(); 
      $('#ctype_subtype_main').show('fast');
    }):
    $('#ctype_subtype_main').hide('fast', function() {
      $('#ctype_subtype_sub').show('fast');
    });
  });
  
  //////////// riskenvironment
  $('input[name="community_survey[riskenvironment]"]:checked').val() === '1' || $('#riskentype').hide();
  $('input[name="community_survey[riskenvironment]"]').on('change', function(event) {
    $('input[name="community_survey[riskentype11]"]:checked').removeAttr('checked');
    $('input[name="community_survey[riskentype12]"]:checked').removeAttr('checked');
    $('input[name="community_survey[riskentype13]"]:checked').removeAttr('checked');
    event.target.value ==='1'? $('#riskentype').show('fast'):$('#riskentype').hide('fast');
  });
  
  //////////// mainvid    
  $('input[name="community_survey[subtype]"]:checked').val() === '21' || $('#mainvid').hide(); 
  $('input[name="community_survey[subtype]"]').on('change', function(event) { 
    $('select[name="community_survey[mainvid]"]').val(''); 
    $('input[name="community_survey[subtype]"]:checked').val() === '21'? $('#mainvid').show('fast'):$('#mainvid').hide('fast'); 
  });
}

$(document).ready(() => {
  console.log('x')
  initCommunities();
});

$(document).on('turbolinks:load', () => {
  console.log('y')
  initCommunities();
});
