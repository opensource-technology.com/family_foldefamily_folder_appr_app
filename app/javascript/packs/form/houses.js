function initHouses() {
  enableCalendar('#datesurvey');
  $('.ui.selection.dropdown').dropdown({
    clearable: true
  });
  load_address('house');

  ////house_business
  $('input[name="house[houseutili]"]:checked').val() === '2' || $('input[name="house[houseutili]"]:checked').val() === '3' || $('#house_business').hide();
  $('input[name="house[houseutili]"]').on('change', function(event) {
    if(event.target.value !=='1'){
      $('#house_business').show('fast');
    }else{
      $('select[name="house[businesstype]"]').prop('selectedIndex',0);
      $('input[name="house[namebusiness]"]').val('');
      $('input[name="house[ncustomer]"]').val('');
      $('input[name="house[accident]"]:checked').removeAttr('checked');
      $('input[name="house[disease]"]:checked').removeAttr('checked');
      $('input[name="house[housestyletype_other]"]').val(''); 
      $('input[name="house[disease_other]"]').val(''); 
      $('input[name="house[accident_other]"]').val(''); 

      $('.accident_other').hide();
      $('.disease_other').hide(); 
      $('.housestyletype_other').hide();

      $('#house_business').hide('fast');
    }
  });

  ////housestyletype_other
  $('select[name="house[businesstype]"]').val() || $('.housestyletype_other').hide();
  $('select[name="house[businesstype]"]').on('change', function(event) {
    if(event.target.value){
      $('.housestyletype_other').show('fast');
    }else{
      $('input[name="house[housestyletype_other]"]').val(''); 
      $('.housestyletype_other').hide('fast');
    }
  });

  ////disease_other
  $('input[name="house[disease]"]:checked').val() == 2 || $('.disease_other').hide();
  $('input[name="house[disease]"]').on('change', function(event) {
    console.log('data in ',$('input[name="house[disease]"]:checked').val())
    if(event.target.value == 2){
      $('.disease_other').show('fast');
    }else{
      $('input[name="house[disease_other]"]').val(''); 
      $('.disease_other').hide('fast');
    }
  });

  
  ////accident_other
  $('input[name="house[accident]"]:checked').val() == 2 || $('.accident_other').hide();
  $('input[name="house[accident]"]').on('change', function(event) {
    // console.log('data in ',$('input[name="house[accident]"]:checked').val())
    if(event.target.value == 2){
      $('.accident_other').show('fast');
    }else{
      $('input[name="house[accident_other]"]').val(''); 
      $('.accident_other').hide('fast');
    }
  });

  ////set decimal 6
  const latitude = parseFloat($('input[name="house[latitude]"]').val());
  if (Number.isNaN(latitude)) $('input[name="house[latitude]"]').val('');
  else $('input[name="house[latitude]"]').val(latitude.toFixed(6));

  $('input[name="house[latitude]"]').on('change', function(event) {
    if (isNaN(this.value) || this.value == '') 
      return this.value = ''
    this.value = parseFloat(this.value).toFixed(6);
  });

  const longitude = parseFloat($('input[name="house[longitude]"]').val());
  if (Number.isNaN(longitude)) $('input[name="house[longitude]"]').val('');
  else $('input[name="house[longitude]"]').val(longitude.toFixed(6));
  $('input[name="house[longitude]"]').on('change', function(event) {
    if (isNaN(this.value) || this.value == '') 
      return this.value = ''
    this.value = parseFloat(this.value).toFixed(6);
  });
}

$(document).ready(() => {
  initHouses();
});

$(document).on('turbolinks:load', () => {
  initHouses();
});
