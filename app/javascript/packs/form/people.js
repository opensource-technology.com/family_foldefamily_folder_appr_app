function initPeople() {
  $("#new_disease_button").on('click', function (event) {
    $.ajax({
      url: "/diseases/new"
    }).done(function (html) {
      console.log(html);
      $("#disease_contents").append(html);
    });
    event.preventDefault();
  });

  enableCalendar('#peole_datesurvey');
  enableCalendar('#datemovin');
  enableCalendar('.ui.input.sickat');
  enableCalendar('#birthdate', {
    onChange: (date, text, mode) => {
      let currentDate = new Date();
      let selectedDate = new Date(date);
      let age = currentDate.getFullYear() - selectedDate.getFullYear();
      let m = currentDate.getMonth() - selectedDate.getMonth();
      $('input[name="person[age]"]').val(age);
    }
  });

  $('.ui.selection.dropdown').dropdown({
    clearable: true
  });

  ///// drugallergic
  $('input[name="person[drugallergic]"]:checked').val() === '3' || $('#detail_drug').hide();
  $('input[name="person[drugallergic]"]').on('change', function (event) {
    if(event.target.value ==='3'){
      $('#detail_drug').show('fast');
    }else{
      $('input[name="person[detaildrug]"]').val('');
      $('#detail_drug').hide('fast');
    }
  });

  ///// foodallergy
  $('input[name="person[foodallergy]"]:checked').val() === '2' || $('#detail_food').hide();
  $('input[name="person[foodallergy]"]').on('change', function (event) {
    if(event.target.value ==='2'){
      $('#detail_food').show('fast');
    }else{
      $('input[name="person[detailfood]"]').val('');
      $('#detail_food').hide('fast');
    }
  });

  ///// geneticdiseas_other
  $('input[name="person[geneticdiseas]"]:checked').val() === '1' || $('#geneticdiseas_other').hide();
  $('input[name="person[geneticdiseas]"]').on('change', function (event) {
    if(event.target.value ==='1'){
      $('#geneticdiseas_other').show('fast');
    }else{
      $('input[name="person[geneticdiseas_other]"]').val('');
      $('#geneticdiseas_other').hide('fast');
    }
  });

  ///// BMI
  const setBMI= () => {
    if($('input[name="person[weight]"]').val()&&$('input[name="person[height]"]').val()){
      const weight =$('input[name="person[weight]"]').val();
      const height =$('input[name="person[height]"]').val();
      const isHeight =(height/100);
      const isBMI = weight/(isHeight*isHeight)
      $('input[name="person[bmi]"]').val(isBMI.toFixed(2));
    }else{
      $('input[name="person[bmi]"]').val(null);
    }
  };
  $('input[name="person[weight]"]').on('change', function (event) {
    setBMI();
  });
  $('input[name="person[height]"]').on('change', function (event) {
    setBMI();
  });

  ///// bloodgroup
  let dataInitBloodg=null;
  $('input[name="person[bloodgroup]"]').on('click', function (event) {
    if(dataInitBloodg !== event.target.value){
      dataInitBloodg=event.target.value;
    }else{
      dataInitBloodg=null;
      $('input[name="person[bloodgroup]"]:checked').removeAttr('checked');
    }
  });

  ///// maininscl_type
  $('select[name="person[maininscl]"]').val()||$('.maininscl_type').hide();
  $('select[name="person[maininscl]"]').on('change', function (event) {
     if(event.target.value !== ''){
      $('.maininscl_type').show('fast');
    }else{
      $('input[name="person[maininscl_type]"]:checked').removeAttr('checked');
      $('.maininscl_type').hide('fast');
    }
  });

  ///// bloodrh
  let dataInitBloodrh=null;
  $('input[name="person[bloodrh]"]').on('click', function (event) {
    console.log("data in person",event.target.value)
    if(dataInitBloodrh !== event.target.value){
      dataInitBloodrh=event.target.value;
    }else{
      dataInitBloodrh=null;
      $('input[name="person[bloodrh]"]:checked').removeAttr('checked');
    }
  });

  // birthdate
  $('input[name="person[age]"]').on('change', function (event) {
    let currentDate = new Date();
    let selectedYear = currentDate.getFullYear() - event.target.value;
    $('#birthdate').calendar('set date', `01/01/${selectedYear}`, true, false);
  });
}

let toggle = false;
$(document).ready(() => {
  initPeople();
  toggle = true;
});

$(document).on('turbolinks:load', () => {
  if (toggle) {
    toggle = false;
    return;
  }
  initPeople();
});
