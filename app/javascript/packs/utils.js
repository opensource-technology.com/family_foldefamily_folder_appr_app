$('#family_folder_id').on('change', function () {
  const community_survey_id = $('#community_survey_id').val();
  const h_id = $('#h_id').val();
  const family_folder_id = $('#family_folder_id').val();
  if (!family_folder_id) return;
  if (!h_id) return;
  $.ajax({
    method: 'GET',
    url: `/community_surveys/${community_survey_id}/houses/${h_id}/family_folders/${family_folder_id}/people`,
    dataType: 'json'
  }).done(function (data) {
    let $component = $('#p_id');
    generate_select_options($component, data);
  }).fail(function () {
    alert("error");
  });
});

$('#h_id').on('change', function () {
  const community_survey_id = $('#community_survey_id').val();
  const h_id = $('#h_id').val();
  if (!h_id) return;
  $.ajax({
    async: true,
    method: 'GET',
    url: `/community_surveys/${community_survey_id}/houses/${h_id}/family_folders`,
    dataType: 'json'
  }).done(function (data) {
    let $component = $('#family_folder_id');
    // $('#family_folder_id').dropdown('clear');
    generate_select_options($component, data);
    reset_for(2);
  }).fail(function () {
    alert("error");
  });
});

$('#community_survey_id').on('change', function () {
  let id = $('#community_survey_id').val();
  $.ajax({
    async: true,
    method: 'GET',
    url: `/community_surveys/${id}/houses`,
    dataType: 'json'
  }).done(function (data) {
    let $component = $('#h_id');
    generate_select_options($component, data);
    reset_for(1);
  }).fail(function () {
    alert("error");
  });
});

function generate_select_options(component, data) {
  component.empty();
  component.dropdown('clear');
  if (data.length > 0) {
    for (let i = 0; i < data.length; i++) {
      component.append('<option id=' + data[i][1] + ' value=' + data[i][1] + '>' + data[i][0] + '</option>');
    }
  } else {
    component.append('<option value="">-</option>');
  }
  component.change();
}

function reset_for(type) {
  if (type == 1) {
    $component = $('#family_folder_id');
    $component.dropdown('clear');
    // $component.empty();
    $component.append('<option value="">-</option>');
    $component.change();
  } 
  $component = $('#p_id');
  $component.dropdown('clear');
  // $component.empty();
  $component.append('<option value="">-</option>');
  $component.change();
}
