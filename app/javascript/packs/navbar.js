$(document).on('turbolinks:load', function () {
  $("#toggle_navbar").click(function () {
    $(".headVisit-contentLeft").toggleClass("--active");
  });

  $("#toggle_breadcrumb").click(function () {
    $(".menuFilter").toggleClass("--active");
    $("#toggle_breadcrumb_icon").toggleClass("ellipsis vertical icon times icon");
  });

  ['house', 'house_health_survey', 'person', 'person_health_survey', 'home_visit'].forEach(i => {
    $(`#goto_${i}_button`).click(function() {
      $('#goto_modal').modal({autofocus: false}).modal('show');
      $('#goto_modal').ready(function () {
        $.ajax({ url: `/goto_${i}` }).done(function (html) {
          $("#content").html(html);
        });
      });
    });
  });
});
