/* eslint no-console:0 */
// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.
//
// To reference this file, add <%= javascript_pack_tag 'application' %> to the appropriate
// layout file, like app/views/layouts/application.html.erb


// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
// const images = require.context('../images', true)
// const imagePath = (name) => images(name, true)
// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//

require("@rails/ujs").start()
require("turbolinks").start()
require("jquery")
require("moment")
require("semantic-ui-sass")
require("semantic-ui-calendar/dist/calendar")
require('./import')
require('./message')
require('./analytics')
require("./navbar")
require("./search_people")

function dateFormatter (date, settings) {
  if (!date) return '';
  const day = ("0" + date.getDate()).slice(-2);
  const month = ("0" + (date.getMonth() + 1)).slice(-2);
  const year = date.getFullYear() + 543;

  let represent;
  if (settings.type == 'date') {
    represent = day + '/' + month + '/' + year;
  } else if (settings.type == 'month') {
    represent = month + '/' + year;
  }

  return represent;
}

function thaiText () {
  return {
    days: ['อา.', 'จ.', 'อ.', 'พ', 'พฤ.', 'ศ.', 'ส.'],
    months: ['มกราคม', 'กุมภาพันธ์', 'มีนาคม', 'เมษายน', 'พฤษภาคม', 'มิถุนายน', 'กรกฎาคม', 'สิงหาคม', 'กันยายน', 'ตุลาคม', 'พฤศจิกายน', 'ธันวาคม'],
    monthsShort: ['ม.ค.', 'ก.พ.', 'มี.ค.', 'เม.ย.', 'พ.ค.', 'มิ.ย.', 'ก.ค.', 'ส.ค.', 'ก.ย.', 'ต.ค.', 'พ.ย.', 'ธ.ค.'],
    today: 'วันนี้',
    now: 'ขณะนี้',
    am: 'AM',
    pm: 'PM'
  };
}

function enableCalendar(component, options) {
  // console.log('enable calendar');
  const defaultOptions = {
    type: 'date',
    maxDate: new Date(),
    text: thaiText(),
    formatter: {
      date: dateFormatter,
      dayHeader: function (date, settings) {
        const month = date.getMonth();
        const year = date.getFullYear() + 543;
        return thaiText().monthsShort[month] + ' ' + year;
      },
      monthHeader: function (date, settings) {
        const year = date.getFullYear() + 543;
        return year;
      },
      yearHeader: function (date, settings) {
        const year = date.getFullYear() + 543;
        return year;
      }
    }
  }

  $(component).calendar(Object.assign({}, defaultOptions, options))
}

function load_address(scope) {
  console.log('load adress');
  $(`#${scope}_addr_changwat_id`).on('change', function (event) {
    update_component('amphur', event.target.value);
  });

  $(`#${scope}_addr_amphur_id`).on('change', function (event) {
    update_component('tambon', event.target.value);
  });

  function update_component(context, value) {
    if(!value) {
      reset(context);
      return;
    }
    $.ajax({
      method: 'GET',
      url: `/addr/update_${context}/${value}`
    }).done(function (data) {
      let $component = $(`#${scope}_addr_${context}_id`);
      $component.empty();
      for (let i = 0; i < data.length; i++) {
        $component.append('<option id=' + data[i][1] + ' value=' + data[i][1] + '>' + data[i][0] + '</option>');
      }
      $component.change();
    }).fail(function () {
      alert("error");
    });
  }

  function reset(context) {
    if (context == 'amphur') {
      $(`#${scope}_addr_amphur_id`).empty().append('<option value="">-</option>');
    }
    $(`#${scope}_addr_tambon_id`).empty().append('<option value="">-</option>');
  }
}

window.load_address = load_address
window.thaiText = thaiText
window.enableCalendar = enableCalendar

import "controllers"
