enableCalendar('#from_date_component', {
  startMode: 'year',
  endCalendar: $('#to_date_component'),
  maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 1),
});

enableCalendar('#to_date_component', {
  startMode: 'year',
  maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 1),
});

$('#from_date_component').calendar('set date', fromDate);
$('#to_date_component').calendar('set date', toDate);
$('.ui.radio.checkbox').checkbox();
