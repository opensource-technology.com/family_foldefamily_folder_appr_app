$(document).on('turbolinks:load', () => {
  $('.ui.search').search({
    minCharacters : 1,
    apiSettings: {
      url: `/search?value={query}`,
      onResponse: function(res) {
        console.log(res)
        console.log("response from search")
        let response = {
            results : []
        };
        $.each(res, function(index, item) {
          response.results.push({
            title: `${item.first_name} ${item.last_name}`,
            description: `เลขประจำตัวประชาชน: ${item.pid} ชุมชน: ${item.house.community_survey.vname} บ้านเลขที่: ${item.house.house}`,
            url: `/community_surveys/${item.house.community_survey_id}/houses/${item.house_id}/family_folders/${item.family_folder_id}/people/search/${item.person_id}`,
          });
        });
        return response;
      }
    }
  });
});
