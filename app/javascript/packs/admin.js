enableCalendar('#from_date_component', {
  startMode: 'year',
  endCalendar: $('#to_date_component'),
  maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 1),
});

enableCalendar('#to_date_component', {
  startMode: 'year',
  maxDate: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 1),
});

$('#from_date_component').calendar('set date', fromDate);
$('#to_date_component').calendar('set date', toDate);
$('.ui.radio.checkbox').checkbox();
$('.ui.search.dropdown').dropdown({ fullTextSearch: true }).dropdown('set selected', hospitalSearch);

[{ name: 'hospital_type_rd', type: 0 }, { name: 'clinic_type_rd', type: 1 }, { name: 'public_health_center_type_rd', type: 2 }].forEach((i) => {
  $(`#${i.name}`).change(() => {
    $.ajax({
      async: true,
      method: 'GET',
      url: `/hospitals/type/${i.type}`,
      dataType: 'json'
    }).done(function (data) {
      let $component = $('#hospital_select');
      generate_select_options($component, data);
    }).fail(function () {
      alert("error");
    });
  });
});

function generate_select_options(component, data) {
  component.empty();
  component.dropdown('clear');
  if (data.length > 0) {
    for (let i = 0; i < data.length; i++) {
      component.append(`<option id=${data[i][1]} value=${data[i][1]}>${data[i][1]}: ${data[i][0]}</option>`);
    }
  } else {
    component.append('<option value="">-</option>');
  }
  component.change();
}
