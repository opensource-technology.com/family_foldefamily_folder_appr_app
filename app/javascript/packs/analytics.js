window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
ga('create', 'UA-62563282-2', 'auto');
ga('send', 'pageview');

function handleTagEventLogin(idHospital,iduser) {
  ga('send', 'event', {
    eventCategory: 'Hospital: '+idHospital+', ID: '+iduser,
    eventAction: 'User Web',
    eventLabel: 'Use Web',
    transport: 'beacon'
  });
}
