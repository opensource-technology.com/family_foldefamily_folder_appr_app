import { Controller } from "stimulus"
import Rails from '@rails/ujs'
import * as moment from 'moment'

export default class extends Controller {
  static targets = [
    "loader", "pid", "aid", "title", "firstName", "lastName", "sex", "race", "national"
  ]

  API_KEY = '58df0f965b2ee16d14ed33232ba88d4f1f2a7c8f76630b64a59416955b1548606f4080c6c72bb49a0de02394612fa270f03142b287a88585c9fd1042164d5016'
  skip = false

  connect() {
    this.checkRaceAndNational()
  }

  checkRaceBeforeSumbit(event) {
    event.preventDefault()
    if (this.raceTarget.value !== '099') {
      $('.not-thai-person-modal').modal({
        // inverted: true,
        closable: true,
        onDeny: function() {
          $('.not-thai-person-modal').modal("hide")
        },
        onApprove: function() {
          event.target.submit()
        }
      }).modal("show")
    } else {
      event.target.submit()
    }
  }

  checkRaceAndNational() {
    let skip = true
    if (this.raceTarget.value === '099' && this.nationalTarget.value === '099') {
      skip = false
    }

    this.pidTarget.disabled = skip
    this.aidTarget.disabled = !skip
  }

  raceAndNationalChanged() {
    this.checkRaceAndNational()
  }

  verify() {
    if (this.skip || this.pidTarget.value.length != 13) {
      return
    }

    this.identify()
  }

  loader(show) {
    if (show) {
      this.pidTarget.parentNode.classList.add("loading")
    } else {
      this.pidTarget.parentNode.classList.remove("loading")
    }
  }

  identify() {
    // Rails.ajax()
    this.loader(true)
    const formData = new FormData()
    formData.append('key', this.API_KEY)
    formData.append('pid', this.pidTarget.value)
    fetch('https://bot.e-referral.net/monitoring/api/API4Ext/checkPeople', {
      method: 'POST',
      mode: 'cors',
      body: formData,
    }).then(res => res.json()).then((result) => {
      if (result instanceof Array) {
        this.setPersonalInfo(result[0])
      } else if (result.status === false) {
        $('.not-found-modal').modal({
          inverted: true
        }).modal("show")
      }
      this.loader(false)
    })
  }

  setPersonalInfo(info) {
    $('#person_title_id').dropdown("set selected", parseInt(info.title))
    this.firstNameTarget.value = info.fname
    this.lastNameTarget.value = info.lname

    // Birhtdate
    $('#birthdate').calendar("set date", moment(info.birthdate).toDate(), true, true)

    // Sex
    this.sexTargets.forEach((target) => {
      target.checked = (target.value === info.sex)
    })

    // inscl
    $('#person_maininscl').dropdown("set selected", this.parseInscl(info.maininscl, info.maininscl_desc))
  }

  parseInscl(inscl, inscl_description) {
    if (inscl === "UCS" || inscl === "WEL") {
      return 1
    } else if (inscl === 'SSS') {
      return 2
    } else if (inscl === 'OFC') {
      return 3
    } else {
      return 4
    }
  }
}
