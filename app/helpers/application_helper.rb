module ApplicationHelper
  def no(obj)
    ((obj.current_page - 1) * obj.limit_value) + 1
  end

  def page_param
    params.fetch('page', '1').to_i
  end

  def offset(page, per_page)
    (page - 1) * per_page
  end

  def active_for(options = {})
    name_of_controller = options[:controller] || nil
    # action_name = options[:action] || nil
    if name_of_controller.include? controller_name
      'btnLogout --active'
    else
      'btnLogout'
    end
  end

  def active(options = {})
    opt_action_name = options[:action] || nil
    template = options[:template] || nil

    if opt_action_name == action_name || template == params.fetch('template', nil)
      'btnLogout --active'
    else
      'btnLogout'
    end
  end

  def date_format(date)
    "#{date.strftime('%d/%m/')}#{date.strftime('%Y').to_i + 543}" unless date.nil?
  end

  def date_time_format(date)
    "#{date_format(date)} #{date.strftime('%H:%M:%S')}" unless date.nil?
  end
end
