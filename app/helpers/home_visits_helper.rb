module HomeVisitsHelper
  def persontype_value(value)
    t("person_health_survey.persontype_type#{value.gsub('0', '')}_label")
  end
end
