module HouseHealthSurveysHelper
  def explore_value(value)
    exp = HouseHealthSurvey.explores.invert.fetch(value.to_i)
    t("house_health_survey.explorsit_#{exp}_label") if exp
  end
end
