module DiseasesHelper
  def get_random_suffix(length = 10)
    source = ('a'..'z').to_a + ('A'..'Z').to_a + (0..9).to_a
    key = ''
    length.times { key += source[rand(source.size)].to_s }
  end
end
