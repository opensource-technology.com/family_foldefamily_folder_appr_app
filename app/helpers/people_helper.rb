module PeopleHelper
  def total_people(value)
    return t(:not_found_label) if value.nil?
    value.size
  end
end
