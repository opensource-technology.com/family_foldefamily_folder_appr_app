module ReportsHelper
  def query_params(query, searchable)
    searchable.from_month_year.nil? || query = query.gsub("{from_month_year}", searchable.from_month_year)
    searchable.to_month_year.nil? || query = query.gsub("{to_month_year}", searchable.to_month_year)
    searchable.vid.nil? || query = query.gsub("{vid}", searchable.vid)
    # searchable.community_id.nil? || query = query.gsub("{community_id}", searchable.community_id)
    searchable.htype.nil? || query = query.gsub("{htype}", searchable.htype.to_s)
    query = query.gsub("{hid}", searchable.hid || '')
    searchable.page.nil? || query = query.gsub("{offset}", offset(searchable.page, Kaminari.config.default_per_page.to_i).to_s)
    searchable.from_date.nil? || query = query.gsub("{from_date}", searchable.from_date)
    searchable.to_date.nil? || query = query.gsub("{to_date}", searchable.to_date)
    searchable.process_results.nil? || query = query.gsub("{process_results}", searchable.process_results)
    query = searchable.hospital.nil? ? query.gsub("{hospcode}", current_user.hospcode) : query.gsub("{hospcode}", searchable.hospital)
    query = if searchable.date_type.nil? || searchable.date_type == 'survey'
              query.gsub("{date_type}", 'datesurvey')
            elsif searchable.date_type == 'save'
              query.gsub("{date_type}", 'updated_at')
            end
    query = if searchable.community_list.nil?
              query.gsub("{community_list}", "and community_surveys.hospcode in ('')")
            elsif searchable.community_list == 'all'
              if searchable.htype.to_i == 0
                query.gsub("{community_list}", "and hospitals.htype not in (1,3,4)")
              else
                query.gsub("{community_list}", "and hospitals.htype = #{searchable.htype}")
              end
            else
              query.gsub("{community_list}", "and community_surveys.hospcode in ('#{searchable.community_list}')")
            end
    query.gsub("{per_page}", Kaminari.config.default_per_page.to_s)
  end

  def execute_query(file_name, searchable)
    # Rails.logger.debug "query #{file_name}"
    ActiveRecord::Base.connection.exec_query query_params(File.read("#{Rails.root}/lib/queries/#{file_name}.txt"), searchable)
  end

  def revert_month_year(text)
    list = text.split '-'
    "#{list[1].rjust(2, '0')}/#{(list[0].to_i + 543)}"
  end

  def revert_date(text)
    list = text.split '-'
    "#{list[2].rjust(2, '0')}/#{list[1].rjust(2, '0')}/#{(list[0].to_i + 543)}"
  end

  def buddhist_date_format(date)
    "#{date.day.to_s.rjust(2, '0')}/#{date.month.to_s.rjust(2, '0')}/#{(date.year.to_i + 543).to_s.rjust(2, '0')}"
  end

  class Searchable
    attr_accessor :page, :from_month_year, :to_month_year, :date_type, :vid, :community_id, :hid, :hospcode, :htype, :hospital, :template,
                  :from_date, :to_date, :community_list, :process_results

    def initialize(params = {})
      @date_type = params[:date_type].blank? ? 'survey' : params[:date_type]
      @from_month_year = params[:from_month_year].blank? ? Time.now.to_date.strftime('%Y-%m') : convert_month_year(params[:from_month_year])
      @to_month_year = params[:to_month_year].blank? ? Time.now.to_date.strftime('%Y-%m') : convert_month_year(params[:to_month_year])
      @page = params[:page].blank? ? 1 : params[:page].to_i
      @vid = params[:vid].blank? ? nil : params[:vid]
      # @community_id = params[:community_id].blank? ? nil : params[:community_id]
      @hid = params[:hid].blank? ? nil : params[:hid]
      @hospcode = params[:hospcode].nil? ? nil : params[:hospcode]
      @htype = params[:htype].nil? ? 1 : params[:htype].to_i
      @hospital = params[:hospital].blank? ? nil : params[:hospital]
      @template = params[:template].blank? ? 'site' : params[:template]
      @from_date = params[:from_date].blank? ? Time.now.to_date.strftime('%Y-%m-%d') : convert_date(params[:from_date])
      @to_date = params[:to_date].blank? ? Time.now.to_date.strftime('%Y-%m-%d') : convert_date(params[:to_date])
      @community_list = params[:hospital].blank? ? nil : params[:hospital]
      @process_results = params[:process_results].blank? ? nil : params[:process_results]
    end

    def convert_month_year(text)
      list = text.split '/'
      "#{(list[1].to_i - 543)}-#{list[0].rjust(2, '0')}"
    end

    def convert_date(text)
      list = text.split '/'
      "#{list[2].to_i - 543}-#{list[1].rjust(2, '0')}-#{list[0].rjust(2, '0')}"
    end
  end

  def translate_status(status, sent_status)
    data_return = {}
    data_return[:class_name] = '--info'
    data_return[:state] = 'รอส่ง'
    if sent_status == 3
      data_return[:class_name] = '--danger'
      data_return[:state] = 'ส่งไม่สำเร็จ'
    elsif sent_status == 2
      case status
      when 1
        data_return[:class_name] = '--warning'
        data_return[:state] = 'รอตรวจ'
      when 2
        data_return[:class_name] = '--success'
        data_return[:state] = 'ผ่าน'
      when 3
        data_return[:class_name] = '--danger'
        data_return[:state] = 'ไม่ผ่าน'
      end
    end
    data_return
  end

  def translate_status_description(
    sent_status,
    status_description,
    sent_errors
  )
    text_return = ''
    if sent_status == 3
      text_return =   sent_errors.to_s
    elsif sent_status == 2
      text_return = status_description == 'Not found' ? '' : status_description
    end
    text_return
  end

  def show_hospital(htype, hospital)
    htype_text = {
      1 => t("report.hospital_type_label"),
      4 => t("report.clinic_type_label"),
      3 => t("report.public_health_center_type_label"),
      0 => t("report.other_label")
    }
    if hospital == 'all'
      "#{htype_text[htype]} #{t('report.all_label')}"
    else
      (Hospital.find_by code: @searchable.hospital).label
    end
  end

  def admin?(role)
    role == 'admin'
  end

  def set_param_people_path(role, searchable, result, process_results)
    method("reports_#{role}_people_path").call(
      from_date: revert_date(searchable.from_date),
      to_date: revert_date(searchable.to_date),
      date_type: searchable.date_type,
      vid: result&.vid || searchable.vid,
      hid: result&.hid || nil,
      htype: searchable.htype,
      hospital: searchable.hospital,
      template: searchable.template,
      process_results: process_results
    )
  end
  # def string_to_reports_route(route_s)
  #   case route_s
  #   when 'reports_admin_health_survey_path'
  #     reports_admin_health_survey_path
  #   when 'reports_admin_home_visit_path'
  #     reports_admin_home_visit_path
  #   when 'reports_admin_houses_path'
  #     reports_admin_houses_path
  #   when 'reports_admin_people_path'
  #     reports_admin_people_path
  #   when 'reports_site_health_survey_path'
  #     reports_site_health_survey_path
  #   when 'reports_site_home_visit_path'
  #     reports_site_home_visit_path
  #   when 'reports_site_houses_path'
  #     reports_site_houses_path
  #   when 'reports_site_people_path'
  #     reports_site_people_path
  #   else
  #     reports_site_people_path
  #   end
  # end
end
