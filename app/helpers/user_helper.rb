require 'net/http'
require 'json'

module UserHelper
  class NhsoUser
    attr_accessor :username, :first_name, :user_id, :hospcode, :role

    def initialize(user_id, username, hospcode, role)
      self.user_id = user_id
      self.username = username
      self.first_name = username
      self.hospcode = hospcode
      self.role = role
    end

    def role?(role)
      self.role.to_sym == role
    end

    def admin?
      role.to_sym == :admin
    end

    def advanced_user?
      role.to_sym == :advanced
    end

    def user?
      role.to_sym == :user
    end
  end

  def parse_role(type)
    case type
    when 'VSTAFFTYPE1', 'V_STAFFTYPE1_NHSO'
      :admin
    when 'VSTAFFTYPE2'
      :advanced
    when 'VSTAFFTYPE3'
      :user
    end
  end

  def authenticate(username, password)
    settings = Rails.configuration.nhso_authentication || {}
    uri = URI("#{settings[:url]}/auth/login")
    params = { username: username, password: password, appid: 11 }
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = settings[:use_ssl]
    request = Net::HTTP::Post.new(uri.request_uri)
    request['Content-Type'] = 'application/json'
    res = http.request request, params.to_json

    body = JSON.parse(res.body)
    status = body['status']
    return nil if status == 'error'

    staff_id = body['staffid']
    staff_name = body['staffname']
    profile = body['profile']
    role = parse_role(body['grant'][0]['permission_var'])

    NhsoUser.new(staff_id, staff_name, profile, role)
  rescue OpenSSL::SSL::SSLError => e
    raise e
  rescue StandardError
    nil
  end

  def login(user)
    reset_session
    session[:user_id] = user.user_id
    session[:username] = user.username
    session[:first_name] = user.first_name
    session[:hospcode] = user.hospcode
    session[:role] = user.role
    session[:hospital] = Hospital.find_by(code: user.hospcode).label
    session[:expired_at] = Rails.configuration.session_timeout.minutes.from_now
  end

  def logout
    reset_session
    session.delete(:user_id)
  end
end
