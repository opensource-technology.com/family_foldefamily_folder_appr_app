module HousesHelper
  def house_style_value(value)
    return t(:not_found_label) if value.nil?
    hs_value = House.house_style_types.invert.fetch(value.to_i)
    t("house.housestyletype_#{hs_value}_label")
  end
end
