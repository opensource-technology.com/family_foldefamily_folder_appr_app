module SessionsHelper #:nodoc:
  def authenticate_user!
    return redirect_to login_url if current_user.nil?

    session_expires!
    active_session
  end

  def active_session
    session[:expired_at] = Rails.configuration.session_timeout.minutes.from_now
  end

  def current_user
    if !session.key?(:user_id)
      @current_user = nil
    elsif @current_user.nil?
      @current_user = if Rails.configuration.external_auth
                        UserHelper::NhsoUser.new session[:user_id], session[:username], session[:hospcode], session[:role]
                      else
                        User.find_by(id: session[:user_id])
                      end
    end
    # p @current_user
    @current_user
  end

  def current_hospital
    session[:hospital]
  end

  def current_site
    @hospital = Hospital.find_by(code: session[:hospcode])
  end

  # Logs in the given user.
  def login(user)
    reset_session
    session[:user_id] = user.id
    session[:hospital] = Hospital.find_by(code: user.hospcode).label if user.hospcode
    active_session
  end

  def logout
    session.delete(:user_id)
    session.delete(:hospital)
    session.delete(:expired_at)
    @current_user = nil
    reset_session
  end

  def logged_in?
    !current_user.nil?
  end

  def session_expires!
    expired_at = session[:expired_at]
    return if expired_at > Time.current

    reset_session
    flash[:error] = t(:error_session_timeout) if Time.current - expired_at.to_time < Rails.application.config.max_session_timeout
    redirect_to login_url
  end
end
