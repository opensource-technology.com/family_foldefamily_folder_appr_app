module AddressHelper
  def load_addr(instance)
    addr_changwat = AddrChangwat.find(instance.addr_changwat_id)
    addr_amphur = AddrAmphur.find(instance.addr_amphur_id) if instance.addr_amphur_id
    # puts "amphur: #{addr_amphur}"
    # puts addr_amphur.present?
    @addr_amphurs = AddrAmphur.where("code like ? and label not like '%*%'", "#{addr_changwat.code}%").order(:label)
    # puts @addr_amphurs.size
    @addr_tambons = AddrTambon.where("code like ? and label not like '%*%'", "#{addr_amphur.code}%").order(:label) if addr_amphur.present?
  end
end
