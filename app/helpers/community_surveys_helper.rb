module CommunitySurveysHelper
  def ctype_value(value)
    return t(:not_found_label) if value.nil?

    ctype = value == '1' ? 'main' : 'sub'
    t("community_survey.title_type_#{ctype}_label")
  end

  def commustyle_value(value)
    return t(:not_found_label) if value.nil?

    cs_value = CommunitySurvey.commu_styles.invert.fetch(value.to_i)
    t("community_survey.commustyle_#{cs_value}_label")
  end
end
