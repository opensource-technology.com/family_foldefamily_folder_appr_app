json.extract! home_visit, :id, :created_at, :updated_at
json.url home_visit_url(home_visit, format: :json)
