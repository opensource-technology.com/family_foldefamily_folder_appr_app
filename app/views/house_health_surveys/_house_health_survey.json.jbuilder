json.extract! house_health_survey, :id, :created_at, :updated_at
json.url house_health_survey_url(house_health_survey, format: :json)
