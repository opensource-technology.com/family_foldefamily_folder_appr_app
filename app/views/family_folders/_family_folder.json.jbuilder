json.extract! family_folder, :id, :created_at, :updated_at
json.url family_folder_url(family_folder, format: :json)
