json.extract! community_survey, :id, :created_at, :updated_at
json.url community_survey_url(community_survey, format: :json)
