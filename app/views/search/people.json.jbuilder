json.array! @persons do |person|
  json.id person.id
  json.first_name person.first_name
  json.last_name person.last_name
  json.house_id person.house_id
  json.pid person.pid
  json.person_id person.person_id
  json.family_folder_id person.family_folder_id
  if person.house
    json.house do
      json.id person.house.hid
      json.house person.house.house
      json.community_survey_id person.house.community_survey_id
      json.community_survey do
        json.id person.house.community_survey.id
        json.vname person.house.community_survey.vname
      end
    end
  end
end
