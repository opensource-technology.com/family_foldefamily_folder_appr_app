json.extract! person_health_survey, :id, :created_at, :updated_at
json.url person_health_survey_url(person_health_survey, format: :json)
