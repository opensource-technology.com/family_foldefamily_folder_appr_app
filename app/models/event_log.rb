class EventLog < ApplicationRecord
  enum level: %i[all trace debug info warn error fatal off], _suffix: true

  def self.info(name, message, actor = "system")
    EventLog.create name: name, message: message, actor: actor, level: :info
  end
end
