class HomeVisit < ApplicationRecord
  before_create :pre_create
  before_update :update_u_date
  after_initialize :init

  # Associate
  belongs_to :family_folder
  belongs_to :house
  belongs_to :person
  belongs_to :process_result, optional: true

  enum g_type: {
    gtype_type1: 2501,
    gtype_type2: 2502,
    gtype_type3: 2503,
    gtype_type4: 2504,
    gtype_type5: 2505,
    gtype_type6: 2506,
    gtype_type7: 2507,
    gtype_type8: 2508
  }

  private

  def autoid
    site = Site.hospcode hospcode
    no = site.hv_no + 1
    site.update(hv_no: no)
    "#{hospcode}#{no.to_s.rjust(10, '0')}"
  end

  def init
    self.datesurvey = Date.today if datesurvey.nil? && new_record?
  end

  def pre_create
    self.hvid = autoid
    update_u_date
  end

  def update_u_date
    self.u_date = updated_at
  end
end
