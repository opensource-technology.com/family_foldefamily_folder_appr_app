class PersonHealthSurvey < ApplicationRecord
  before_create :pre_create
  before_update :update_u_date
  after_initialize :init

  # Associate
  belongs_to :family_folder
  belongs_to :person
  belongs_to :process_result, optional: true

  # Validation
  validates :phsurvey_id, allow_blank: true, allow_nil: true, uniqueness: true,
                          length: { maximum: 14 }
  validates :persontype, presence: true
  validates :datesurvey, presence: true
  validates :hospcode, allow_blank: true, length: { minimum: 5, maximum: 5 }

  enum person_type: {
    persontype_type1: '01',
    persontype_type2: '02',
    persontype_type3: '03',
    persontype_type4: '04',
    persontype_type5: '05',
    persontype_type6: '06',
    persontype_type7: '07',
    persontype_type8: '08',
    persontype_type9: '09'
  }

  enum antenatal_care1: {
    antenatalcare1_type1: 1,
    antenatalcare1_type2: 2,
    antenatalcare1_type3: 3,
    antenatalcare1_type4: 4,
    antenatalcare1_type5: 5
  }

  enum antenatal_care2: {
    antenatalcare2_type1: 1,
    antenatalcare2_type2: 2,
    antenatalcare2_type3: 3,
    antenatalcare2_type4: 4,
    antenatalcare2_type5: 5
  }

  enum person_h_servicecares: {
    servicecare_type1: 1,
    servicecare_type2: 2,
    servicecare_type3: 3,
    servicecare_type4: 4,
    servicecare_type5: 5
  }

  private

  def autoid
    site = Site.hospcode hospcode
    no = site.phs_no + 1
    site.update(phs_no: no)
    "#{hospcode}#{no.to_s.rjust(9, '0')}"
  end

  def init
    self.datesurvey = Date.today if datesurvey.nil? && new_record?
  end

  def pre_create
    self.phsurvey_id = autoid
    update_u_date
  end

  def update_u_date
    self.u_date = updated_at
  end
end
