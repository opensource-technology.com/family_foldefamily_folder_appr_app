class FamilyFolder < ApplicationRecord
  scope :filter_by_family_folder_id, -> (family_folder_id) { where ffolder_id: family_folder_id }

  # Hooks Events
  before_create :pre_create
  before_update :update_u_date

  # Associate
  belongs_to :house
  has_many :people

  def short
    ffolder_id
  end

  private

  def autoid
    site = Site.hospcode hospcode
    no = site.ff_no + 1
    site.update(ff_no: no)
    "#{hospcode}#{no.to_s.rjust(9, '0')}"
  end

  def pre_create
    self.ffolder_id = autoid
    update_u_date
  end

  def update_u_date
    self.u_date = updated_at
  end
end
