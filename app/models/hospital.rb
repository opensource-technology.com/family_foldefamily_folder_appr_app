class Hospital < ApplicationRecord
  validates :code, uniqueness: true
end
