class User < ApplicationRecord
  after_initialize :set_default_role

  validates :username, presence: true, uniqueness: true, length: { minimum: 4, maximum: 20 }
  validates :password, presence: true, length: { minimum: 6 }, confirmation: true
  validates :first_name, length: { maximum: 20 }
  validates :role, inclusion: { in: :role }

  has_secure_password
  enum role: %i[user admin]

  private

  def set_default_role
    self.role ||= :user
  end
end
