class Person < ApplicationRecord
  scope :filter_by_person_id, -> (person_id) { where person_id: person_id }

  # Hooks Events
  before_create :pre_create
  before_update :update_u_date
  after_initialize :init

  belongs_to :title
  belongs_to :house
  belongs_to :family_folder
  has_many :diseases
  accepts_nested_attributes_for :diseases, allow_destroy: true, reject_if: proc { |attributes| attributes['name'].blank? }

  # Validation
  validates :person_id, allow_blank: true, allow_nil: true, uniqueness: true,
                        length: { minimum: 13, maximum: 13 }
  validates :hospcode, allow_blank: true, length: { minimum: 5, maximum: 5 }
  validates :sex, presence: true

  enum name_register: {
    have_list: 1,
    bangkok_list: 2,
    country_list: 3,
    not_have_foreign_list: 4,
    unknown: 5
  }

  enum status_person: {
    type1: 1,
    type2: 2,
    type3: 3,
    type4: 4,
    type5: 5
  }

  enum marital_state: {
    maritalstate_type1: 1,
    maritalstate_type2: 2,
    maritalstate_type3: 3,
    maritalstate_type4: 4,
    maritalstate_type5: 5,
    maritalstate_type6: 6
  }
  enum peducate: {
    educate_type1: 1,
    educate_type2: 2,
    educate_type3: 3,
    educate_type4: 4,
    educate_type5: 5,
    educate_type6: 6,
    educate_type7: 7,
    educate_type8: 8,
    educate_type9: 9
  }

  enum pcareer: {
    career_type1: 1,
    career_type2: 2,
    career_type3: 3,
    career_type4: 4,
    career_type5: 5,
    career_type6: 6,
    career_type7: 7,
    career_type8: 8,
    career_type9: 9,
    career_type10: 10,
    career_type11: 11
  }

  enum pcommustate: {
    commustate_type1: 1,
    commustate_type2: 2,
    commustate_type3: 3,
    commustate_type4: 4,
    commustate_type5: 5,
    commustate_type6: 6,
    commustate_type7: 7,
    commustate_type8: 8
  }

  enum pmaininscl: {
    maininscl_type1: 1,
    maininscl_type2: 2,
    maininscl_type3: 3,
    maininscl_type4: 4
  }

  enum ppracsick: {
    pracsick_type1: 1,
    pracsick_type2: 2,
    pracsick_type3: 3,
    pracsick_type4: 4,
    pracsick_type5: 5,
    pracsick_type6: 6,
    pracsick_type7: 7,
    pracsick_type8: 8,
    pracsick_type9: 9,
    pracsick_type10: 10
  }

  attr_accessor :age

  def short
    "#{first_name} #{last_name}"
  end

  private

  def autoid
    site = Site.hospcode hospcode
    no = site.p_no + 1
    site.update(p_no: no)
    "#{hospcode}#{no.to_s.rjust(8, '0')}"
  end

  def init
    self.birth_date = Date.today if birth_date.nil? && new_record?
    self.datesurvey = Date.today if datesurvey.nil? && new_record?
    # self.datemovin = Date.today if datemovin.nil? && new_record?
    if age.nil? && new_record?
      self.age = 0
    elsif !birth_date.nil?
      self.age = Date.today.year - birth_date.year
    end
  end

  def pre_create
    self.person_id = autoid
    update_u_date
  end

  def update_u_date
    self.u_date = updated_at
  end
end
