class ProcessResult < ApplicationRecord
  validates :client_code, uniqueness: { scope: :process_type }
end
