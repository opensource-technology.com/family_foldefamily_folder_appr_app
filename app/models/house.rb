class House < ApplicationRecord
  scope :filter_by_hid, -> (hid) { where hid: hid }

  # Hooks Events
  before_create :pre_create
  before_update :pre_update
  after_initialize :init

  # Associate
  belongs_to :community_survey
  belongs_to :addr_changwat
  belongs_to :addr_amphur
  belongs_to :addr_tambon
  has_many :family_folders
  has_many :people

  # Validation
  validates :hid, allow_blank: true, allow_nil: true, uniqueness: true,
                  length: { minimum: 14, maximum: 14 }
  validates :hospcode, allow_blank: true, length: { minimum: 5, maximum: 5 }
  validates :namesurvey, presence: true
  validates :positionsurvey, presence: true
  validates :catm, allow_blank: true, allow_nil: true,
                   length: { minimum: 8, maximum: 8 }
  validates :housestyletype, presence: true, length: { minimum: 1, maximum: 1 }
  validates :housetype, presence: true, length: { minimum: 1, maximum: 1 }
  validates :addr_changwat, presence: true
  validates :addr_amphur, presence: true
  validates :addr_tambon, presence: true

  # Enum
  enum house_style_type: {
    single_house: 1,
    townhouse: 2,
    condominium: 3,
    dorm: 4,
    worker_house: 5,
    religious_place: 6,
    shophouse: 7,
    other: 8,
    unknown: 9
  }

  enum houst_tyde: {
    own: 1,
    rent: 2,
    rent_room: 3,
    with_others: 4,
    agency: 5,
    invade: 6,
    crown_property: 7,
    unknown_type: 8
  }

  enum business_type: {
    health: 231,
    sanitary_surveillance: 232,
    shelter: 233,
    community: 234
  }

  def short
    "(#{hid}): #{I18n.t('house.house_label')}: #{house}, #{moo}, #{villaname}, #{soimain}, #{road}"
  end

  private

  def autoid
    site = Site.hospcode hospcode
    no = site.h_no + 1
    site.update(h_no: no)
    "#{hospcode}#{no.to_s.rjust(9, '0')}"
  end

  def init
    self.datesurvey = Date.today if datesurvey.nil? && new_record?
    self.addr_changwat_id = 1 if addr_changwat_id.nil? && new_record? # default กทม
  end

  def pre_create
    self.hid = hid || autoid
    reset
    update_u_date
  end

  def pre_update
    reset
    update_u_date
  end

  def update_u_date
    self.u_date = updated_at
  end

  def reset
    return unless houseutili == '1'

    self.businesstype = nil
    self.namebusiness = nil
    self.ncustomer = nil
    self.accident = nil
    self.disease = nil
  end
end
