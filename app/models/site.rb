class Site < ApplicationRecord
  scope :hospcode, (lambda do |hospcode|
    site = find_by(hospcode: hospcode)
    site = create(hospcode: hospcode) if site.nil?
    site
  end)
end
