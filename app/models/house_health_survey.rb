class HouseHealthSurvey < ApplicationRecord
  # Hooks Events
  before_create :pre_create
  before_update :pre_update
  after_initialize :init

  # Associate
  belongs_to :family_folder

  # Validation
  validates :hhsurvey_id, allow_blank: true, allow_nil: true, uniqueness: true,
                          length: { minimum: 14, maximum: 14 }
  validates :hospcode, allow_blank: true, length: { minimum: 5, maximum: 5 }
  validates :family_folder, allow_blank: true, presence: false
  validates :explorsit, presence: true
  validates :transparent, presence: true
  validates :cleanliness, presence: true
  validates :wateruse, presence: true
  validates :foodsanit, presence: true
  validates :floodhouse, presence: true
  validates :homeenvirom, presence: true
  validates :animaldisease, presence: true
  validates :pets, presence: true

  # Enum
  enum explore: {
    not_resident: 1,
    house_closed: 2,
    non_cooporative: 3,
    discovered: 4
  }

  private

  def autoid
    site = Site.hospcode hospcode
    no = site.hhs_no + 1
    site.update(hhs_no: no)
    "#{hospcode}#{no.to_s.rjust(9, '0')}"
  end

  def init
    self.datesurvey = Date.today if datesurvey.nil? && new_record?
    # self.ndogvaccine_at = Date.today if ndogvaccine_at.nil? && new_record?
    # self.ncatvaccine_at = Date.today if ncatvaccine_at.nil? && new_record?
    # self.nmousevaccine_at = Date.today if nmousevaccine_at.nil? && new_record?
    # self.nrabbitvaccine_at = Date.today if nrabbitvaccine_at.nil? && new_record?
    # self.nsquirrelvaccine_at = Date.today if nsquirrelvaccine_at.nil? && new_record?
    # self.nmonkeyvaccine_at = Date.today if nmonkeyvaccine_at.nil? && new_record?
  end

  def pre_create
    self.hhsurvey_id = autoid
    reset
    update_u_date
  end

  def pre_update
    reset
    update_u_date
  end

  def update_u_date
    self.u_date = updated_at
  end

  def reset
    self.toilet_hygienic = nil if toilet == '1'
    if animaldisease == '1'
      self.rat = nil
      self.mosquito = nil
      self.cockroach = nil
      self.fly = nil
      self.snake = nil
    end

    if pets == '1'
      %w[ndog ncat nmouse nrabbit nsquirrel nmonkey nbirds nducks
         ncows npigs nchicken nfish nbuffalo ngoat nreptile].each do |p|
        method("#{p}=").call(nil) if p.respond_to?("#{p}=")
        method("#{p}sterilize=").call(nil) if p.respond_to?("#{p}sterilize=")
        method("#{p}vaccine=").call(nil) if p.respond_to?("#{p}vaccine=")
        method("#{p}micro=").call(nil) if p.respond_to?("#{p}micro=")
        method("#{p}vaccine_at=").call(nil) if p.respond_to?("#{p}vaccine_at=")
      end
    end

    self.explorsit_reason = nil unless explorsit == '3'
  end
end
