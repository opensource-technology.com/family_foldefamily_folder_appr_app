class CommunitySurvey < ApplicationRecord
  scope :filter_by_vid, -> (vid) { where vid: vid }

  # Hooks Events
  before_create :pre_create
  before_update :pre_update
  after_initialize :init

  # Associate
  belongs_to :addr_changwat
  belongs_to :addr_amphur
  belongs_to :addr_tambon

  # Validation
  validates :vid, allow_blank: true, allow_nil: true, uniqueness: true,
                  length: { minimum: 8, maximum: 14 }
  validates :hospcode, allow_blank: true, length: { minimum: 5, maximum: 5 }
  validates :vname, presence: true
  validates :catm, allow_blank: true, allow_nil: true,
                   length: { minimum: 8, maximum: 8 }
  validates :ctype, presence: true, length: { minimum: 1, maximum: 1 }
  validates :subtype, presence: true, length: { minimum: 2, maximum: 2 }
  validates :commustyle, presence: true, length: { minimum: 1, maximum: 1 }
  validates :dogs, presence: true, length: { minimum: 1, maximum: 1 }
  validates :addr_changwat, allow_blank: true, presence: true
  validates :addr_amphur, allow_blank: true, presence: true
  validates :addr_tambon, allow_blank: true, presence: true
  validates :datesurvey, presence: true
  # validates :nhouse, numericality: { greater_than_or_equal_to: 0 }

  # Enum
  enum commu_style: {
    edge_town: 1,
    crowded: 2,
    housing_estate: 3,
    town: 4,
    community_housing: 5,
    high_building: 6
  }

  private

  def autoid
    site = Site.hospcode hospcode
    no = site.cs_no + 1
    site.update(cs_no: no)
    "#{hospcode}#{no.to_s.rjust(9, '0')}"
  end

  def init
    self.datesurvey = Date.today if datesurvey.nil? && new_record?
    self.addr_changwat_id = 1 if addr_changwat_id.nil? && new_record? # default กทม
  end

  def pre_create
    self.vid = vid || autoid
    reset
    update_u_date
  end

  def pre_update
    reset
    update_u_date
  end

  def update_u_date
    self.u_date = updated_at
  end

  def reset
    if riskenvironment == '2'
      self.riskentype11 = nil
      self.riskentype12 = nil
      self.riskentype13 = nil
    end
  end
end
