class ApplicationController < ActionController::Base
  include SessionsHelper
  include ApplicationHelper
  include ReportsHelper
  include HomeHelper
  before_action :authenticate_user!

  def convert_buddhist_year_to_gregorian_year(date)
    split = date.split('/')
    split[2] = split[2].to_i - 543
    split.join('/')
  end
end
