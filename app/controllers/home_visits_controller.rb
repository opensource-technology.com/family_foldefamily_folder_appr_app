class HomeVisitsController < ApplicationController
  before_action :set_community_survey
  before_action :set_house
  before_action :set_family_folder
  before_action :set_person
  before_action :set_home_visit, only: [:show, :edit, :update, :destroy]

  # GET /home_visits
  # GET /home_visits.json
  def index
    @home_visits = HomeVisit.where(person: @person).page page_param
  end

  # GET /home_visits/1
  # GET /home_visits/1.json
  def show
  end

  # GET /home_visits/new
  def new
    @home_visit = HomeVisit.new
  end

  # GET /home_visits/1/edit
  def edit
  end

  # POST /home_visits
  # POST /home_visits.json
  def create
    @home_visit = HomeVisit.new(home_visit_params)
    respond_to do |format|
      if @home_visit.save
        timestamp = Time.now
        logger.info "[FF] Home Visit [#{@home_visit.id}] created by #{current_user.username} at #{timestamp}"
        AuditLog.create user: current_user.username, category: 'HOME_VISIT', action: "created [#{@home_visit.id}] at #{timestamp}"
        format.html do
          redirect_to  community_survey_house_family_folder_person_home_visit_path(@community_survey, @house, @family_folder, @person, @home_visit),
                       notice: t('home_visit.m_created_label')
        end
        format.json { render :show, status: :created, location: @home_visit }
      else
        format.html { render :new }
        format.json { render json: @home_visit.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /home_visits/1
  # PATCH/PUT /home_visits/1.json
  def update
    respond_to do |format|
      if @home_visit.update(home_visit_params)
        timestamp = Time.now
        logger.info "[FF] Home Visit [#{@home_visit.id}] updated by #{current_user.username} at #{timestamp}"
        AuditLog.create user: current_user.username, category: 'HOME_VISIT', action: "updated [#{@home_visit.id}] at #{timestamp}"
        format.html do
          redirect_to  community_survey_house_family_folder_person_home_visit_path(@community_survey, @house, @family_folder, @person, @home_visit),
                       notice: t('home_visit.m_updated_label')
        end
        format.json { render :show, status: :ok, location: @home_visit }
      else
        format.html { render :edit }
        format.json { render json: @home_visit.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /home_visits/1
  # DELETE /home_visits/1.json
  def destroy
    timestamp = Time.now
    @home_visit.destroy
    logger.info "[FF] Home Visit [#{@home_visit.id}] destroyed by #{current_user.username} at #{timestamp}"
    AuditLog.create user: current_user.username, category: 'HOME_VISIT', action: "destroyed [#{@home_visit.id}] at #{timestamp}"
    respond_to do |format|
      format.html do
        redirect_to  community_survey_house_family_folder_person_home_visits_path(@community_survey, @house, @family_folder, @person),
                     notice: t('home_visit.m_deleted_label')
      end
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_home_visit
    @home_visit = HomeVisit.find(params[:id])
  end

  def set_person
    @person = Person.find(params[:person_id])
  end

  def set_family_folder
    @family_folder = FamilyFolder.find(params[:family_folder_id])
  end

  def set_house
    @house = House.find(params[:house_id])
  end

  def set_community_survey
    @community_survey = CommunitySurvey.find(params[:community_survey_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def home_visit_params
    datesurvey_param = params.fetch(:home_visit, {}).fetch(:datesurvey)
    params.fetch(:home_visit, {})
          .merge(hospcode: current_user.hospcode,
                 family_folder: @family_folder,
                 person: @person,
                 house: @house,
                 user_last_updated_at: DateTime.now,
                 datesurvey: convert_buddhist_year_to_gregorian_year(datesurvey_param)).permit!
  end
end
