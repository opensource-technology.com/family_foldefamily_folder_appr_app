# require_relative '../services/authentication'

class SessionsController < ApplicationController
  skip_before_action :authenticate_user!, only: %i[new create destroy]
  
  include UserHelper if Rails.configuration.external_auth

  def new
    @user ||= User.new
  end

  def create
    return external_login if Rails.configuration.external_auth

    internal_login
  end

  def destroy
    timestamp = Time.now
    logger.info "[FF] #{current_user.username} logged out at #{timestamp}"
    AuditLog.create user: current_user.username, category: 'SESSION', action: "logged out at #{timestamp}"
    logout
    reset_session
    redirect_to login_url
  end

  private

  def login_params
    params.fetch(:user, {}).permit(:username, :password)
  end

  def external_login
    @user = authenticate(login_params[:username], login_params[:password])
    if !@user.present?
      flash[:error] = t(:error_password_mismatch)
    else
      login(@user)
      timestamp = Time.now
      logger.info "[FF] #{@user.username} logged in at #{timestamp}"
      AuditLog.create user: current_user.username, category: 'SESSION', action: "logged in at #{timestamp}"
      return redirect_to root_url
    end
    @user = User.new(login_params)
    render :new
  rescue OpenSSL::SSL::SSLError
    flash[:error] = t(:error_ssl_certificate_verify_failed)
    @user = User.new(login_params)
    render :new
  end

  def internal_login
    @user = User.find_by(username: login_params[:username])
    if !@user.present?
      flash[:error] = t(:error_user_does_not_exist)
    elsif @user.authenticate(login_params[:password])
      reset_session
      login @user
      timestamp = Time.now
      logger.info "[FF] #{@user.username} logged in at #{timestamp}"
      AuditLog.create user: current_user.username, category: 'SESSION', action: "logged in at #{timestamp}"
      return redirect_to root_url
    else
      flash[:error] = t(:error_password_mismatch)
    end
    @user = User.new(login_params)
    render :new
  end
end
