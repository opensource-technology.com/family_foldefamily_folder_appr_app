class CommunitySurveysController < ApplicationController
  include AddressHelper
  skip_before_action :authenticate_user!, only: [:search_main]
  before_action :set_community_survey, only: [:show, :edit, :update, :destroy]
  # GET /community_surveys
  # GET /community_surveys.json
  def index
    @community_surveys = CommunitySurvey.where(hospcode: current_user.hospcode)
                                        .order(created_at: :desc).page page_param
    @community_surveys = @community_surveys.filter_by_vid(params[:vid]) if params[:vid].present?
  end

  # GET /community_surveys/1
  # GET /community_surveys/1.json
  def show
    load_addr @community_survey
  end

  # GET /community_surveys/new
  def new
    @community_survey = CommunitySurvey.new
    load_addr @community_survey
  end

  # GET /community_surveys/1/edit
  def edit
    load_addr @community_survey
  end

  # POST /community_surveys
  # POST /community_surveys.json
  def create
    tambon = AddrTambon.find(community_survey_params.fetch(:addr_tambon_id))
    @community_survey = CommunitySurvey.new(riskentype(community_survey_params.merge(catm: "#{tambon.code}00")))
    respond_to do |format|
      if @community_survey.save
        timestamp = Time.now
        logger.info "[FF] Community Survey [#{@community_survey.id}] created by #{current_user.username} at #{timestamp}"
        AuditLog.create user: current_user.username, category: 'COMMUNITY_SURVEY', action: "created [#{@community_survey.id}] at #{timestamp}"
        format.html do
          redirect_to community_survey_path(@community_survey),
                      notice: t('community_survey.m_created_label')
        end
        format.json { render :show, status: :created, location: @community_survey }
      else
        load_addr @community_survey
        format.html { render :new }
        format.json { render json: @community_survey.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /community_surveys/1
  # PATCH/PUT /community_surveys/1.json
  def update
    respond_to do |format|
      if @community_survey.update(riskentype(community_survey_params))
        timestamp = Time.now
        logger.info "[FF] Community Survey [#{@community_survey.id}] updated by #{current_user.username} at #{timestamp}"
        AuditLog.create user: current_user.username, category: 'COMMUNITY_SURVEY', action: "updated [#{@community_survey.id}] at #{timestamp}"
        format.html do
          redirect_to community_survey_path(@community_survey),
                      notice: t('community_survey.m_updated_label')
        end
        format.json { render :show, status: :ok, location: @community_survey }
      else
        load_addr @community_survey
        format.html { render :edit }
        format.json { render json: @community_survey.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /community_surveys/1
  # DELETE /community_surveys/1.json
  def destroy
    @community_survey.destroy
    timestamp = Time.now
    logger.info "[FF] Community Survey [#{@community_survey.id}] destroyed by #{current_user.username} at #{timestamp}"
    AuditLog.create user: current_user.username, category: 'COMMUNITY_SURVEY', action: "destroyed [#{@community_survey.id}] at #{timestamp}"
    respond_to do |format|
      format.html do
        redirect_to community_surveys_url,
                    notice: t('community_survey.m_deleted_label')
      end
      format.json { head :no_content }
    end
  end

  def search_main
    @main_communities = CommunitySurvey.where(ctype: 1).map(&:vname)
  end

  private

  def riskentype(community_survey_params)
    community_survey_params[:riskentype11].nil? && community_survey_params[:riskentype11] = nil
    community_survey_params[:riskentype12].nil? && community_survey_params[:riskentype12] = nil
    community_survey_params[:riskentype13].nil? && community_survey_params[:riskentype13] = nil
    community_survey_params
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_community_survey
    @community_survey = CommunitySurvey.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def community_survey_params
    datesurvey_param = params.fetch(:community_survey, {}).fetch(:datesurvey)
    params.fetch(:community_survey, {}).merge(hospcode: current_user.hospcode, datesurvey: convert_buddhist_year_to_gregorian_year(datesurvey_param)).permit!
  end
end
