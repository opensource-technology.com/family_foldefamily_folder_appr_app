class HospitalsController < ApplicationController
  def search
    q = params[:q]
    query = "code like :q or label like :q"
    hospitals = Hospital.where(query, q: "#{q}%")

    results = {
      results: hospitals.map { |h| { title: "#{h.code}: #{h.label}" } }
    }
    render json: results.as_json
  end

  def by_type
    type_id = params[:id]
    hospitals = if(type_id.to_i == 0)
                Hospital.where.not(htype: [1, 3, 4])
              else
                Hospital.where(htype: type_id)
              end
    results = hospitals.map { |h| [h.label, h.code] }
    render json: results.as_json
  end
end
