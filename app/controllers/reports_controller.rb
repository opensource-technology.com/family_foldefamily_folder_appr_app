require 'object_result'

class ReportsController < ApplicationController
  include ReportsHelper
  skip_before_action :authenticate_user!, only: [:search_main]

  def admin_houses
    @current_site = current_site
    @searchable = Searchable.new report_params
    @results = []
    return unless report_params.keys.size.positive?

    @count = execute_query("#{@searchable.template}/count_houses", @searchable)[0].symbolize_keys
    @obj_results = execute_query("#{@searchable.template}/result_houses", @searchable).map do |r|
      ObjResult.new(r.symbolize_keys)
    end
    @results = Kaminari.paginate_array(@obj_results, total_count: @count[:count]).page(page_param)

    respond_to do |format|
      format.html
      format.xlsx do
        @results = execute_query("#{@searchable.template}/export/result_houses", @searchable).map do |r|
          ObjResult.new(r.symbolize_keys)
        end
        template = "xlsx/#{@searchable.template}/house.xlsx.axlsx"
        render xlsx: "admin_#{@searchable.template == 'admin/hs' ? 'hs' : 'hv'}_house_#{@searchable.hospital}_#{@searchable.vid}_#{@searchable.from_date}_#{@searchable.to_date}", template: template
      end
    end
  end

  def admin_people
    @current_site = current_site
    @searchable = Searchable.new report_params
    @results = []
    return unless report_params.keys.size.positive?

    @count = execute_query("#{@searchable.template}/count_people", @searchable)[0].symbolize_keys
    @obj_results = execute_query("#{@searchable.template}/result_people", @searchable).map do |r|
      ObjResult.new(r.symbolize_keys)
    end
    @results = Kaminari.paginate_array(@obj_results, total_count: @count[:count]).page(page_param)

    respond_to do |format|
      format.html
      format.xlsx do
        @results = execute_query("#{@searchable.template}/export/result_people", @searchable).map do |r|
          ObjResult.new(r.symbolize_keys)
        end
        template = "xlsx/#{@searchable.template}/people.xlsx.axlsx"
        render xlsx: "admin_#{@searchable.template == 'admin/hs' ? 'hs' : 'hv'}_people_#{@searchable.hospital}_#{@searchable.vid}_#{@searchable.hid}_#{@searchable.from_date}_#{@searchable.to_date}", template: template
      end
    end
  end

  def admin_health_survey
    @current_site = current_site
    @searchable = Searchable.new report_params.merge(template: 'admin/hs')
    @searchable.htype = 3 if current_user.advanced_user?
    @results = []
    @hospitals_select = Hospital.where(htype: @searchable.htype).map { |h| ["#{h.code}: #{h.label}", h.code] }
    return unless report_params.keys.size.positive?

    @count = execute_query("#{@searchable.template}/count_community", @searchable)[0].symbolize_keys
    @obj_results = execute_query("#{@searchable.template}/result_community", @searchable).map do |r|
      ObjResult.new(r.symbolize_keys)
    end
    @results = Kaminari.paginate_array(@obj_results, total_count: @count[:count]).page(page_param)

    respond_to do |format|
      format.html
      format.xlsx do
        @results = execute_query("#{@searchable.template}/export/result_community", @searchable).map do |r|
          ObjResult.new(r.symbolize_keys)
        end
        template = "xlsx/#{@searchable.template}/community.xlsx.axlsx"
        render xlsx: "admin_hs_community_#{@searchable.hospital}_#{@searchable.from_date}_#{@searchable.to_date}", template: template
      end
    end
  end

  def admin_home_visit
    @current_site = current_site
    @searchable = Searchable.new report_params.merge(template: 'admin/hv')
    @searchable.htype = 3 if current_user.advanced_user?
    @results = []
    @hospitals_select = Hospital.where(htype: @searchable.htype).map { |h| ["#{h.code}: #{h.label}", h.code] }
    return unless report_params.keys.size.positive?

    @count = execute_query("#{@searchable.template}/count_community", @searchable)[0].symbolize_keys
    @obj_results = execute_query("#{@searchable.template}/result_community", @searchable).map do |r|
      ObjResult.new(r.symbolize_keys)
    end
    @results = Kaminari.paginate_array(@obj_results, total_count: @count[:count]).page(page_param)

    respond_to do |format|
      format.html
      format.xlsx do
        @results = execute_query("#{@searchable.template}/export/result_community", @searchable).map do |r|
          ObjResult.new(r.symbolize_keys)
        end
        template = "xlsx/#{@searchable.template}/community.xlsx.axlsx"
        render xlsx: "admin_hv_community_#{@searchable.hospital}_#{@searchable.from_date}_#{@searchable.to_date}", template: template
      end
    end
  end

  def site_health_survey
    @current_site = current_site
    @searchable = Searchable.new report_params.merge(template: 'site/hs')
    @results = []
    return unless report_params.keys.size.positive?

    @count = execute_query("#{@searchable.template}/count_community", @searchable)[0].symbolize_keys
    @obj_results = execute_query("#{@searchable.template}/result_community", @searchable).map do |r|
      ObjResult.new(r.symbolize_keys)
    end
    @results = Kaminari.paginate_array(@obj_results, total_count: @count[:count]).page(page_param)

    respond_to do |format|
      format.html
      format.xlsx do
        @results = execute_query("#{@searchable.template}/export/result_community", @searchable).map do |r|
          ObjResult.new(r.symbolize_keys)
        end
        template = "xlsx/#{@searchable.template}/community.xlsx.axlsx"
        render xlsx: "site_hs_community_#{current_site[:code]}_#{@searchable.from_date}_#{@searchable.to_date}", template: template
      end
    end
  end

  def site_home_visit
    @current_site = current_site
    @searchable = Searchable.new report_params.merge(template: 'site/hv')
    @results = []
    return unless report_params.keys.size.positive?

    @count = execute_query("#{@searchable.template}/count_community", @searchable)[0].symbolize_keys
    @obj_results = execute_query("#{@searchable.template}/result_community", @searchable).map do |r|
      ObjResult.new(r.symbolize_keys)
    end
    @results = Kaminari.paginate_array(@obj_results, total_count: @count[:count]).page(page_param)

    respond_to do |format|
      format.html
      format.xlsx do
        @results = execute_query("#{@searchable.template}/export/result_community", @searchable).map do |r|
          ObjResult.new(r.symbolize_keys)
        end
        template = "xlsx/#{@searchable.template}/community.xlsx.axlsx"
        render xlsx: "site_hv_community_#{current_site[:code]}_#{@searchable.from_date}_#{@searchable.to_date}", template: template
      end
    end
  end

  def site_houses
    @current_site = current_site
    @searchable = Searchable.new report_params
    @results = []
    return unless report_params.keys.size.positive?

    @count = execute_query("#{@searchable.template}/count_houses", @searchable)[0].symbolize_keys
    @obj_results = execute_query("#{@searchable.template}/result_houses", @searchable).map do |r|
      ObjResult.new(r.symbolize_keys)
    end
    @results = Kaminari.paginate_array(@obj_results, total_count: @count[:count]).page(page_param)

    respond_to do |format|
      format.html
      format.xlsx do
        @results = execute_query("#{@searchable.template}/export/result_houses", @searchable).map do |r|
          ObjResult.new(r.symbolize_keys)
        end
        template = "xlsx/#{@searchable.template}/house.xlsx.axlsx"
        render xlsx: "site_#{@searchable.template == 'admin/hs' ? 'hs' : 'hv'}_house_#{current_site[:code]}_#{@searchable.vid}_#{@searchable.from_date}_#{@searchable.to_date}", template: template
      end
    end
  end

  def site_people
    @current_site = current_site
    @searchable = Searchable.new report_params
    @results = []
    return unless report_params.keys.size.positive?

    @count = execute_query("#{@searchable.template}/count_people", @searchable)[0].symbolize_keys
    @obj_results = execute_query("#{@searchable.template}/result_people", @searchable).map do |r|
      ObjResult.new(r.symbolize_keys)
    end
    @results = Kaminari.paginate_array(@obj_results, total_count: @count[:count]).page(page_param)

    respond_to do |format|
      format.html
      format.xlsx do
        @results = execute_query("#{@searchable.template}/export/result_people", @searchable).map do |r|
          ObjResult.new(r.symbolize_keys)
        end
        template = "xlsx/#{@searchable.template}/people.xlsx.axlsx"
        render xlsx: "site_#{@searchable.template == 'admin/hs' ? 'hs' : 'hv'}_people_#{current_site[:code]}_#{@searchable.vid}_#{@searchable.hid}_#{@searchable.from_date}_#{@searchable.to_date}", template: template
      end
    end
  end

  private

  def report_params
    params.except(:controller, :action).permit!
  end
end
