class HomeController < ApplicationController
  layout 'sessions'
  skip_before_action :verify_authenticity_token

  before_action :load_community_surveys, only: %i[house house_health_survey person person_health_survey home_visit]

  def index; end

  def house
    render :house, layout: false
  end

  def house_health_survey
    render :house_health_survey, layout: false
  end

  def person
    render :person, layout: false
  end

  def person_health_survey
    render :person_health_survey, layout: false
  end

  def home_visit
    render :home_visit, layout: false
  end

  private

  def load_community_surveys
    @community_surveys = CommunitySurvey.where(hospcode: current_user.hospcode)
                                        .order(created_at: :desc)
  end
end
