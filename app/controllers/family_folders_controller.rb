class FamilyFoldersController < ApplicationController
  before_action :set_community_survey
  before_action :set_house
  before_action :set_family_folder, only: [:show, :edit, :update, :destroy]

  # GET /family_folders
  # GET /family_folders.json
  def index
    @family_folders = FamilyFolder.where(house: @house)
    @family_folder = FamilyFolder.new
    @family_folders = @family_folders.filter_by_family_folder_id(params[:family_folder_id]) if params[:family_folder_id].present?
    respond_to do |format|
      format.html { render :index }
      format.json do
        family_folders = @family_folders.map { |f| [f.short, f.ffolder_id] }
        label = family_folders.empty? ? t(:no_data_label) : t(:please_select_label)
        family_folders = family_folders.unshift([label, '""'])
        render json: family_folders, status: :ok
      end
    end
  end

  # GET /family_folders/1
  # GET /family_folders/1.json
  def show
  end

  # GET /family_folders/new
  def new
    @family_folder = FamilyFolder.new
  end

  # GET /family_folders/1/edit
  def edit
  end

  # POST /family_folders
  # POST /family_folders.json
  def create
    @family_folder = FamilyFolder.new(family_folder_params)

    respond_to do |format|
      if @family_folder.save
        timestamp = Time.now
        logger.info "[FF] Family Folder [#{@family_folder.id}] created by #{current_user.username} at #{timestamp}"
        AuditLog.create user: current_user.username, category: 'FAMILY_FOLDER', action: "created [#{@family_folder.id}] at #{timestamp}"
        format.html do
          redirect_to community_survey_house_family_folders_path(@community_survey, @house),
                      notice: t('family_folder.m_created_label')
        end
        format.json { render :show, status: :created, location: @family_folder }
      else
        format.html { render :new }
        format.json { render json: @family_folder.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /family_folders/1
  # PATCH/PUT /family_folders/1.json
  def update
    respond_to do |format|
      if @family_folder.update(family_folder_params)
        timestamp = Time.now
        logger.info "[FF] Family Folder [#{@family_folder.id}] updated by #{current_user.username} at #{timestamp}"
        AuditLog.create user: current_user.username, category: 'FAMILY_FOLDER', action: "updated [#{@family_folder.id}] at #{timestamp}"
        format.html { redirect_to @family_folder, notice: t('family_folder.m_updated_label') }
        format.json { render :show, status: :ok, location: @family_folder }
      else
        format.html { render :edit }
        format.json { render json: @family_folder.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /family_folders/1
  # DELETE /family_folders/1.json
  def destroy
    @family_folder.destroy
    timestamp = Time.now
    logger.info "[FF] Family Folder [#{@family_folder.id}] destroyed by #{current_user.username} at #{timestamp}"
    AuditLog.create user: current_user.username, category: 'FAMILY_FOLDER', action: "destroyed [#{@family_folder.id}] at #{timestamp}"
    respond_to do |format|
      format.html { redirect_to community_survey_house_family_folders_path(@community_survey, @house), notice: t('family_folder.m_deleted_label') }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_family_folder
    @family_folder = FamilyFolder.find(params[:id])
  end

  def set_house
    @house = House.find(params[:house_id])
  end

  def set_community_survey
    @community_survey = CommunitySurvey.find(params[:community_survey_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def family_folder_params
    params.fetch(:family_folder, {}).permit!
  end
end
