class HousesController < ApplicationController
  include AddressHelper
  before_action :set_community_survey
  before_action :set_house, only: [:show, :edit, :update, :destroy]

  # GET /houses
  # GET /houses.json
  def index
    respond_to do |format|
      format.html do
        @houses = House.where(community_survey: @community_survey).order(created_at: :desc).page(page_param)
        @houses = @houses.filter_by_hid(params[:hid]) if params[:hid].present?
        render :index
      end
      format.json do
        @houses = House.where(community_survey: @community_survey).order(created_at: :desc)
        houses = @houses.map { |h| [h.short, h.hid] }
        label = houses.empty? ? t(:no_data_label) : t(:please_select_label)
        houses = houses.unshift([label, '""'])
        render json: houses, status: :ok
      end
    end
  end

  # GET /houses/1
  # GET /houses/1.json
  def show
    @family_folders = FamilyFolder.where(house: @house)
    load_addr @house
  end

  # GET /houses/new
  def new
    @house = House.new addr_changwat_id: @community_survey.addr_changwat_id,
                       addr_amphur_id: @community_survey.addr_amphur_id, addr_tambon_id: @community_survey.addr_tambon_id
    load_addr @house
  end

  # GET /houses/1/edit
  def edit
    load_addr @house
    # @external = true if request.referer && request.referer.include?('family_folders')
  end

  # POST /houses
  # POST /houses.json
  def create
    tambon = AddrTambon.find(house_params.fetch(:addr_tambon_id))
    @house = House.new(house_params.merge(catm: "#{tambon.code}00"))

    respond_to do |format|
      if @house.save
        timestamp = Time.now
        logger.info "[FF] House [#{@house.id}] created by #{current_user.username} at #{timestamp}"
        AuditLog.create user: current_user.username, category: 'HOUSE', action: "created [#{@house.id}] at #{timestamp}"
        # Create Family Folder
        ff = FamilyFolder.create house: @house, hospcode: current_user.hospcode
        AuditLog.create user: current_user.username, category: 'FAMILY_FOLDER', action: "created [#{ff.id}] at #{timestamp}"
        format.html do
          redirect_to community_survey_house_path(@community_survey, @house),
                      notice: t('house.m_created_label')
        end
        format.json { render :show, status: :created, location: @house }
      else
        load_addr @house
        format.html { render :new }
        format.json { render json: @house.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /houses/1
  # PATCH/PUT /houses/1.json
  def update
    respond_to do |format|
      if @house.update(house_params)
        timestamp = Time.now
        logger.info "[FF] House [#{@house.id}] updated by #{current_user.username} at #{timestamp}"
        AuditLog.create user: current_user.username, category: 'HOUSE', action: "updated [#{@house.id}] at #{timestamp}"
        format.html do
          redirect_to community_survey_house_path(@community_survey, @house),
                      notice: t('house.m_updated_label')
        end
        format.json { render :show, status: :ok, location: @house }
      else
        load_addr @house
        format.html { render :edit }
        format.json { render json: @house.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /houses/1
  # DELETE /houses/1.json
  def destroy
    timestamp = Time.now
    @house.destroy
    logger.info "[FF] House [#{@house.id}] destroyed by #{current_user.username} at #{timestamp}"
    AuditLog.create user: current_user.username, category: 'HOUSE', action: "destroyed [#{@house.id}] at #{timestamp}"
    respond_to do |format|
      format.html do
        redirect_to community_survey_houses_path(@community_survey),
                    notice: t('house.m_deleted_label')
      end
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_house
    @house = House.find(params[:id])
  end

  def set_community_survey
    @community_survey = CommunitySurvey.find(params[:community_survey_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def house_params
    datesurvey_param = params.fetch(:house, {}).fetch(:datesurvey)
    params.fetch(:house, {}).merge(hospcode: current_user.hospcode,
                                   community_survey: @community_survey,
                                   datesurvey: convert_buddhist_year_to_gregorian_year(datesurvey_param)).permit!
  end
end
