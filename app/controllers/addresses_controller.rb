class AddressesController < ApplicationController
  def update_amphur
    current_changwat = AddrChangwat.find(params[:id])
    amphurs = AddrAmphur.where("code like ? and label not like '%*%'", "#{current_changwat.code}%").order(:label)
    render json: amphurs.map { |a| [a.label, a.id] }.unshift([t('please_select_label'), '""'])
  end

  def update_tambon
    current_amphur = AddrAmphur.find(params[:id])
    tambons = AddrTambon.where("code like ? and label not like '%*%'", "#{current_amphur.code}%").order(:label)
    render json: tambons.map { |a| [a.label, a.id] }.unshift([t('please_select_label'), '""'])
  end
end
5