class HouseHealthSurveysController < ApplicationController
  before_action :set_community_survey
  before_action :set_house
  before_action :set_family_folder
  before_action :set_house_health_survey, only: [:show, :edit, :update, :destroy]

  # GET /house_health_surveys
  # GET /house_health_surveys.json
  def index
    @house_health_surveys = HouseHealthSurvey.where(family_folder: @family_folder)
                                             .order(created_at: :desc).page page_param
  end

  # GET /house_health_surveys/1
  # GET /house_health_surveys/1.json
  def show
  end

  # GET /house_health_surveys/new
  def new
    @house_health_survey = HouseHealthSurvey.new
  end

  # GET /house_health_surveys/1/edit
  def edit
  end

  # POST /house_health_surveys
  # POST /house_health_surveys.json
  def create
    @house_health_survey = HouseHealthSurvey.new(house_health_survey_params)

    respond_to do |format|
      if @house_health_survey.save
        timestamp = Time.now
        logger.info "[FF] House Health Survey [#{@house_health_survey.id}] created by #{current_user.username} at #{timestamp}"
        AuditLog.create user: current_user.username, category: 'HOUSE_HEALTH_SURVEY', action: "created [#{@house_health_survey.id}] at #{timestamp}"
        format.html do
          redirect_to(
            community_survey_house_family_folder_house_health_survey_path(@community_survey, @house,
                                                                          @family_folder, @house_health_survey),
            notice: t('house_health_survey.m_created_label')
          )
        end
        format.json { render :show, status: :created, location: @house_health_survey }
      else
        format.html { render :new }
        format.json { render json: @house_health_survey.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /house_health_surveys/1
  # PATCH/PUT /house_health_surveys/1.json
  def update
    respond_to do |format|
      if @house_health_survey.update(house_health_survey_params)
        timestamp = Time.now
        logger.info "[FF] House Health Survey [#{@house_health_survey.id}] updated by #{current_user.username} at #{timestamp}"
        AuditLog.create user: current_user.username, category: 'HOUSE_HEALTH_SURVEY', action: "updated [#{@house_health_survey.id}] at #{timestamp}"
        format.html do
          redirect_to(
            community_survey_house_family_folder_house_health_survey_path(@community_survey, @house,
                                                                          @family_folder, @house_health_survey),
            notice: t('house_health_survey.m_updated_label')
          )
        end
        format.json { render :show, status: :ok, location: @house_health_survey }
      else
        format.html { render :edit }
        format.json { render json: @house_health_survey.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /house_health_surveys/1
  # DELETE /house_health_surveys/1.json
  def destroy
    timestamp = Time.now
    @house_health_survey.destroy
    logger.info "[FF] House Health Survey [#{@house_health_survey.id}] destroyed by #{current_user.username} at #{timestamp}"
    AuditLog.create user: current_user.username, category: 'HOUSE_HEALTH_SURVEY', action: "destroyed [#{@house_health_survey.id}] at #{timestamp}"
    respond_to do |format|
      format.html do
        redirect_to community_survey_house_family_folder_house_health_surveys_path(@community_survey, @house, @family_folder),
                    notice: t('house_health_survey.m_deleted_label')
      end
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_house_health_survey
    @house_health_survey = HouseHealthSurvey.find(params[:id])
  end

  def set_family_folder
    @family_folder = FamilyFolder.find(params[:family_folder_id])
  end

  def set_house
    @house = House.find(params[:house_id])
  end

  def set_community_survey
    @community_survey = CommunitySurvey.find(params[:community_survey_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def house_health_survey_params
    datesurvey_param = params.fetch(:house_health_survey, {}).fetch(:datesurvey)
    params.fetch(:house_health_survey, {})
          .merge(hospcode: current_user.hospcode,
                 family_folder: @family_folder,
                 datesurvey: convert_buddhist_year_to_gregorian_year(datesurvey_param)).permit!
  end
end
