class PeopleController < ApplicationController
  before_action :set_community_survey
  before_action :set_house
  before_action :set_family_folder
  before_action :set_person, only: %i[show edit update destroy search]

  # GET /people
  # GET /people.json
  def index
    @people = Person.where(family_folder: @family_folder).order(created_at: :desc).page page_param
    @people = @people.filter_by_person_id(params[:person_id]) if params[:person_id].present?
    respond_to do |format|
      format.html { render :index }
      format.json do
        people = @people.map { |f| [f.short, f.person_id] }
        label = people.empty? ? t(:no_data_label) : t(:please_select_label)
        people = people.unshift([label, '""'])
        render json: people, status: :ok
      end
    end
  end

  # GET /people/1
  # GET /people/1.json
  def show
    @titles = Title.all
  end

  # GET /people/new
  def new
    @titles = Title.all
    @person = Person.new
    @person.diseases.build
  end

  # GET /people/1/edit
  def edit
    @titles = Title.all
  end

  # POST /people
  # POST /people.json
  def create
    @person = Person.new(person_params)

    if params[:add_disease]
      # add empty ingredient associated with @recipe
      @person.diseases.build
      @titles = Title.all
      render :new
    elsif params[:remove_disease]
      # nested model that have _destroy attribute = 1 automatically deleted by rails
      @titles = Title.all
      render :new
    else
      respond_to do |format|
        if @person.save
          timestamp = Time.now
          logger.info "[FF] Person [#{@person.id}] created by #{current_user.username} at #{timestamp}"
          AuditLog.create user: current_user.username, category: 'PERSON', action: "created [#{@person.id}] at #{timestamp}"
          format.html do
            redirect_to community_survey_house_family_folder_person_path(@community_survey, @house, @family_folder, @person),
                        notice: t('person.m_created_label')
          end
          format.json { render :show, status: :created, location: @person }
        else
          @titles = Title.all
          format.html { render :new }
          format.json { render json: @person.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /people/1
  # PATCH/PUT /people/1.json
  def update
    if params[:add_disease]
      unless params[:person][:diseases_attributes].blank?
        params[:person][:diseases_attributes].each do |attribute|
          @person.diseases.new(attribute.last.except(:_destroy).permit!) unless attribute.last.key?(:id)
        end
      end
      @person.diseases.build
      @titles = Title.all
      render :edit
    elsif params[:remove_disease]
      @titles = Title.all
      unless params[:person][:diseases_attributes].nil?
        removed_diseases = params[:person][:diseases_attributes].values.map do |att|
          att[:id] if att[:id] && att[:_destroy].to_i == 1
        end
        Disease.delete(removed_diseases)
        params[:person][:diseases_attributes].each do |attribute|
          @person.diseases.new(attribute.last.except(:_destroy).permit!) if !attribute.last.key?(:id) && attribute.last[:_destroy].to_i.zero?
        end
      end
      render :edit
    else
      respond_to do |format|
        if @person.update(person_params)
          timestamp = Time.now
          logger.info "[FF] Person [#{@person.id}] updated by #{current_user.username} at #{timestamp}"
          AuditLog.create user: current_user.username, category: 'PERSON', action: "updated [#{@person.id}] at #{timestamp}"
          format.html do
            redirect_to community_survey_house_family_folder_person_path(@community_survey, @house, @family_folder, @person),
                        notice: t('person.m_updated_label')
          end
          format.json { render :show, status: :ok, location: @person }
        else
          @titles = Title.all
          format.html { render :edit }
          format.json { render json: @person.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /people/1
  # DELETE /people/1.json
  def destroy
    timestamp = Time.now
    @person.destroy
    logger.info "[FF] Person [#{@person.id}] destroyed by #{current_user.username} at #{timestamp}"
    AuditLog.create user: current_user.username, category: 'PERSON', action: "destroyed [#{@person.id}] at #{timestamp}"
    respond_to do |format|
      format.html do
        redirect_to community_survey_house_family_folder_people_path(@community_survey, @house,
                                                                     @family_folder),
                    notice: t('person.m_deleted_label')
      end
      format.json { head :no_content }
    end
  end

  def search
    @people = Person.where(family_folder: @family_folder, person_id: @person.id).order(created_at: :desc).page page_param
    render :index
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_person
    @person = Person.find(params[:id])
  end

  def set_family_folder
    @family_folder = FamilyFolder.find(params[:family_folder_id])
  end

  def set_house
    @house = House.find(params[:house_id])
  end

  def set_community_survey
    @community_survey = CommunitySurvey.find(params[:community_survey_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def person_params
    datesurvey_param = params.fetch(:person, {}).fetch(:datesurvey)
    birthdate_param = params.fetch(:person, {}).fetch(:birth_date)

    diseases_params = params.fetch(:person, {}).fetch(:diseases_attributes, {})
    diseases_params.each do |k, v|
      diseases_params[k] = diseases_params[k].merge(sick_at: convert_buddhist_year_to_gregorian_year(v.fetch(:sick_at)))
    end

    params.fetch(:person, {}).merge(hospcode: current_user.hospcode,
                                    house: @house,
                                    family_folder: @family_folder,
                                    diseases_attributes: diseases_params,
                                    datesurvey: convert_buddhist_year_to_gregorian_year(datesurvey_param),
                                    birth_date: convert_buddhist_year_to_gregorian_year(birthdate_param)).permit!
  end
end
