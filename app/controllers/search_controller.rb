class SearchController < ApplicationController
  # skip_before_action :authenticate_user!, only: [:people]

  # def search_people
  #   persons = Person.includes([:house])
  #                   .where(hospcode: current_user.hospcode)
  #                   .where(
  #                     "
  #                       first_name SIMILAR TO :value
  #                       OR last_name SIMILAR TO :value
  #                       OR pid SIMILAR TO :value
  #                     ",
  #                     value: "%(#{request.params["value"].split(' ').collect(&:strip).join("|")})%"
  #                   ) if request.params["personId"].blank?
  #   persons = Person.includes(:house)
  #                   .where(person_id: request.params["personId"])
  #                   .where(hospcode: current_user.hospcode) unless request.params["personId"].blank?
  #   @length = persons.length
  #   @results = persons.page page_param

  #   respond_to do |format|
  #     format.html { render 'search/index' }
  #     format.json
  #     # { render json: persons.as_json(include: { house: { include: %i[community_survey addr_tambon addr_amphur addr_changwat] } }) }
  #   end
  # end

  def people
    @persons = Person.joins(house: :community_survey).where(
      'people.hospcode = :hospcode and (first_name like :value or last_name like :value or pid like :value)',
      { hospcode: current_user.hospcode, value: "#{params['value']}%" }
    )
    respond_to do |format|
      format.html { render 'search/index' }
      format.json
    end
  end
end
