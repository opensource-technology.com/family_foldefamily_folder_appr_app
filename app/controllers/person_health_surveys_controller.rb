class PersonHealthSurveysController < ApplicationController
  include HomeVisitsHelper
  before_action :set_community_survey
  before_action :set_house
  before_action :set_family_folder
  before_action :set_person
  before_action :set_person_health_survey, only: [:show, :edit, :update, :destroy]

  # GET /person_health_surveys
  # GET /person_health_surveys.json
  def index
    @person_health_surveys = PersonHealthSurvey.where(person: @person)
                                               .order(created_at: :desc).page page_param
  end

  # GET /person_health_surveys/1
  # GET /person_health_surveys/1.json
  def show
  end

  # GET /person_health_surveys/new
  def new
    @person_health_survey = PersonHealthSurvey.new
  end

  # GET /person_health_surveys/1/edit
  def edit
  end

  # POST /person_health_surveys
  # POST /person_health_surveys.json
  def create
    @person_health_survey = PersonHealthSurvey.new(person_health_survey_params)

    respond_to do |format|
      if @person_health_survey.save
        timestamp = Time.now
        logger.info "[FF] Person Health Survey [#{@person_health_survey.id}] created by #{current_user.username} at #{timestamp}"
        AuditLog.create user: current_user.username, category: 'PERSON_HEALTH_SURVEY', action: "created [#{@person_health_survey.id}] at #{timestamp}"
        format.html do
          redirect_to community_survey_house_family_folder_person_person_health_survey_path(@community_survey, @house, @family_folder,
                                                                                            @person, @person_health_survey),
                      notice: t('person_health_survey.m_created_label')
        end
        format.json { render :show, status: :created, location: @person_health_survey }
      else
        format.html { render :new }
        format.json { render json: @person_health_survey.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /person_health_surveys/1
  # PATCH/PUT /person_health_surveys/1.json
  def update
    respond_to do |format|
      if @person_health_survey.update(person_health_survey_params)
        timestamp = Time.now
        logger.info "[FF] Person Health Survey [#{@person_health_survey.id}] updated by #{current_user.username} at #{timestamp}"
        AuditLog.create user: current_user.username, category: 'PERSON_HEALTH_SURVEY', action: "updated [#{@person_health_survey.id}] at #{timestamp}"
        format.html do
          redirect_to community_survey_house_family_folder_person_person_health_survey_path(@community_survey, @house, @family_folder,
                                                                                            @person, @person_health_survey),
                      notice: t('person_health_survey.m_updated_label')
        end
        format.json { render :show, status: :ok, location: @person_health_survey }
      else
        format.html { render :edit }
        format.json { render json: @person_health_survey.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /person_health_surveys/1
  # DELETE /person_health_surveys/1.json
  def destroy
    timestamp = Time.now
    @person_health_survey.destroy
    logger.info "[FF] Person Health Survey [#{@person_health_survey.id}] destroyed by #{current_user.username} at #{timestamp}"
    AuditLog.create user: current_user.username, category: 'PERSON_HEALTH_SURVEY', action: "destroyed [#{@person_health_survey.id}] at #{timestamp}"
    respond_to do |format|
      format.html do
        redirect_to community_survey_house_family_folder_person_person_health_surveys_path(@community_survey, @house, @family_folder, @person),
                    notice: t('person_health_survey.m_deleted_label')
      end
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_person_health_survey
    @person_health_survey = PersonHealthSurvey.find(params[:id])
  end

  def set_person
    @person = Person.find(params[:person_id])
  end

  def set_family_folder
    @family_folder = FamilyFolder.find(params[:family_folder_id])
  end

  def set_house
    @house = House.find(params[:house_id])
  end

  def set_community_survey
    @community_survey = CommunitySurvey.find(params[:community_survey_id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def person_health_survey_params
    datesurvey_param = params.fetch(:person_health_survey, {}).fetch(:datesurvey)
    params.fetch(:person_health_survey, {})
          .merge(hospcode: current_user.hospcode,
                 family_folder: @family_folder,
                 user_last_updated_at: DateTime.now,
                 person: @person,
                 datesurvey: convert_buddhist_year_to_gregorian_year(datesurvey_param)).permit!
  end
end

