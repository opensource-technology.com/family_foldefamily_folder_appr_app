#!/bin/bash
dependencies() {
  sudo apt update
  version=`lsb_release -r -s`
  if [ $version == "18.04" ]; then
    sudo apt install -y autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm5 libgdbm-dev libpq-dev redis-server python2.7 git nginx
  else
    sudo apt install -y autoconf bison build-essential libssl-dev libyaml-dev libreadline6-dev zlib1g-dev libncurses5-dev libffi-dev libgdbm3 libgdbm-dev libpq-dev redis-server python2.7 git nginx
  fi
}
# Install dependencies
dependencies

# Install postgresql
echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" | sudo tee /etc/apt/sources.list.d/pgdg.list

wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt update
sudo apt install postgresql -y

git clone https://github.com/rbenv/rbenv.git ~/.rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc

export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"

git clone https://github.com/rbenv/ruby-build.git ~/.rbenv/plugins/ruby-build
rbenv install 2.6.4
rbenv global 2.6.4
gem install rails
gem install puma
gem install bundler

git clone http://gitlab.opensource-technology.com:9090/karn/family_folder_app
cd family_folder_app
gem update --system
bundle install
bundle update --bundler

cp config/example.app.env config/app.env
export $(cat config/app.env |xargs)

rails deploy:make_key
rails deploy:systemd
