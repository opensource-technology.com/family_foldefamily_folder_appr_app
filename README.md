# Family Folder

## Installation
### Prerequisite
- PostgreSQL 11.0.x
- Rails 5.2.3
- Ruby 2.5.1

## Issues
- เมื่อใช้งานบน server สปสช. จะมีปัญหาเมื่อทำการกดเลือกเมนู แล้ว request ที่วิ่งไปหา server ไม่เป็น https ด้วยทำให้ไม่สามารถดึงข้อมูลอื่นๆ มาแสดงได้ **วิธีแก้** ปัญหาให้เพิ่ม meta tag header ด้านล่างเข้าไป

```
<meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
```