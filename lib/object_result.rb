class ObjResult
  attr_accessor :vname, :vid, :count_houses, :count_people, :hid, :h_datesurvey, :house,
                :first_name, :last_name, :person_id, :p_datesurvey, :created_at, :all_people,
                :pid, :datesurvey, :status_description, :status, :pass, :fail, :waiting, :sent_status,
                :sent_errors, :hospital_label, :count_survey
  def initialize(result)
    @vname = result[:vname]
    @vid = result[:vid]
    @count_houses = result[:count_houses]
    @count_people = result[:count_people]
    @hid = result[:hid]
    @h_datesurvey = result[:h_datesurvey]
    @house = result[:house]
    @first_name = result[:first_name]
    @last_name = result[:last_name]
    @person_id = result[:person_id]
    @p_datesurvey = result[:p_datesurvey]
    @created_at = result[:created_at]
    @all_people = result[:all_people]
    @pass = result[:pass]
    @fail = result[:fail]
    @pid = result[:pid]
    @datesurvey = result[:datesurvey]
    @status = result[:status]
    @status_description = result[:status_description]
    @sent_status = result[:sent_status]
    @sent_errors = result[:sent_errors]
    @waiting = result[:waiting]
    @hospital_label = result[:hospital_label]
    @count_survey = result[:count_survey]
  end
end
