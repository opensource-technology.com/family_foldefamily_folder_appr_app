select community_surveys.vid,
       people.person_id,
       people.first_name,
       people.last_name,
       houses.house,
       people.datesurvey as p_datesurvey
from people
left join houses on houses.hid::text = people.house_id::text
left join community_surveys on community_surveys.vid::text = houses.community_survey_id::text
where to_char({date_type}, 'YYYY-MM') between '{from_month_year}' and '{to_month_year}'
      and community_surveys.vid = '{vid}'
group by community_surveys.vname,
         community_surveys.vid,
         people.person_id,
         houses.hid
