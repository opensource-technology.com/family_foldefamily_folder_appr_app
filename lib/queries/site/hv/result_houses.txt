select community_surveys.vid
    ,community_surveys.vname
    ,houses.hid
    ,houses.house
    ,count(distinct home_visits.person_id) as count_people
    ,count(home_visits.person_id) as count_survey
    ,sum(case when process_results.pass is null then 0 else process_results.pass end) as pass
    ,sum(case when process_results.fail is null then 0 else process_results.fail end) as fail
    ,sum(case when process_results.waiting is null then 1 else process_results.waiting end) as waiting
from home_visits
    inner join people on people.person_id = home_visits.person_id
    inner join houses on houses.hid = people.house_id
    inner join community_surveys on community_surveys.vid = houses.community_survey_id
    left join (select process_results.client_code
                    ,process_results.hospcode
                    ,case when process_results.status in (2,4) then 1 else 0 end as pass
                    ,case when process_results.status = 3 then 1 else 0 end as fail
                    ,case when process_results.status in (0,1) or process_results.status is null then 1 else 0 end as waiting
                from process_results
                where process_type = 2
                ) as process_results on process_results.client_code = home_visits.hvid
where community_surveys.hospcode = '{hospcode}'
    and community_surveys.vid = '{vid}'
    and home_visits.{date_type}::date between '{from_date}' and '{to_date}'     
group by community_surveys.vname
    ,community_surveys.vid
    ,houses.hid
order by houses.hid
limit {per_page} offset {offset};