select
      community_surveys.vname,
      houses.house
from houses
    inner join community_surveys on community_surveys.vid::text = houses.community_survey_id::text
where to_char({date_type}, 'YYYY-MM-DD') between '{from_date}' and '{to_date}'
      and community_surveys.vid = '{vid}'
      and houses.hid = '{hid}'
group by community_surveys.vname,
         community_surveys.vid,
         houses.house
