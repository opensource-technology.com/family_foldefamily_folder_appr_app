select community_surveys.vid,
       houses.hid,
       houses.house,
       houses.datesurvey as h_datesurvey,
       houses.created_at,
       count(home_visits.person_id) as count_people,
       count(houses.nperson) as all_people
from home_visits
left join houses on houses.hid::text = home_visits.house_id::text
left join community_surveys on community_surveys.vid::text = houses.community_survey_id::text
where to_char({date_type}, 'YYYY-MM') between '{from_month_year}' and '{to_month_year}'
      and community_surveys.vid = '{vid}'
group by community_surveys.vname,
         community_surveys.vid,
         houses.hid,
         houses.house
order by houses.hid

