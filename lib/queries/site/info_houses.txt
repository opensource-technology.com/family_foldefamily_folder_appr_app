select count(houses.house),
       community_surveys.vname,
       count(houses.nperson)
from community_surveys
left join houses on community_surveys.vid::text = houses.community_survey_id::text
where to_char({date_type}, 'YYYY-MM-DD') between '{from_date}' and '{to_date}'
      and community_surveys.vid = '{vid}' 
      and community_surveys.hospcode = '{hospcode}'
group by community_surveys.vname,
         community_surveys.vid
