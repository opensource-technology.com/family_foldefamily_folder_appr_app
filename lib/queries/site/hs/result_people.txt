select houses.hid
    ,houses.house
    ,people.pid
    ,people.first_name
    ,people.last_name
    ,person_health_surveys.datesurvey
    ,person_health_surveys.updated_at as created_at
    ,process_results.status
    ,process_results.sent_status
    ,process_results.sent_errors
    ,process_results.status_description
from person_health_surveys
    inner join people on people.person_id = person_health_surveys.person_id
    inner join houses on houses.hid = people.house_id
    inner join community_surveys on community_surveys.vid = houses.community_survey_id
    left join process_results on process_results.client_code = person_health_surveys.phsurvey_id
            and process_results.process_type = '1'
where community_surveys.hospcode = '{hospcode}'
    and community_surveys.vid = '{vid}'
    and (case when '{hid}' = '' or '{hid}' is null or '{hid}' = 'all' then true else houses.hid = '{hid}' end)
    and person_health_surveys.{date_type}::date between '{from_date}' and '{to_date}'
    and (case when '{process_results}' = '' or '{process_results}' is null or '{process_results}' = 'all' 
                    then true 
                else (case when '{process_results}' = '1' then process_results.status::text in ('0','1') or process_results.status is null 
                           when '{process_results}' = '2' then process_results.status::text in ('2','4')
                           when '{process_results}' = '3' then process_results.status::text = '3'
                        end)
            end) 
group by houses.hid
    ,houses.house
    ,people.pid
    ,people.first_name
    ,people.last_name
    ,person_health_surveys.datesurvey
    ,person_health_surveys.updated_at
    ,process_results.status
    ,process_results.sent_status
    ,process_results.sent_errors
    ,process_results.status_description
order by houses.hid
limit {per_page} offset {offset};