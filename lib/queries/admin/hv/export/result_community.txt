select community_surveys.vname
    ,community_surveys.vid
    ,community_surveys.hospcode
    ,hospitals.label as hospital_label
    ,count(distinct person.hid)  as count_houses
    ,count(distinct person.person_id) as count_people
    ,count(person.person_id) as count_survey
    ,sum(person.pass) as pass
    ,sum(person.fail) as fail
    ,sum(person.waiting) as waiting
from community_surveys
    inner join hospitals on hospitals.code = community_surveys.hospcode
    inner join (select distinct community_surveys.hospcode
                    ,community_surveys.vid
                    ,community_surveys.vname
                    ,houses.hid as hid
                    ,home_visits.datesurvey
                    ,home_visits.created_at
                    ,home_visits.person_id
                    ,process_results.client_code
                    ,case when process_results.status in (2,4) then 1 else 0 end as pass
                    ,case when process_results.status = 3 then 1 else 0 end as fail
                    ,case when process_results.status in (0,1) or process_results.status is null then 1 else 0 end as waiting
                from home_visits
                    inner join people on people.person_id = home_visits.person_id
                    inner join houses on houses.hid = people.house_id
                    inner join community_surveys on community_surveys.vid = houses.community_survey_id
                    left join process_results on process_results.client_code = home_visits.hvid
                    inner join hospitals on hospitals.code = community_surveys.hospcode
                where home_visits.{date_type}::date  between '{from_date}' and '{to_date}' {community_list}
               ) person on person.vid = community_surveys.vid

group by community_surveys.vname
    ,community_surveys.vid
    ,community_surveys.hospcode
    ,hospitals.label
order by community_surveys.vid;