require 'hshv'
require 'parallel'
require 'utils'

start_time = Time.now
range_date = (18...22).map do |i|
  "2019-09-#{i.to_s.rjust(2, '0')}"..."2019-09-#{(i + 1).to_s.rjust(2, '0')}"
end

range_date.each do |range|
  # hlist = PersonHealthSurvey.where(datesurvey: range, process_result: nil)
  hlist = HomeVisit.where(datesurvey: range, process_result: nil)
  items = hlist.map { |h| HSHV::Item.new h }
  c = HSHV::SendCommand.new
  HSHV::Commander.call c, list: items
end

end_time = Time.now

wait_time = end_time - start_time
puts "Wait: #{seconds_to_hms(wait_time.to_i)}"
