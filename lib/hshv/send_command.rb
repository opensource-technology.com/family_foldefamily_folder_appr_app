module HSHV
  class SendCommand < Command
    def call(*_, **kwargs)
      if ENV.key?('FIXED_TOKEN')
        token = ENV['FIXED_TOKEN']
      else
        params = { username: service_username, password: service_password, appid: 11 }
        login_res = Faraday.post("#{api_url}/auth/login", params.to_json)
        data = JSON.parse(login_res.body)
        token = data['token_key']
        EventLog.info("AUTH", "Authenticate with #{service_username} and received token #{token}")
      end

      return if token.nil?

      connection = Faraday.new("#{api_url}/v1/HVService/send_data") do |conn|
        conn.adapter :net_http
        conn.headers['token-key'] = token
        conn.headers['content-type'] = 'application/json'
      end

      # TODO: Load all here
      item_list = kwargs[:list]
      item_list.each do |item|
        res = connection.post("#{api_url}/v1/HVService/send_data", item.to_json)
        process_response(res, item)
      end
    end

    def create_process_result(params)
      pr = ProcessResult.create params.to_h
      update_reference(pr, params.process_type, params.client_code)
    end

    def process_response(res, item)
      if res.status == 200
        body = JSON.parse(res.body)
        parse_body(body, item)
      else # 400 and html
        send_command_failed(item)
      end
    rescue JSON::ParserError
      puts $ERROR_INFO
      EventLog.info("ERROR", "Cannot parse json from #{item.surveytype}-#{item.hv_client_code}")
    end

    def parse_body(body, item)
      if body['status'] == 'error'
        send_command_error(body, item)
      else
        send_command_success(body['record'], item)
      end
    end

    def send_command_error(data, item)
      pr = ProcessResult.find_by(client_code: item.hv_client_code)
      # check_reference(data['record'], item) if data.key?('record') # create check mulitple time

      if pr.present?
        pr.update(sent_status: 3, sent_errors: merge_errors(data['message'], parse_errors(data['error_list']))) if pr.sent_status != 2
      else
        params = OpenStruct.new process_type: item.surveytype, pid: item.pid, code_check: data['hv_code_check'],
                                client_code: item.hv_client_code, hospcode: item.hospcode, sent_status: 3,
                                sent_errors: merge_errors(data['message'], parse_errors(data['error_list']))
        create_process_result(params)
      end
      EventLog.info("SEND", "Send data to cloud success with error. [type: #{item.surveytype}, client_code: #{item.hv_client_code}, errors: #{merge_errors(data['message'], parse_errors(data['error_list']))}]")
    end

    def send_command_failed(item)
      # params = OpenStruct.new process_type: item.surveytype, pid: item.pid, code_check: '',
      # client_code: item.hv_client_code, hospcode: item.hospcode, sent_status: 3, sent_errors: 'Cannot connect server'
      # create_process_result(params)
      EventLog.info("SEND", "Send data to cloud faild. [type: #{item.surveytype}, client_code: #{item.hv_client_code}")
    end

    def send_command_success(record, item)
      params = OpenStruct.new process_type: item.surveytype, pid: item.pid, code_check: record['hv_code_check'],
                              client_code: item.hv_client_code, hospcode: item.hospcode, status: record['hv_import_status'],
                              status_description: record['hv_import_status_desc'], sent_status: 2, sent_errors: ''
      pr = ProcessResult.find_by(client_code: item.hv_client_code)
      if pr.present?
        pr.update(code_check: record['hv_code_check'], status: record['hv_import_status'],
                  status_description: record['hv_import_status_desc'],
                  sent_status: 2, sent_errors: '')
      else
        create_process_result(params)
      end
      EventLog.info("SEND", "Send data to cloud sucess. [type: #{params.process_type}, client_code: #{params.client_code}]")
    end

    def merge_errors(*errors)
      errors.map { |e| transform_error_message(e) }.join(',')
    end

    def transform_error_message(message)
      if message.include?("ไม่พบ Token")
        t('errors.nhso_internal_error')
      else
        message
      end
    end

    def update_reference(process_result, type, client_code)
      instance = get_instance(type, client_code)
      instance.update(process_result: process_result)
    end

    def check_reference(record, item)
      hv_code_check = record['hv_code_check']

      instance = get_instance(item.surveytype, item.hv_client_code)
      pr = instance.process_result
      if pr.present?
        # TODO: Update hv_code_check if nil
        pr.update(code_check: hv_code_check, sent_status: 2, sent_errors: '') if pr.code_check.nil?
      else
        # TODO: Create process result
        params = OpenStruct.new process_type: item.surveytype, pid: item.pid, code_check: hv_code_check,
                                client_code: item.hv_client_code, hospcode: item.hospcode, status: 1,
                                status_description: 'รอตรวจสอบข้อมูล', sent_status: 2, sent_errors: ''
        create_process_result(params)
      end
    end

    def get_instance(type, client_code)
      if type == 1
        PersonHealthSurvey.find_by(phsurvey_id: client_code)
      else
        HomeVisit.find_by(hvid: client_code)
      end
    end
  end
end
