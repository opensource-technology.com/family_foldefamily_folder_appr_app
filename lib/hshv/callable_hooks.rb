module HSHV
  module CallableHooks
    def self.included(base)
      base.extend(ClassMethods)
    end

    module ClassMethods
      def after_call(&block)
        @after_call = block
      end

      def before_call(&block)
        @before_call = block
      end
    end
  end
end
