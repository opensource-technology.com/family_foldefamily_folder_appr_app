require 'json'

module HSHV
  class Commander
    class << self
      def call(command, *args, **kwargs)
        command.execute(*args, **kwargs)
      end
    end
  end
end
