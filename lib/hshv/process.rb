module HSHV
  module Process
    # Load HV from yesterday
    def load_home_visit
      HomeVisit.where(process_result: nil, created_at: Date.yesterday...Date.today).limit(2000)
    end

    def load_home_visit_token_expired
      HomeVisit.joins(:process_result).where(process_results: { sent_status: 3 }).limit(2000)
    end

    # Load HS from yesterday
    def load_health_survey
      PersonHealthSurvey.where(process_result: nil, created_at: Date.yesterday...Date.today).limit(2000)
    end

    def load_health_survey_token_expired
      PersonHealthSurvey.joins(:process_result).where(process_results: { sent_status: 3 }).limit(2000)
    end

    def load_process_result
      ProcessResult.where(status: 1, sent_status: 2).order(created_at: :desc).limit(2000)
    end

    # def load_rest_process_results
    #   ProcessResult.where(status: 1, sent_status: 2).limit(2000)
    # end
  end
end
