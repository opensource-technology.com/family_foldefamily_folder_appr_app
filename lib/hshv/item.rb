module HSHV
  class Item
    attr_reader :hospcode, :hv_client_code, :pid, :surveytype, :hv_code_check

    def initialize(instance)
      if instance.instance_of?(ProcessResult)
        extract_process_result(instance)
      else
        extract(instance)
      end
    end

    private

    def extract(instance)
      @hospcode = instance.hospcode
      @datesurvey = instance.datesurvey.strftime('%Y%m%d')
      @create_date = datetime_format(instance.updated_at)
      extract_person(instance.person)
      extract_community_survey(instance.person.house.community_survey)
      extract_house(instance.person.house)
      if instance.instance_of?(HomeVisit)
        extract_home_visit(instance)
        @hv_client_code = instance.hvid
        @surveytype = 2
      elsif instance.instance_of?(PersonHealthSurvey)
        @hv_client_code = instance.phsurvey_id
        @surveytype = 1
      else
        raise 'Unknow Object'
      end
    end

    def extract_community_survey(community_survey)
      @villa_code = community_survey.vid
      @villaname = community_survey.vname
    end

    def extract_person(person)
      @pid = person.pid&.slice(0...13)
      @title_id = person.title.code
      @first_name = person.first_name.strip
      @last_name = person.last_name&.strip
      if person.birth_date
        @birth_date = person.birth_date.strftime('%Y%m%d')
        @age_year, @age_mm = age(person.birth_date)
      else
        @birth_date = ''
        @age_year = 0
        @age_mm = 0
      end
      @sex = person.sex
    end

    def extract_home_visit(hv)
      @sbp = hv.sbp
      @dbp = hv.dbp
      @weight = hv.weight
      @height = hv.height
      @healthadvice = hv.healthadvice
    end

    def extract_house(house)
      @house_id = house.house_id
      @house = house.house
      @soisum = house.soisum&.slice(0...50)
      @soimain = house.soimain&.slice(0...50)
      @road = house.road
      @catm = house.catm
      @postcode = house.postcode
      @phonenumber = house.phonenumber&.slice(0...30)
      @latitude = house.latitude
      @longitude = house.longitude
    end

    def extract_process_result(instance)
      @hospcode = instance.hospcode
      @hv_client_code = instance.client_code
      @hv_code_check = instance.code_check
    end

    def age(dob)
      now = Time.now.utc.to_date
      year = now.year - dob.year

      month = if now.month > dob.month
                now.month - dob.month
              else
                12 - (dob.month - now.month)
              end
      [year, month]
    end

    def date_format(date)
      return "-" if date.nil?

      date.strftime('%Y%m%d')
    end

    def datetime_format(date)
      return "-" if date.nil?

      date.strftime("%Y%m%d%H%M%S")
    end
  end
end
