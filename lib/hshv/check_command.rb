module HSHV
  class CheckCommand < Command
    def call(*args, **kwargs)
      if ENV.key?('FIXED_TOKEN')
        token = ENV['FIXED_TOKEN']
      else
        params = { username: service_username, password: service_password, appid: 11 }
        p params
        login_res = Faraday.post("#{api_url}/Auth/login", params.to_json)
        data = JSON.parse(login_res.body)
        token = data['token_key']
      end

      connection = Faraday.new("#{api_url}/v1/HVService/check_approved") do |conn|
        conn.adapter :net_http
        conn.headers['token-key'] = token
        conn.headers['content-type'] = 'application/json'
      end

      item_list = kwargs[:list]
      item_list.each do |item|
        res = connection.post("#{api_url}/v1/HVService/check_approved", item.to_json)
        process_response(res, item)
      end
    end

    def process_response(res, item)
      if res.status == 200
        body = JSON.parse(res.body)
	puts "--response--"
	puts body
	puts "---"
        if body['status'] == 'error'
          # TODO: record not found, cancel this process result
          #deactivate_process_result(item)
	  check_command_with_error(body, item)
        else
          update_status(body['record'], item)
        end
      else
        # TODO: handle error
        EventLog.info("CHECK", "Server Error [client_code: #{item.hv_client_code}, code_check: #{item.hv_code_check}]")
      end
    end

    def check_command_with_error(data, item)
      pr = ProcessResult.find_by(client_code: item.hv_client_code)

      if pr.present?
        pr.update(status: 3, status_description: merge_errors(data['message'], parse_errors(data['error_list'])))
      else
        puts "Error:: not find ProcessResult"
      end
    end

    def merge_errors(*errors)
      errors.join(',')
    end

    def deactivate_process_result(item)
      pr = ProcessResult.find_by(client_code: item.hv_client_code)
      pr.update(status: 0, status_description: 'Not found')
      EventLog.info("CHECK", "Not found process result. [client_code: #{item.hv_client_code}, code_check: #{item.hv_code_check}]")
    end

    def update_status(record, item)
      pr = ProcessResult.find_by(client_code: item.hv_client_code)
      pr.update(status: record['hv_import_status'], status_description: record['hv_import_status_desc'])
      EventLog.info("CHECK", "Update process result. [client_code: #{item.hv_client_code}, code_check: #{item.hv_code_check}]")
    end
  end
end
