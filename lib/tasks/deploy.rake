namespace :deploy do
  desc "Create master key for production"
  task :make_key do
    `echo 'd2c2067edb536bb1ca14dd08e8caa629' > config/master.key`
  end

  desc "Create systemd config for application"
  task :systemd do
    CONFIG = <<~Q.freeze
      [Unit]
      Description=#{Rails.configuration.app_name} Server
      After=network.target

      # Uncomment for socket activation (see below)
      # Requires=puma.socket

      [Service]
      # Foreground process (do not use --daemon in ExecStart or config.rb)
      Type=simple

      # Preferably configure a non-privileged user
      User=#{`whoami`}

      # The path to the puma application root
      # Also replace the "<WD>" place holders below with this path.
      WorkingDirectory=#{Dir.pwd}

      # Helpful for debugging socket activation, etc.
      Environment=RAILS_ENV=production

      # The command to start Puma. This variant uses a binstub generated via
      # `bundle binstubs puma --path ./sbin` in the WorkingDirectory
      # (replace "<WD>" below)
      ExecStart=#{`rbenv root`.chomp}/shims/bundle exec puma -C #{Dir.pwd}/config/puma.rb
      ExecStop=#{`rbenv root`.chomp}/shims/bundle exec pumactl -S #{Dir.pwd}/tmp/pids/puma.pid stop
      PIDFile=#{Dir.pwd}/tmp/pids/puma.pid

      # Variant: Use config file with `bind` directives instead:
      # ExecStart=<WD>/sbin/puma -C config.rb
      # Variant: Use `bundle exec --keep-file-descriptors puma` instead of binstub

      # Restart=always

      [Install]
      WantedBy=multi-user.target
    Q

    File.open("#{Dir.pwd}/tmp/family_folder.service", 'w', 0o655) do |f|
      f.write(CONFIG)
      f.flush
    end

    copy_files
  end

  desc "Create systemd config for sidekiq"
  task :sidekiq do
    CONFIG = <<~Q.freeze
      # Customize this file based on your bundler location, app directory, etc.
      # Put this in /usr/lib/systemd/system (CentOS) or /lib/systemd/system (Ubuntu).
      # Run:
      #   - systemctl enable sidekiq
      #   - systemctl {start,stop,restart} sidekiq
      #
      # This file corresponds to a single Sidekiq process. Add multiple copies
      # to run multiple processes (sidekiq-1, sidekiq-2, etc).
      #
      # See Inspeqtor's Systemd wiki page for more detail about Systemd:
      # https://github.com/mperham/inspeqtor/wiki/Systemd
      #
      [Unit]
      Description=sidekiq
      After=syslog.target network.target

      [Service]
      Type=simple
      WorkingDirectory=#{Dir.pwd}
      ExecStart=#{`rbenv root`.chomp}/shims/bundle exec sidekiq -e production -C #{Dir.pwd}/config/sidekiq.yml
      User=#{`whoami`}
      # Group=deploy
      # UMask=0002

      # if we crash, restart
      RestartSec=1
      #Restart=on-failure
      Restart=always

      # output goes to /var/log/syslog
      StandardOutput=syslog
      StandardError=syslogv 

      # This will default to "bundler" if we don't specify it
      SyslogIdentifier=sidekiq

      [Install]
      WantedBy=multi-user.target
    Q
    File.open("#{Dir.pwd}/tmp/sidekiq.service", 'w', 0o655) do |f|
      f.write(CONFIG)
      f.flush
    end

    `sudo cp #{Dir.pwd}/tmp/sidekiq.service /etc/systemd/system`
    `rm #{Dir.pwd}/tmp/sidekiq.service`
    puts 'Copy sidekiq.service to /etc/systemd/system success.'
    `sudo systemctl enable sidekiq.service`
  end

  private

  def copy_files
    `sudo cp #{Dir.pwd}/tmp/family_folder.service /etc/systemd/system`
    `rm #{Dir.pwd}/tmp/family_folder.service`
    puts 'Copy family_folder.service to /etc/systemd/system success.'
    `sudo systemctl enable family_folder.service`
  end
end
