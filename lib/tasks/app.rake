require 'yaml'

namespace :app do
  desc "Auto increase build number"
  task auto_build_number: :environment do
    version = YAML.safe_load(File.read('config/version.yml'))
    version['build'] = version['build'].to_i + 1
    File.open("config/version.yml", "w") { |file| file.write(version.to_yaml.gsub("---\n", '')) }
    puts "New build number is #{version['build']}"
  end

  desc "Get current build number"
  task build_number: :environment do
    version = YAML.safe_load(File.read('config/version.yml'))
  end
end
